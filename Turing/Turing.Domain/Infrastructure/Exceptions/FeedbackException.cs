﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Turing.Domain.Exceptions
{
    public class FeedbackException : Exception
    {
        public FeedbackException()
        {
        }

        public FeedbackException(string message) : base(message)
        {
        }
    }
}
