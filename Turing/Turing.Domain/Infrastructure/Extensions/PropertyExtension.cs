﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Turing.Domain.Infrastructure.Extensions
{
    public static class PropertyExtension
    {
        public static void SetPropertyValue<T>(this T source, T value)
        {
            source.GetType().GetProperty(nameof(source)).SetValue(source, value);
        }
    }
}
