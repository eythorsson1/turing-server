﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Turing.Domain.Infrastructure.Extensions
{
    public static class LinqExtensions
    {
        public static List<TResult> SelectList<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
        {
            return source.Select(selector).ToList();
        }
        public static List<TResult> SelectList<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, int, TResult> selector)
        {
            return source.Select(selector).ToList();
        }

        public static List<TSource> WhereList<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            return source.Where(predicate).ToList();
        }
        public static List<TSource> WhereList<TSource>(this IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
        {
            return source.Where(predicate).ToList();
        }

        public static bool Empty<TSource>(this IEnumerable<TSource> source)
        {
            return source == null || !source.Any();
        }

        public static bool NotEmpty<TSource>(this IEnumerable<TSource> source)
        {
            return source != null && source.Any();
        }

        public static string Join<TSource>(this IEnumerable<TSource> source, string separator)
        {
            return string.Join(separator, source);
        }
    }
}
