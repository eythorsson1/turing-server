﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Turing.Domain.Infrastructure.Helper
{
    public class StringHelper
    {
        public static char[] LegalLetters => "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".ToCharArray();

        public static string GenerateRandomString(uint stringLength)
        {
            Random random = new Random();

            char[] legalLetters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".ToCharArray();
            string response = "";

            for (int i = 0; i < stringLength; i++)
                response += legalLetters[random.Next(0, legalLetters.Length - 1)];

            return response;
        }
    }
}
