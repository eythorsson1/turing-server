﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Turing.Domain.Domain.Enums.Device;
using Turing.Domain.Domain.Models.Device.Feature;
using Turing.Domain.Exceptions;

namespace Turing.Domain.Infrastructure.Helper
{
    public static class PresetHelper
    {
        public static readonly Regex PresetRegex = new Regex(@"{{(.*?)}}");


        public static void ValidateDataPreset(string dataPreset, List<DeviceFeatureModel> features)
        {
            var invalidKeys = new List<string>();
            var presetPlaceholder = new Regex(@"\{\{(.*?)\}\}");

            var featureDict = features.ToDictionary(x => x.DeviceFeatureId);

            var matches = presetPlaceholder.Matches(dataPreset);

            if (features == null && matches.Count > 0)
                throw new ArgumentException("Features is not set while the DataPreset includes device features");

            foreach (Match item in matches) {
                var parts = item.Groups[1].ToString().Split(".");

                switch (parts[0].ToLower()) {
                    case "device": {
                        if (!long.TryParse(parts[2], out long featureId) || !featureDict.TryGetValue(featureId, out var feature))
                            invalidKeys.Add(parts[0]);
                        else {
                            if (feature.Type == DeviceFeatureEnum.PowerState) {
                                if (parts.Length > 1) invalidKeys.Add(item.Groups[1].ToString());
                            }
                            else if (feature.Type == DeviceFeatureEnum.ColorPicker) {
                                if (!new string[] { "Red", "Green", "Blue", "Hue", "Saturation", "Value" }.Contains(parts[3]))
                                    invalidKeys.Add(item.Groups[1].ToString());
                            }
                            else if (feature.Type == DeviceFeatureEnum.ColorShema) {
                                if (parts.Length > 3) invalidKeys.Add(item.Groups[1].ToString());
                            }
                            else if (feature.Type == DeviceFeatureEnum.Brightness) {
                                if (parts.Length > 3) invalidKeys.Add(item.Groups[1].ToString());
                            }
                            else if (feature.Type == DeviceFeatureEnum.Select) {
                                if (parts.Length > 3) invalidKeys.Add(item.Groups[1].ToString());
                            }
                            else if (feature.Type == DeviceFeatureEnum.Slider) {
                                if (parts.Length > 3) invalidKeys.Add(item.Groups[1].ToString());
                            }
                            else if (feature.Type == DeviceFeatureEnum.Toggle) {
                                if (parts.Length > 3) invalidKeys.Add(item.Groups[1].ToString());
                            }
                        }


                        break;
                    }
                }


                
            }
            if (invalidKeys.Any())
                throw new FeedbackException($"The Data Preset Validation failed.<br />Invalid preset properties:<br />{string.Join("<br />", invalidKeys)}");
        }


        //public void ValidateDeviceData(DeviceTypeFeatureModel feature, object value)
        //{
        //    if (feature.Type == DeviceTypeFeatureEnum.PowerState) {
        //        if (!(value is bool)) throw new FeedbackException($"The object property {feature.Name} is not a boolean");
        //    }
        //    else if (feature.Type == DeviceTypeFeatureEnum.ColorPicker) {
        //        if (!(value is DeviceTypeFeature_ColorPicker))
        //            throw new FeedbackException($"The property {feature.Name} is not a of the ColorPicker object type");

        //        var val = value as DeviceTypeFeature_ColorPicker;
        //        if (val.Red < 0 || val.Red > 255) throw new FeedbackException($"The {feature.Name}.Red value is invalid, it should be a number between 0 and 255");
        //        if (val.Green < 0 || val.Green > 255) throw new FeedbackException($"The {feature.Name}.Green value is invalid, it should be a number between 0 and 255");
        //        if (val.Blue < 0 || val.Blue > 255) throw new FeedbackException($"The {feature.Name}.Blue value is invalid, it should be a number between 0 and 255");

        //    }
        //    else if (feature.Type == DeviceTypeFeatureEnum.Brightness) {
        //        if (!float.TryParse(value.ToString(), out float val))
        //            throw new FeedbackException($"The property {feature.Name} does not have a valid Brightness value type");

        //        if (val < 0 || val > 100) throw new FeedbackException($"The {feature.Name} brightness value should be a number between 0 and 100");
        //    }
        //    else if (feature.Type == DeviceTypeFeatureEnum.Select) {
        //        throw new NotImplementedException();
        //    }
        //    else if (feature.Type == DeviceTypeFeatureEnum.Slider) {
        //        throw new NotImplementedException();
        //    }
        //    else if (feature.Type == DeviceTypeFeatureEnum.Toggle) {
        //        throw new NotImplementedException();
        //    }
        //    else {
        //        throw new NotImplementedException();
        //    }
        //}
    }

}
