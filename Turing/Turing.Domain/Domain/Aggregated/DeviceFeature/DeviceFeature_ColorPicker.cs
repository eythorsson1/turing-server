﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Turing.Domain.Domain.Aggregated.DeviceFeature
{
    public class DeviceFeature_ColorPicker
    {
        private Color color;

        public byte Red {
            get { return color.R; }
            set { color = Color.FromArgb(value, Green, Blue); }
        }

        public byte Green {
            get { return color.G; }
            set { color = Color.FromArgb(Red, value, Blue); }
        }

        public byte Blue {
            get { return color.B; }
            set { color = Color.FromArgb(Red, Green, value); }
        }

        public double Hue {
            get { return color.GetHue(); }
            set { color = ColorFromHSV(value, Saturation, Value); }
        }
        public double Saturation {
            get { return color.GetSaturation(); }
            set { color = ColorFromHSV(Hue, value, Value); }
        }
        public double Value {
            get { return color.GetBrightness(); }
            set { color = ColorFromHSV(Hue, Saturation, value); }
        }


        public string Hex {
            get { return $"#{Red:X2}{Green:X2}{Blue:X2}"; }
            set {
                value = value.TrimStart('#');
                if (value.Length == 3 || value.Length != 6)
                    throw new ArgumentException("The hex string must be 3 or 6 characters long");

                int partLength = value.Length / 3;
                byte[] parts = new byte[3];

                for (int i = 0; i < 3; i++) {
                    var str = string.Concat(Enumerable.Repeat(
                        value.Substring(i * partLength, partLength),
                        3 - partLength
                    ));

                    parts[i] = byte.Parse(str, System.Globalization.NumberStyles.AllowHexSpecifier);
                }

                Red = parts[0];
                Green = parts[1];
                Blue = parts[2];
            }
        }


        public static void ColorToHSV(Color color, out double hue, out double saturation, out double value)
        {
            int max = Math.Max(color.R, Math.Max(color.G, color.B));
            int min = Math.Min(color.R, Math.Min(color.G, color.B));

            hue = color.GetHue();
            saturation = (max == 0) ? 0 : 1d - (1d * min / max);
            value = max / 255d;
        }

        public static Color ColorFromHSV(double hue, double saturation, double value)
        {
            int hi = Convert.ToInt32(Math.Floor(hue / 60)) % 6;
            double f = hue / 60 - Math.Floor(hue / 60);

            value = value * 255;
            int v = Convert.ToInt32(value);
            int p = Convert.ToInt32(value * (1 - saturation));
            int q = Convert.ToInt32(value * (1 - f * saturation));
            int t = Convert.ToInt32(value * (1 - (1 - f) * saturation));

            if (hi == 0)
                return Color.FromArgb(255, v, t, p);
            else if (hi == 1)
                return Color.FromArgb(255, q, v, p);
            else if (hi == 2)
                return Color.FromArgb(255, p, v, t);
            else if (hi == 3)
                return Color.FromArgb(255, p, q, v);
            else if (hi == 4)
                return Color.FromArgb(255, t, p, v);
            else
                return Color.FromArgb(255, v, p, q);
        }

        public bool TrySet(string propertyName, string value)
        {
            // HSV has the lowest priority
            if (propertyName == "Hue") { if (double.TryParse(value, out double val)) Hue = val; }
            else if (propertyName == "Value") { if (double.TryParse(value, out double val)) Value = val; }
            else if (propertyName == "Saturation") { if (double.TryParse(value, out double val)) Saturation = val; }

            // RGB has medium priority
            else if (propertyName == "Red") { if (byte.TryParse(value, out byte val)) Red = val; }
            else if (propertyName == "Green") { if (byte.TryParse(value, out byte val)) Green = val; }
            else if (propertyName == "Blue") { if (byte.TryParse(value, out byte val)) Blue = val; }

            // HEX has high priority
            else if (propertyName == "Hex") Hex = value;
            else return false;

            return true;
        }

        public string TryGet(string propertyName)
        {
            return GetType().GetProperty(propertyName)
                ?.GetValue(this).ToString();
        }

        public Dictionary<string, string> ToDictionary()
        {
            return GetType().GetProperties().Cast<PropertyInfo>()
                .ToDictionary(prop=> prop.Name, prop=> prop.GetValue(this).ToString());
        }
    }
}
