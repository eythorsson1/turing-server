﻿namespace Turing.Domain.Aggregated.Automation
{
    public class AutomationAction_Mqtt : BaseAutomationItemData
    {
        public string Topic { get; set; }
        public object Payload { get; set; }

        private AutomationAction_Mqtt() { }
        public AutomationAction_Mqtt(object data) : base(data) { }
        public AutomationAction_Mqtt(string topic, object payload)
        {
            Topic = topic;
            Payload = payload;
        }
    }
}
