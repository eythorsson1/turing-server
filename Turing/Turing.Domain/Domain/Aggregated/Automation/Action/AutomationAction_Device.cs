﻿namespace Turing.Domain.Aggregated.Automation
{
    public class AutomationAction_Device : BaseAutomationItemData
    {
        public long DeviceId { get; set; }
        public object DataPreset { get; set; }

        private AutomationAction_Device() { }
        public AutomationAction_Device(object data) : base(data) { }
        public AutomationAction_Device(long deviceId, object payload)
        {
            DeviceId = deviceId;
            DataPreset = payload;
        }
    }
}
