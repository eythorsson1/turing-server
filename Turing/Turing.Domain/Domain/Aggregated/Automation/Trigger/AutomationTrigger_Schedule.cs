﻿namespace Turing.Domain.Aggregated.Automation
{
    public class AutomationTrigger_Schedule : BaseAutomationItemData
    {
        public string CronExpression { get; set; }

        private AutomationTrigger_Schedule() : base() { }
        public AutomationTrigger_Schedule(object data) : base(data) { }
        public AutomationTrigger_Schedule(string cronExpression) : base()
        {
            CronExpression = cronExpression;
        }
    }
}
