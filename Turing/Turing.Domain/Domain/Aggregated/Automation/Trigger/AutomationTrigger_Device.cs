﻿namespace Turing.Domain.Aggregated.Automation
{
    public class AutomationTrigger_Device : BaseAutomationItemData
    {
        public long DeviceId { get; set; }
        public object Payload { get; set; }

        private AutomationTrigger_Device() : base() { }
        public AutomationTrigger_Device(object data) : base(data) { }
        public AutomationTrigger_Device(long deviceId, object payload) : base()
        {
            DeviceId = deviceId;
            Payload = payload;
        }
    }
}
