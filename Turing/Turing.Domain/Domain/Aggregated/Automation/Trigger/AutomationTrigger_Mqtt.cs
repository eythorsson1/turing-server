﻿namespace Turing.Domain.Aggregated.Automation
{
    public class AutomationTrigger_Mqtt : BaseAutomationItemData
    {
        public string Topic { get; set; }
        public object Payload { get; set; }

        private AutomationTrigger_Mqtt() { }
        public AutomationTrigger_Mqtt(object data) : base(data) { }
        public AutomationTrigger_Mqtt(string topic, object payload)
        {
            Topic = topic;
            Payload = payload;
        }
    }
}
