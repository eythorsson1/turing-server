﻿using Turing.Domain.Aggregated.Automation;

namespace Turing.Domain.Domain.Aggregated.Automation
{
    public class AutomationCondition_IsEqual : BaseAutomationItemData
    {
        public object Value { get; set; }
    }
}
