﻿using System;
using Turing.Domain.Aggregated.Automation;
using Turing.Domain.Enums.Automation;
using Turing.Domain.Enums.Automation.AutomationItemType;
using Turing.Domain.Models.Automation.Item;

namespace Turing.Domain.Models.Automation
{
    public class AutomationTriggerModel : BaseAutomationItemModel<AutomationTriggerEnum> 
    {
        private AutomationTriggerModel() : base() { }
        public AutomationTriggerModel(long automationId, AutomationTriggerEnum type, long clientId, object data) 
            : base(automationId, AutomationTypeEnum.Trigger, type, clientId) {

            SetData(ParseData(type, data));
        }


        public static IAutomationItemData ParseData(AutomationTriggerEnum type, object data)
        {
            IAutomationItemData response;

            if (type == AutomationTriggerEnum.Schedule) response = new AutomationTrigger_Schedule(data);
            else if (type == AutomationTriggerEnum.DeviceState) response = new AutomationTrigger_Device(data);
            else if (type == AutomationTriggerEnum.DeviceTimeout) response = new AutomationTrigger_Device(data);
            else if (type == AutomationTriggerEnum.DeviceCallbackEmpty) response = new AutomationTrigger_Device(data);
            else if (type == AutomationTriggerEnum.DeviceCallbackSuccess) response = new AutomationTrigger_Device(data);
            else if (type == AutomationTriggerEnum.DeviceCallbackFailiour) response = new AutomationTrigger_Device(data);
            else throw new ArgumentException("The provided trigger type is not supported");

            return response;
        }
    }
}
