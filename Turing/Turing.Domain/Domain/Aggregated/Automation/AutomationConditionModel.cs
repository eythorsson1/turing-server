﻿using System;
using Turing.Domain.Domain.Aggregated.Automation;
using Turing.Domain.Enums.Automation;
using Turing.Domain.Enums.Automation.AutomationItemType;

namespace Turing.Domain.Models.Automation.Item
{
    public class AutomationConditionModel : BaseAutomationItemModel<AutomationConditionEnum> 
    {
        private AutomationConditionModel() { }
        public AutomationConditionModel(long automationId, AutomationConditionEnum type, long clientId, object data) 
            : base(automationId, AutomationTypeEnum.Condition, type, clientId) {

            SetData(ParseData(type, data));
        }

        public static IAutomationItemData ParseData(AutomationConditionEnum type, object data)
        {
            throw new NotFiniteNumberException();
            //switch (type) {
            //    case AutomationConditionEnum.IsEqual:
            //        return data as AutomationCondition_IsEqual;
            //    default:
            //        throw new NotImplementedException();
            //}
        }
    }
}
