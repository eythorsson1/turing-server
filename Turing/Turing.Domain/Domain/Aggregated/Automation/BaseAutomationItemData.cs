﻿using Newtonsoft.Json;
using Turing.Domain.Models.Automation.Item;

namespace Turing.Domain.Aggregated.Automation
{
    abstract public class BaseAutomationItemData : IAutomationItemData
    {
        protected BaseAutomationItemData() { }

        protected BaseAutomationItemData(object data)
        {
            if (data == null) return;

            var parsedData = JsonConvert.DeserializeObject(data.ToString(), GetType());

            foreach (var item in GetType().GetProperties()) {
                if (parsedData.GetType().GetProperty(item.Name) != null)
                    item.SetValue(this, item.GetValue(parsedData));
            }
        }

        public override string ToString() => JsonConvert.SerializeObject(this);
    }
}
