﻿using System;
using Turing.Domain.Aggregated.Automation;
using Turing.Domain.Enums.Automation;
using Turing.Domain.Enums.Automation.AutomationItemType;

namespace Turing.Domain.Models.Automation.Item
{
    public class AutomationActionModel : BaseAutomationItemModel<AutomationActionEnum>
    {
        private AutomationActionModel() : base() { }
        public AutomationActionModel(long automationId, AutomationActionEnum type, long clientId, object data)
            : base(automationId, AutomationTypeEnum.Action, type, clientId)
        {

            SetData(ParseData(type, data));
        }


        public static IAutomationItemData ParseData(AutomationActionEnum type, object data)
        {
            switch (type) {
                case AutomationActionEnum.Device:
                    return new AutomationAction_Device(data);
                case AutomationActionEnum.Mqtt:
                    return new AutomationAction_Mqtt(data);
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
