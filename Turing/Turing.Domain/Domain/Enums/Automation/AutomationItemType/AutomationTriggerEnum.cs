﻿
namespace Turing.Domain.Enums.Automation.AutomationItemType
{
    public enum AutomationTriggerEnum
    {
        Schedule = 1,
        DeviceState = 2,
        DeviceTimeout = 3,
        DeviceCallbackSuccess = 4,
        DeviceCallbackFailiour = 5,
        DeviceCallbackEmpty = 6,
        //WebRequest = 7
    }
}
