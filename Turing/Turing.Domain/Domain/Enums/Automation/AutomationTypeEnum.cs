﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Turing.Domain.Enums.Automation
{
    public enum AutomationTypeEnum
    {
        Trigger = 1,
        Condition = 2,
        Action = 3
    }
}
