﻿namespace Turing.Domain.Domain.Enums.Device
{
    public enum DeviceFeatureEnum
    {
        PowerState = 1,

        ColorPicker = 2,
        ColorShema = 3,
        Brightness = 4,

        Select = 5,
        Slider = 6,
        Toggle = 7
    }
}
