﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Turing.Domain.Enums.Device
{
    public enum  DeviceLogActionEnum
    {
        [Description("Publish Message")]
        PublishMessage = 1,
        [Description("Publish Message Response")]
        PublishMessageResponse = 2,
        DeviceConnected = 3
    }
}
