﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Turing.Domain.Infrastructure.Extensions;
using Turing.Domain.Models;

namespace Turing.Domain.Domain.Models.Data.Value
{
    public class DataValueModel : IModel
    {
        public long DataValueId { get; private set; }
        public long DataPresetId { get; set; }
        public string DataValueStr { get; set; }
        public long ClientId { get; set; }

        private DataValueModel() { }
        public DataValueModel(long dataPresetId, string dataValueStr, long clientId)
        {
            DataPresetId = dataPresetId;
            DataValueStr = dataValueStr;
            ClientId = clientId;
        }

        public List<(string Path, string Value)> Items => GetItems(JsonConvert.DeserializeObject(DataValueStr));

        public bool TryGetPath(string path, out string value)
        {
            value = null;
            if (string.IsNullOrWhiteSpace(path)) return false;

            object val = JsonConvert.DeserializeObject(DataValueStr);
            foreach (var part in path.Split("."))
                val = (val as JObject)?[part];

            value = val?.ToString();
            return val != null;
        }

        public static bool TryGetPath(ref string dataValueStr, string path, out string value)
        {
            value = null;
            if (string.IsNullOrWhiteSpace(path)) return false;

            object val = JsonConvert.DeserializeObject(dataValueStr);
            foreach (var part in path.Split("."))
                val = (val as JObject)?[part];

            value = val?.ToString();
            return val != null;
        }

        public bool TrySetPath(string path, string value)
        {
            if (path == null || value == null) return false;
            object val = JsonConvert.DeserializeObject(DataValueStr);
            var parts = path.Split(".");

            for (int i = 0; i < parts.Length - 1; i++)
                val = (val as JObject)?[parts[i]];

            (val as JObject)[parts.Last()] = value;

            DataValueStr = (val as JObject).Root.ToString();
            return (val as JObject)?[parts.Last()] != null;
        }

        public static bool TrySetPath(ref string dataValueStr, string path, string value)
        {
            object val = JsonConvert.DeserializeObject(dataValueStr);
            var parts = path.Split(".");

            for (int i = 0; i < parts.Length - 1; i++)
                val = (val as JObject)?[parts[i]];

            if (val == null) return false;

            (val as JObject)[parts.Last()] = value;

            dataValueStr = (val as JObject).Root.ToString();
            return (val as JObject)?[parts.Last()] != null;
        }

        public static List<(string Path, string Value)> GetItems(object request)
        {
            var response = new List<(string Path, string Placeholder)>();

            foreach (var item in request as JObject) {
                if (item.Value is JObject)
                    response.AddRange(GetItems(item.Value));
                else
                    response.Add((
                        Path: item.Value.Path,
                        Placeholder: item.Value.ToString()
                            .Replace("{", "").Replace("}", "")
                        )
                    );
            }

            return response;
        }

        //public bool TryGetDeviceIds(out List<long> deviceIds)
        //{
        //    deviceIds = Items?.Where(x => x.StartsWith("device"))
        //        .SelectList(x => long.Parse(x.Split(".")[1]));

        //    return Items != null;
        //    //throw new NotImplementedException("UNTESTED CODE"); // UNTESTED CODE
        //}
    }
}
