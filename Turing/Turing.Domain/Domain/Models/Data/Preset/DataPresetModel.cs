﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Turing.Domain.Infrastructure.Extensions;
using Turing.Domain.Infrastructure.Helper;

namespace Turing.Domain.Domain.Models.Data.Preset
{
    public class DataPresetModel
    {
        public long DataPresetId { get; private set; }
        public string DataPresetStr { get; set; }
        public long ClientId { get; }


        private DataPresetModel() { }
        public DataPresetModel(string dataPresetStr, long clientId)
        {
            DataPresetStr = dataPresetStr;
            ClientId = clientId;
        }


        public List<(string Path, string Placeholder)> Items
            {
            get {
                List<(string Path, string Placeholder)> ParseData(object request)
                {
                    var response = new List<(string Path, string Placeholder)>();

                    foreach (var item in request as JObject) {
                        if (item.Value is JObject) 
                            response.AddRange(ParseData(item.Value));
                        else
                            response.Add((
                                Path: item.Value.Path, 
                                Placeholder: item.Value.ToString()
                            ));
                        
                    }

                    return response;
                }

                return ParseData(JsonConvert.DeserializeObject(DataPresetStr));
            }
        }

        /// <summary>
        /// Dictionary< PresetPath, DataPath > -> 3.Red => Color.R
        /// </summary>
        public Dictionary<string, string> PresetDataPathDict => Items
           .Where(x => PresetHelper.PresetRegex.IsMatch(x.Placeholder))
           .ToDictionary(x => string.Join(".", PresetHelper.PresetRegex.Match(x.Placeholder).Groups[1].Value.Split(".").Skip(2)), x => x.Path);

        /// <summary>
        /// Dictionary< DataPath, PresetPath > -> Color.3 => 3.Red
        /// </summary>
        public Dictionary<string, string> DataPresetPathDict => Items
            .Where(x => PresetHelper.PresetRegex.IsMatch(x.Placeholder))
            .ToDictionary(x => x.Path, x => string.Join(".", PresetHelper.PresetRegex.Match(x.Placeholder).Groups[1].Value.Split(".").Skip(2)));


        public bool TryGetPath(string path, out string value)
        {
            value = null;
            if (string.IsNullOrWhiteSpace(path)) return false;

            object val = JsonConvert.DeserializeObject(DataPresetStr);
            foreach (var part in path.Split("."))
                val = (val as JObject)?[part];

            value = val?.ToString();
            return val != null;
        }

        public bool TrySetPath(string path, string value)
        {
            object val = JsonConvert.DeserializeObject(DataPresetStr);
            var parts = path.Split(".");

            for (int i = 0; i < parts.Length - 1; i++)
                val = (val as JObject)?[parts[i]];

            if (val == null) return false;

            (val as JObject)[parts.Last()] = value;
            
            DataPresetStr = (val as JObject).Root.ToString();
            return (val as JObject)?[parts.Last()] != null;
        }

        public bool TryGetDeviceIds(out List<long> deviceIds)
        {
            deviceIds = Items?.Where(x => x.Placeholder.StartsWith("device"))
                .SelectList(x => long.Parse(x.Placeholder.Split(".")[1]));

            return Items != null;
            throw new NotImplementedException("UNTESTED CODE"); // UNTESTED CODE
        }

    }
}
