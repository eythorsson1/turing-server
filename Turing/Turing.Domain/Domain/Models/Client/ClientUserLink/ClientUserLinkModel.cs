﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Turing.Domain.Domain.Models.Client.ClientUserLink
{
    public class ClientUserLinkModel
    {
        public long ClientUserLinkId { get; }
        public long ClientId { get; }
        public long UserId { get; }

        public ClientUserLinkModel(long clientId, long userId)
        {
            ClientId = clientId;
            UserId = userId;
        }
    }
}
