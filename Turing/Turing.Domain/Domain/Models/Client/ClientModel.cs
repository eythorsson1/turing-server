﻿using System;
using System.Collections.Generic;
using System.Text;
using Turing.Domain.Infrastructure.Helper;

namespace Turing.Domain.Models.Client
{
    public class ClientModel : IModel
    {
        public long ClientId { get; private set; }
        public string Name { get; set; }
        public string UniqueIdentifier { get; } // length == 10

        public DateTime CreatedDateUTC { get; }
        public long CreatedByUserId { get; private set; }
        public long CreatedByEmail { get; }
        public long CreatedByFirstName { get; }
        public long CreatedByLastName { get; }

        public ClientModel(string name, long createdByUserId)
        {
            Name = name;
            UniqueIdentifier = StringHelper.GenerateRandomString(10);
            CreatedByUserId = createdByUserId;
            CreatedDateUTC = DateTime.UtcNow;
        }
    }

    public enum ClientOrderByEnum
    {
        ClientId = 1,
        Name = 2,
        UniqueIdentifier = 3,
        CreatedDateUTC = 4,
        CreatedByUserId = 5,
        CreatedByFullName = 6,
        CreatedByFirstName = 7,
        CreatedByLastName = 8,
        CreatedByEmail = 9
    }
}
