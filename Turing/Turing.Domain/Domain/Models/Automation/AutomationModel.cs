﻿using System;
using System.Collections.Generic;
using System.Text;
using Turing.Domain.Exceptions;

namespace Turing.Domain.Models.Automation
{
    public class AutomationModel : IModel
    {
        public long AutomationId { get; }
        public string Name { get; set; }

        public DateTime CreatedDateUTC { get; }
        public long CreatedByUserId { get; }
        public long CreatedByFirstName { get; }
        public long CreatedByLastName { get; }

        public DateTime LastModifiedDateUTC { get; }
        public long LastModifiedByUserId { get; set; }
        public long LastModifiedByFirstName { get; }
        public long LastModifiedByLastName { get; }

        public long ClientId { get; }


        private AutomationModel() { }

        public AutomationModel(string name, long createdByUserId, long clientId)
        {
            Name = name;
            CreatedDateUTC = DateTime.UtcNow;
            CreatedByUserId = createdByUserId;
            ClientId = clientId;
        }

        public void Validate()
        {
            if (string.IsNullOrWhiteSpace(Name)) throw new FeedbackException("A name must be specified for the automation");

            if (CreatedByUserId < 1) throw new ArgumentNullException("CreatedByUserId is not set");
        }
    }





}
