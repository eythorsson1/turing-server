﻿using Newtonsoft.Json;
using System;
using Turing.Domain.Enums.Automation;

namespace Turing.Domain.Models.Automation.Item
{
    public interface IAutomationItemData
    {
        
    }

    public abstract class BaseAutomationItemModel<AutomationItemTypeEnum> : IModel
        where AutomationItemTypeEnum : Enum
    {
        public long AutomationItemId { get; protected set; }
        public long AutomationId { get; protected set; }
        public AutomationTypeEnum AutomationType { get; protected set; }
        public AutomationItemTypeEnum Type { get; protected set; }

        public string DataStr { get; private set; }
        public long DataPresetId { get; protected set; }

        public long ClientId { get; protected set; }


        public T GetData<T>() where T : IAutomationItemData => JsonConvert.DeserializeObject<T>(DataStr);
        public void SetData<T>(T data) where T : IAutomationItemData => DataStr = JsonConvert.SerializeObject(data);
        

        protected BaseAutomationItemModel() { }
        public BaseAutomationItemModel(long automationId, AutomationTypeEnum automationType, AutomationItemTypeEnum itemType, long clientId)
        {
            AutomationId = automationId;
            AutomationType = automationType;
            Type = itemType;
            ClientId = clientId;
        }
    }
}
