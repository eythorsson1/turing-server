﻿using System;
using System.Collections.Generic;
using System.Text;
using Turing.Domain.Enums.Automation;

namespace Turing.Domain.Models.Automation.Item
{
    public class AutomationItemLinkModel : IModel
    {
        public long AutomationItemLinkId { get; }
        public long AutomationId { get; }
        public long ParentAutomationItemId { get; }
        public long ChildAutomationItemId { get; }
        public long ClientId { get; }

        // Aggregated
        public AutomationTypeEnum ParentAutomationType { get; }
        public int ParentAutomationItemType { get; }
        public AutomationTypeEnum ChildAutomationType { get; }
        public int ChildAutomationItemType { get; }

        private AutomationItemLinkModel() { }
        public AutomationItemLinkModel(long automationId, long clientId, long parentAutomationItemId, long childAutomationItemId)
        {
            AutomationId = automationId;
            ClientId = clientId;
            ParentAutomationItemId = parentAutomationItemId;
            ChildAutomationItemId = childAutomationItemId;
        }

        public void Validate()
        {
            if (ParentAutomationType == AutomationTypeEnum.Condition && ChildAutomationType == AutomationTypeEnum.Trigger)
                throw new ArgumentException("A Condition cannot trigger an automation.");
        }
    }
}
