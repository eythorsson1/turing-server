﻿using Turing.Domain.Models;

namespace Turing.Domain.Domain.Models.Dimension
{
    public class DimensionRegisterLinkModel : IModel
    {
        public long DimensionRegisterLinkId { get; private set; }
        public long DimensionRegisterId { get; }
        public long DimensionId { get; }

        private DimensionRegisterLinkModel() { }

        public DimensionRegisterLinkModel(long dimensionRegisterId, long dimensionId)
        {
            DimensionRegisterId = dimensionRegisterId;
            DimensionId = dimensionId;
        }
    }
}
