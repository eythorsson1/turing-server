﻿using Turing.Domain.Domain.Enums.Dimension;
using Turing.Domain.Models;

namespace Turing.Domain.Domain.Models.Dimension
{
    public class DimensionRegisterModel : IModel
    {
        public long DimensionRegisterId { get; private set; }
        public DimensionTypeEnum Type { get; }
        public string Code { get; set; }
        public string Description { get; set; }

        private DimensionRegisterModel() { }

        public DimensionRegisterModel(DimensionTypeEnum type, string code, string description)
        {
            Type = type;
            Code = code;
            Description = description;
        }
    }
}
