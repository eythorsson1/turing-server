﻿using System.Collections.Generic;
using Turing.Domain.Domain.Enums.Dimension;
using Turing.Domain.Models;

namespace Turing.Domain.Domain.Models.Dimension
{
    public class DimensionModel : IModel
    {
        public long DimensionId { get; private set; }
        public string Name { get; set; }
        public DimensionTypeEnum Type { get; }
        public long ClientId { get; }

        public List<DimensionRegisterModel> Register { get; private set; } // Aggregated

        private DimensionModel() { }

        public DimensionModel(string name, DimensionTypeEnum type, long clientId)
        {
            Name = name;
            Type = type;
            ClientId = clientId;
        }
    }
}
