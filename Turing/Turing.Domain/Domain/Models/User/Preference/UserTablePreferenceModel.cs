﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Turing.Domain.Domain.Enums.User;

namespace Turing.Domain.Domain.Models.User.Preference
{
    public class UserTablePreferenceModel<TableColumnOrderByEnum> where TableColumnOrderByEnum : Enum
    {
        public UserTablePreferenceEnum TableId { get; }
        public long UserId { get; }

        private string ColumnOrderStr { 
            set => ColumnOrder = JsonConvert.DeserializeObject<List<TableColumnOrderModel<TableColumnOrderByEnum>>>(value);
        }

        public List<TableColumnOrderModel<TableColumnOrderByEnum>> ColumnOrder { get; set; }

        private UserTablePreferenceModel() { }
        public UserTablePreferenceModel(long userId, UserTablePreferenceEnum userTablePreferenceTableId,
            List<TableColumnOrderModel<TableColumnOrderByEnum>> columnOrder)
        {

            TableId = userTablePreferenceTableId;
            UserId = userId;

            ColumnOrder = columnOrder;
        }

    }

    public class TableColumnOrderModel<TableColumnOrderByEnum> where TableColumnOrderByEnum : Enum
    {
        public bool Hidden { get; set; }
        public TableColumnOrderByEnum ColumnId { get; set; }
        public string ColumnName => System.Enum.GetName(typeof(TableColumnOrderByEnum), ColumnId);

        public TableColumnOrderModel(TableColumnOrderByEnum columnId, bool hidden = false)
        {
            ColumnId = columnId;
            Hidden = hidden;
        }
    }
}
