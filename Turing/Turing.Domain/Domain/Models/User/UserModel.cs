﻿using System;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using Turing.Domain.Exceptions;
using Turing.Domain.Infrastructure;

namespace Turing.Domain.Models.User
{
    public class UserModel : IModel
    {
        public long UserId { get; private set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long CurrentClientId { get; set; }

        public string FullName => FirstName + " " + LastName;

        public string PasswordHash { get; private set; }


        private UserModel(){}
        public UserModel(string email, string username, string firstName, string lastName, string password) {
            this.Email = email;
            this.Username = username;
            this.FirstName = firstName;
            this.LastName = lastName;

            SetPassword(password);
        }

        public void Validate()
        {
            if (string.IsNullOrWhiteSpace(Email)) throw new FeedbackException("Email is not set");
            if (string.IsNullOrWhiteSpace(FirstName)) throw new FeedbackException("First Name is not set");
            if (string.IsNullOrWhiteSpace(LastName)) throw new FeedbackException("Last Name is not set");
        }

        public void SetPassword(string password)
        {
            if (string.IsNullOrWhiteSpace(password))  throw new FeedbackException("Password is not set");

            if (password.Length < 8)  throw new FeedbackException("Password must contain at least 8 characters");
            if (Regex.Matches(password, "[0-9]").Count < 1)  throw new FeedbackException("Password must contain at least one number");
            if (Regex.Matches(password, "[A-Z]").Count < 1)  throw new FeedbackException("Password must contain at least one capital letter");

            this.PasswordHash = StringHasher.GetHash(password);
        }

        /// <summary>
        /// Throws an UnauthorizedAccess Exception if the password is not valid
        /// </summary>
        public bool VerifyPassword(string password)
        {
            if(!StringHasher.VerifyHash(password, this.PasswordHash))
                throw new UnauthorizedAccessException();

            return true;
        }

    }

    public enum UserOrderByEnum
    {
        UserId = 1,
        Username = 2,
        Email = 3,
        FirstName = 4,
        LastName = 5,
        CurrentClientId = 6
    }
}
