﻿using Turing.Domain.Domain.Enums.Device;
using Turing.Domain.Models;

namespace Turing.Domain.Domain.Models.Device.Feature
{
    public class DeviceFeatureModel : IModel
    {
        public long DeviceFeatureId { get; private set; }
        public long DeviceId { get; private set; }
        public DeviceFeatureEnum Type { get; private set; }
        public string Name { get; set; }

        public long DimensionId { get; set; }

        private DeviceFeatureModel() { }
        public DeviceFeatureModel(long deviceId, DeviceFeatureEnum type, string name, long dimensionId = 0)
        {
            DeviceId = deviceId;
            Type = type;
            Name = name;
            DimensionId = dimensionId;
        }
    }
}
