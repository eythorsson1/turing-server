﻿using Turing.Domain.Domain.Enums.Device;
using Turing.Domain.Models;

namespace Turing.Domain.Domain.Models.Device.Feature.Data
{
    public class DeviceFeatureDataModel : IModel
    {
        public long DeviceFeatureDataId { get; private set; }
        public long DeviceFeatureId { get; }
        public long DeviceId { get; }
        public string Value { get; set; }

        public DeviceFeatureEnum Type { get; } // Aggregated
        public string Name { get; } // Aggregated

        private DeviceFeatureDataModel() { }

        public DeviceFeatureDataModel(long deviceId, long deviceFeatureId, string value)
        {
            DeviceId = deviceId;
            DeviceFeatureId = deviceFeatureId;
            Value = value;
        }
    }
}
