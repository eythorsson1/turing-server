﻿using Newtonsoft.Json;
using System.Linq;
using Turing.Domain.Models;

namespace Turing.Domain.Domain.Models.Device.Data
{
    public class DeviceDataModel : IModel
    {
        public long DeviceId { get; }
        public long DeviceFeatureId { get; }
        public long ParentDeivceTypeFieldId { get; } // Aggregated
        public string DataTypeFieldName { get; } // Aggregated
        public string DataPresetStr { get; private set; } // Aggregated

        public string ValueStr {
            get { return JsonConvert.SerializeObject(Value); }
            set { Value = JsonConvert.DeserializeObject(value); }
        }

        public object Value { get; set; }

        private DeviceDataModel() { }

        public DeviceDataModel(long deviceId, long deviceFeatureId, string value)
        {
            DeviceId = deviceId;
            DeviceFeatureId = deviceFeatureId;
            Value = value;
        }

        /// <summary>
        /// The Try Get Method is untested
        /// </summary>

        public bool TryGetData(string pathStr, out string data)
        {
            data = null;
            if (string.IsNullOrWhiteSpace(pathStr)) return false;
            else if (string.IsNullOrWhiteSpace(DataPresetStr)) return false;

            object value = Value;
            var parts = pathStr.Split(".");

            foreach (string part in parts)
                value = value.GetType().GetProperty(part);

            return true;
        }

        /// <summary>
        /// The Try Set Method is untested
        /// </summary>
        public bool TrySetData(string pathStr, string value)
        {
            if (string.IsNullOrWhiteSpace(pathStr)) return false;

            object val = Value;
            var parts = pathStr.Split(".");

            foreach (string part in parts) {
                var p = val.GetType().GetProperty(part);
                if (part == parts.Last() && val != null) p.SetValue(Value, value);
                else val = p;
            }

            return true;
        }
    }
}
