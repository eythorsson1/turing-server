﻿using System;
using System.Collections.Generic;
using Turing.Domain.Domain.Models.Data.Preset;
using Turing.Domain.Domain.Models.Device.Feature;
using Turing.Domain.Domain.Models.Device.Feature.Data;
using Turing.Domain.Infrastructure;
using Turing.Domain.Infrastructure.Helper;

namespace Turing.Domain.Models.Device
{
    public class DeviceModel : IModel
    {
        public long DeviceId { get; private set; }
        public string Name { get; set; }
        public string ChipId { get; set; }

        public string SetTopic { get; }
        public string StateTopic { get; }

        public string AuthName { get; }
        public string AuthPasswordHash { get; private set; } //36 bytes

        public long ClientId { get; }

        public long DataPresetId { get; set; }
        public DataPresetModel DataPreset { get; private set; }


        public List<DeviceFeatureModel> Features { get; private set; } // Aggregated
        public List<DeviceFeatureDataModel> FeatureData { get; private set; } // Aggregated

        public DateTime CreatedDateUTC { get; }
        public long CreatedByUserId { get; }
        public string CreatedByFirstName { get; } // Aggregated
        public string CreatedByLastName { get; } // Aggregated
        public string CreatedByEmail { get; } // Aggregated

        public DateTime LastModifiedDateUTC { get; }
        public long LastModifiedByUserId { get; }
        public string LastModifiedByFirstName { get; } // Aggregated
        public string LastModifiedByLastName { get; } // Aggregated
        public string LastModifiedByEmail { get; } // Aggregated

        private DeviceModel() { }

        public DeviceModel(string name, string authName, string authPassword, long clientId, long createdByUserId, string chipId = null)
        {
            this.Name = name;
            this.ClientId = clientId;
            this.CreatedDateUTC = DateTime.UtcNow;
            this.CreatedByUserId = createdByUserId;

            this.AuthName = authName;
            this.AuthPasswordHash = StringHasher.GetHash(authPassword);

            string guid = Guid.NewGuid().ToString();
            this.SetTopic = guid + "/set";
            this.StateTopic = guid + "/state";

            this.ChipId = chipId;
        }

        public string GenerateNewAuthPassword()
        {
            var authPassword = StringHelper.GenerateRandomString(10);
            this.AuthPasswordHash = StringHasher.GetHash(authPassword);
            return authPassword;
        }

        public bool ValidateAuthPassword(string password)
        {
            return StringHasher.VerifyHash(password, AuthPasswordHash);
        }
    }

    public enum DeviceOrderByEnum
    {
        DeviceId = 1,
        Name = 2,
        SetTopic = 4,
        StateTopic = 5,
        ClientId = 6,
        AuthName = 7,

        CreatedDateUTC = 8,
        CreatedByUserId = 9,
        CreatedByFullName = 10,
        CreatedByFirstName = 11,
        CreatedByLastName = 12,
        CreatedByEmail = 13,

        LastModifiedDateUTC = 14,
        LastModifiedByUserId = 15,
        LastModifiedByFullName = 16,
        LastModifiedByFirstName = 17,
        LastModifiedByLastName = 18,
        LastModifiedByEmail = 19,
    }
}
