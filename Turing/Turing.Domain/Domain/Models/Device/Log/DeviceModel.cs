﻿using System;
using System.Collections.Generic;
using System.Text;
using Turing.Domain.Enums.Device;
using Turing.Domain.Infrastructure.Extensions;

namespace Turing.Domain.Models.Device.Log
{
    public class DeviceLogModel : IModel
    {
        public long DeviceLogId { get; }
        public long DeviceId { get; }

        public DeviceLogActionEnum Action { get; }
        public DateTime ActionDateUTC { get; }
        public long ActionByUserId { get; }
        public string ActionByEmail { get; }
        public string ActionByFirstName { get; }
        public string ActionByLastName { get; }

        public string Description { get; }
        public string TechnicalDescription { get; }

        public long ClientId { get; }

        // Aggregated
        public string DeviceName { get; }
        public string ActionName => Action.GetDescription();

        public DeviceLogModel(long deviceId, DeviceLogActionEnum action, string description,  string technicalDescription, long clientId,
            long actionByUserId = 0, string actionByEmail = "", string actionByFirstName = "System", 
            string actionByLastName = "")
        {
            DeviceId = deviceId;
            ClientId = clientId;
            Action = action;

            ActionDateUTC = DateTime.UtcNow;
            ActionByUserId = actionByUserId;
            ActionByEmail = actionByEmail;
            ActionByFirstName = actionByFirstName;
            ActionByLastName = actionByLastName;

            Description = description;
            TechnicalDescription = technicalDescription;
        }

        public void Validate()
        {
            if (DeviceId < 1) throw new ArgumentNullException("DeviceId is not set");

            if (ActionDateUTC == DateTime.MinValue) throw new ArgumentNullException("ActionDateUTC is not set");

            if (ActionByUserId > 0) {
                if (string.IsNullOrWhiteSpace(ActionByEmail)) throw new ArgumentNullException("ActionByEmail is not set");
                if (string.IsNullOrWhiteSpace(ActionByFirstName)) throw new ArgumentNullException("ActionByFirstName is not set");
                if (string.IsNullOrWhiteSpace(ActionByLastName)) throw new ArgumentNullException("ActionByLastName is not set");
            }

            if (string.IsNullOrWhiteSpace(Description)) throw new ArgumentNullException("Description is not set");
        }
    }

    public enum DeviceLogOrderByEnum
    {
        DeviceLogId = 1,
        DeviceId = 2,
        Action = 3,
        ActionDateUTC = 4,
        ActionByUserId = 5,
        ActionByEmail = 6,
        ActionByFirstName = 7,
        ActionByLastName = 8,
        Description = 9,
        TechnicalDescription = 10,
        ClientId = 11,

        DeviceName = 12,
    }
}
