﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Turing.Core.Infrastructure.Mqtt.Exceptions;
using Turing.Core.Request.Device;
using Turing.Core.Request.Device.Feature;
using Turing.Core.Request.Device.Feature.Data;
using Turing.Core.Request.Dimension;
using Turing.Core.Services.Device;
using Turing.Core.Services.Device.Feature;
using Turing.Core.Services.Device.Type.Feature.Data;
using Turing.Core.Services.Dimension;
using Turing.Domain.Domain.Models.Device.Feature;
using Turing.Domain.Domain.Models.Device.Feature.Data;
using Turing.Domain.Domain.Models.Dimension;
using Turing.Domain.Exceptions;
using Turing.Domain.Infrastructure.Extensions;
using Turing.Domain.Infrastructure.Helper;
using Turing.Domain.Models.Device;
using Turing.Web.Configuration.AutoMapper;
using Turing.Web.Dto.Device;

namespace Turing.Web.Controller.Device
{
    [Route("api/device")]
    public class DeviceController : BaseController
    {
        private DeviceService DeviceService => Services.DeviceService;
        private DeviceFeatureService DeviceFeatureService => Services.DeviceFeatureService;
        private DeviceFeatureDataService DeviceFeatureDataService => Services.DeviceFeatureDataService;
        private DimensionService DimensionService => Services.DimensionService;

        [HttpGet("lookup")]
        public IActionResult GetLookup([FromQuery] DeviceRequest request)
        {
            if (request == null) return BadRequest();
            request.ClientId = CurrentUser.CurrentClientId;
            return Ok(DeviceService.GetLookup(request));
        }

        [HttpGet("all")]
        public IActionResult GetList([FromQuery] DeviceRequest request)
        {
            if (request == null) request = new DeviceRequest();
            request.ClientId = CurrentUser.CurrentClientId;
            var models = DeviceService.GetList(request);

            var deviceData = new List<DeviceFeatureDataModel>();
            var deviceFeatures = new List<DeviceFeatureModel>();
            var dimensions = new List<DimensionModel>();

            if (!models.Empty()) {
                deviceData = DeviceFeatureDataService.GetList(new DeviceFeatureDataRequest { DeviceIds = models.SelectList(x => x.DeviceId) });
                deviceFeatures = DeviceFeatureService.GetList(new DeviceFeatureRequest { DeviceIds = models.SelectList(x => x.DeviceId) });
            }
            if (!deviceData.Empty()) dimensions = DimensionService.GetList(new DimensionRequest { DimensionIds = deviceFeatures.SelectList(x => x.DimensionId) }, includeRegister: true);

            var featureLook = deviceFeatures.ToLookup(x => x.DeviceId);
            var deviceDataLook = deviceData.ToLookup(x => x.DeviceId);


            var dtoList = models.MapTo<DeviceDto>();

            foreach (var dto in dtoList) {
                dto.Features = featureLook[dto.DeviceId].MapTo<DeviceFeatureDto>();
                dto.Data = deviceDataLook[dto.DeviceId].ToDictionary(x => x.DeviceFeatureId, x => x.Value);
            }

            return Ok(dtoList);
        }

        [HttpPost("")]
        public IActionResult Insert([FromBody] DeviceDto dto)
        {
            dto.Name = dto.Name.Trim();
            string deviceAuthName = dto.Name.Replace(" ", "_");
            string deviceAuthPass = StringHelper.GenerateRandomString(12);

            var model = new DeviceModel(dto.Name, deviceAuthName, deviceAuthPass, CurrentUser.CurrentClientId, CurrentUser.CurrentClientId);

            return Ok(new { DeviceAuthName = deviceAuthName, DeivceAuthPassword = deviceAuthPass });
        }


        [HttpPost("{deviceId}/set")]
        public async Task<IActionResult> SetDevice([FromRoute] long deviceId, [FromBody] Dictionary<long, string> dataDto)
        {
            if (deviceId < 1) return BadRequest();
            if (dataDto.Empty()) return BadRequest();

            var device = DeviceService.GetById(deviceId);
            if (device?.ClientId != CurrentUser.CurrentClientId) return BadRequest();


            try {
                await DeviceService.SetState(deviceId, dataDto, CurrentUser.UserId);
                return Ok(dataDto);
            }
            catch (MqttException) {
                dataDto = DeviceFeatureDataService.GetList(new DeviceFeatureDataRequest { DeviceId = deviceId })
                    .ToDictionary(x => x.DeviceFeatureId, x => x.Value);

                return Ok(dataDto);
            }
            catch (FeedbackException ex) {
                // LogService.LogException();
                return BadRequest(ex.Message);
            }
            catch (Exception) {
                // LogService.LogException(ex);
                return BadRequest();
            }
        }



        //[HttpGet("parseTest")]
        //public IActionResult ParseTest()
        //{
        //    // 1. Create the fields list from the presetData
        //    // 2. Parse the request
        //    // 3. Create a response from the parsed request

        //    // 4. Validate the parsed request


        //    var presetData = JsonConvert.DeserializeObject("{ \"RightColor\": { \"R\": \"{{ColorPicker_Right.Red}}\", \"G\": \"{{ColorPicker_Right.Green}}\", \"B\": \"{{ColorPicker_Right.Blue}}\", }, \"ColorBrightness\": \"{{Brightness1}}\", \"Effect\": \"{{Select_Effect}}\", \"LeftMonitor\": { \"Color\": { \"Hue\": \"{{ColorPicker_LeftMonitor.Hue}}\", \"Saturation\": \"{{ColorPicker_LeftMonitor.Saturation}}\", \"Value\": \"{{ColorPicker_LeftMonitor.Value}}\", \"ColorPreset\": \"{{Select_LeftColorPreset}}\" }, \"State\": { \"Power\": \"{{PowerState_LeftMonitor}}\", \"Effect\": \"{{Select_LeftEffect}}\", \"Speed\": \"{{Slider_Speed}}\" } }}");


        //    //object request = JsonConvert.DeserializeObject("{ \"RightColor\": { \"R\": 100, \"G\": 150, \"B\": 229 } , \"ColorBrightness\": 100, \"Effect\": 2, \"LeftMonitor\": { \"Color\": { \"Hue\": 123, \"Saturation\": 60, \"Value\": 80, \"ColorPreset\": 1 }, \"State\": { \"Power\": true, \"Effect\": 4 } } }");

        //    object request = JsonConvert.DeserializeObject("{\"ColorPicker_Right\":{\"Red\":100,\"Green\":150,\"Blue\":229},\"Brightness1\":100,\"Select_Effect\":2,\"ColorPicker_LeftMonitor\":{\"Hue\":123,\"Saturation\":60,\"Value\":80},\"Select_LeftColorPreset\":1,\"PowerState_LeftMonitor\":true,\"Select_LeftEffect\":4}");


        //    long deviceTypeId = 1;


        //    var features = new List<DeviceTypeFeatureModel>() {
        //        new DeviceTypeFeatureModel(deviceTypeId, DeviceTypeFeatureEnum.ColorPicker, "ColorPicker_Right"),
        //        new DeviceTypeFeatureModel(deviceTypeId, DeviceTypeFeatureEnum.Brightness, "Brightness1"),
        //        new DeviceTypeFeatureModel(deviceTypeId, DeviceTypeFeatureEnum.Select, "Select_Effect"),
        //        new DeviceTypeFeatureModel(deviceTypeId, DeviceTypeFeatureEnum.ColorPicker, "ColorPicker_LeftMonitor"),
        //        new DeviceTypeFeatureModel(deviceTypeId, DeviceTypeFeatureEnum.ColorShema, "Select_LeftColorPreset"),
        //        new DeviceTypeFeatureModel(deviceTypeId, DeviceTypeFeatureEnum.PowerState, "PowerState_LeftMonitor"),
        //        new DeviceTypeFeatureModel(deviceTypeId, DeviceTypeFeatureEnum.Select, "Select_LeftEffect"),
        //        new DeviceTypeFeatureModel(deviceTypeId, DeviceTypeFeatureEnum.Slider, "Slider_Speed")
        //    };

        //    var featureDict = features.ToDictionary(x => x.Name);

        //    var presetPlaceholderRegex = new Regex(@"\{\{(.*?)\}\}");

        //    // Validate the preset



        //    //var mqttRequest = new Regex(@"\{\{(.*?)\}\}").Replace(presetData.ToString(), delegate (Match m) {
        //    //    var itemString = m.Groups[1].ToString();
        //    //    var parts = itemString.Split(".");
        //    //});


        //    return Ok();
        //}
    }
}
