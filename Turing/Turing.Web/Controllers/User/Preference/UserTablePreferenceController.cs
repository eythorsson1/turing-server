﻿using Microsoft.AspNetCore.Mvc;
using System;
using Turing.Core.Request.Automation;
using Turing.Core.Services.User.Preference;
using Turing.Domain.Domain.Enums.User;
using Turing.Domain.Domain.Models.User.Preference;
using Turing.Web.Configuration.AutoMapper;
using Turing.Web.Dto.User;

namespace Turing.Web.Controller.User.Security
{
    [Route("api/user/tablePreference")]
    public class UserTablePreferenceController : BaseController
    {
        private UserTablePreferenceService UserTablePreferenceService => Services.UserTablePreferenceService;

        [HttpGet("{tableId}")]
        public IActionResult GetColumnOrder([FromRoute] UserTablePreferenceEnum tableId)
        {
            if (tableId == 0) return BadRequest();

            var tableColumnEnumType = GetTableOrderByEnum(tableId);

            // GENERATE THE GET BY ID METHOD WITH THE TableColumnsEnum CORRESPONDING THE INSERTED tablePreferenceId
            var method = typeof(UserTablePreferenceService).GetMethod("GetById");
            method = method.MakeGenericMethod(tableColumnEnumType);

            var model = method.Invoke(UserTablePreferenceService, new object[] { CurrentUser.UserId, tableId });

            var response = model.GetType().GetProperty("ColumnOrder").GetValue(model);
            return Ok(response);
        }

        [HttpPost("{tableId}")]
        public IActionResult UpdateColumnOrder([FromRoute] UserTablePreferenceEnum tableId, [FromBody] string columnOrderStr)
        {
            if (tableId == 0) return BadRequest();

            var tableColumnEnumType = GetTableOrderByEnum(tableId);


            var modelType = typeof(UserTablePreferenceModel<>).MakeGenericType(tableColumnEnumType);
            var model = Activator.CreateInstance(modelType, new { UserId = CurrentUser.UserId, TableId = tableId, ColumnOrderStr = columnOrderStr });


            // GENERATE THE Update METHOD WITH THE TableColumnsEnum CORRESPONDING THE INSERTED tablePreferenceId
            var method = typeof(UserTablePreferenceService).GetMethod("Update");
            method = method.MakeGenericMethod(tableColumnEnumType);

            bool success = (bool)method.Invoke(UserTablePreferenceService, new object[] { model });

            if (success) return Ok();
            else return BadRequest();
        }

        [HttpPost("{tableId}/restore")]
        public IActionResult RestoreColumnOrder([FromRoute] UserTablePreferenceEnum tableId)
        {
            if (tableId == 0) return BadRequest();

            var tableColumnEnumType = GetTableOrderByEnum(tableId);

            // GENERATE THE Delete METHOD WITH THE TableColumnsEnum CORRESPONDING THE INSERTED tablePreferenceId
            var method = typeof(UserTablePreferenceService).GetMethod("Delete");
            method = method.MakeGenericMethod(tableColumnEnumType);

            method.Invoke(UserTablePreferenceService, new object[] { CurrentUser.UserId, tableId });

            // GENERATE THE GET BY ID METHOD WITH THE TableColumnsEnum CORRESPONDING THE INSERTED tablePreferenceId
            method = typeof(UserTablePreferenceService).GetMethod("GetById");
            method = method.MakeGenericMethod(tableColumnEnumType);

            var model = method.Invoke(UserTablePreferenceService, new object[] { CurrentUser.UserId, tableId });

            var response = model.GetType().GetProperty("ColumnOrder").GetValue(model);
            return Ok(response);
        }



        private Type GetTableOrderByEnum(UserTablePreferenceEnum tableId)
        {
            switch (tableId) {
                case UserTablePreferenceEnum.AutomationTable: return typeof(AutomationOrderByEnum);

                default: throw new NotImplementedException("The TableId is not implemented");
            }
        }
    }
}
