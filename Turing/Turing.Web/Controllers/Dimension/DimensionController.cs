﻿using Microsoft.AspNetCore.Mvc;
using Turing.Core.Request.Dimension;
using Turing.Core.Services.Dimension;
using Turing.Domain.Domain.Models.Dimension;
using Turing.Web.Configuration.AutoMapper;
using Turing.Web.Dto.Automaiton;
using Turing.Web.Dto.Dimension;

namespace Turing.Web.Controller.Dimension
{
    [Route("api/dimension")]
    public class DimensionController : BaseController
    {
        DimensionService DimensionService => Services.DimensionService;

        //[HttpGet("lookup")]
        //public IActionResult GetLookup([FromQuery] DimensionRequest request)
        //{
        //    if (request == null) return BadRequest();
        //    request.ClientId = CurrentUser.CurrentClientId;

        //    DimensionService.
        //}

        //[HttpGet("")]
        //public IActionResult GetPagedList([FromQuery] DimensionRequest request)
        //{
        //    if (request == null) return BadRequest();
        //    request.ClientId = CurrentUser.CurrentClientId;

        //    var pagedList = DimensionService.GetPagedList(request, includeRegister: true);
        //    return Ok(pagedList.MapTo<DimensionDto>());
        //}

        [HttpGet("all")]
        public IActionResult GetList([FromQuery] DimensionRequest request)
        {
            if (request == null) return BadRequest();
            request.ClientId = CurrentUser.CurrentClientId;

            var list = DimensionService.GetList(request, includeRegister: true);
            return Ok(list.MapTo<DimensionDto>());
        }

        [HttpPost("{dimensionId}")]
        public IActionResult GetById([FromQuery] long dimensionId)
        {
            if (dimensionId < 1) return BadRequest();

            var model = DimensionService.GetById(dimensionId);

            if (model?.ClientId == CurrentUser.CurrentClientId) 
                return Ok(model.MapTo<DimensionDto>());
            else
                return BadRequest();
        }


        //[HttpPost("")]
        //public IActionResult Create([FromBody] DimensionDto dto)
        //{
        //    if (dto == null) return BadRequest();

        //    var model = new DimensionModel(dto.Type, CurrentUser.CurrentClientId);

        //    var dimensionId = DimensionService.Insert(model);
        //    return Ok(dimensionId);
        //}

        //[HttpPost("{dimensionId}")]
        //public IActionResult Update([FromQuery] long dimensionId, [FromBody] DimensionDto dto)
        //{
        //    if (dto == null || dimensionId != dto.DimensionId) return BadRequest();

        //    var model = DimensionService.GetById(dimensionId);
        //    model.Name = dto.Name;

        //    DimensionService.Update(model);
        //}
    }
}
