﻿using Hangfire;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Turing.Web.Dto.Automaiton;
using Turing.Core.Request.Automation.Item;
using Turing.Core.Services.Automation;
using Turing.Core.Services.Automation.Item;
using Turing.Domain.Aggregated.Automation;
using Turing.Domain.Enums.Automation;
using Turing.Domain.Enums.Automation.AutomationItemType;
using Turing.Domain.Models.Automation;
using Turing.Core.Request.Automation;
using Turing.Web.Configuration.AutoMapper;
using static Turing.Web.Configuration.AutoMapper.AutoMapperConfig;

namespace Turing.Web.Controller.Automation
{
    [Route("api/automation")]
    public class AutomationController : BaseController
    {

        private AutomationService AutomationService => Services.AutomationService;
        private AutomationTriggerService AutomationTriggerService => Services.AutomationTriggerService;


        [HttpGet("all")]
        public IActionResult GetList(AutomationRequest request)
        {
            if (request == null) return BadRequest();
            request.ClientId = CurrentUser.CurrentClientId;

            var list = AutomationService.GetList(request);
            return Ok(list.MapTo<AutomationItemDto>());
        }

        [HttpGet("")]
        public IActionResult GetPagedList(AutomationRequest request)
        {
            if (request == null) return BadRequest();
            request.ClientId = CurrentUser.CurrentClientId;

            var pagedList = AutomationService.GetPagedList(request);
            return Ok(pagedList.MapTo<AutomationDto, AutomationModel>());
        }


        [HttpPost("")]
        public IActionResult Create([FromBody] AutomationDto dto)
        {
            if (dto == null) return BadRequest();

            var model = new AutomationModel(dto.Name, CurrentUser.UserId, CurrentUser.CurrentClientId);

            var itemModel = dto.Items.Select(x => (x.AutomationItemId, x.ParentAutomationItemId, x.AutomationType, x.Type, x.Data)).ToList();

            var automationId = AutomationService.Insert(model, itemModel);

            // If a scheduled trigger was included in the automation must we schedule the item with HangFire 
            var scheduleTriggers = AutomationTriggerService.GetList(
                new AutomationTriggerRequest {
                    AutomationId = automationId,
                    Type = AutomationTriggerEnum.Schedule,
                    ClientId = CurrentUser.CurrentClientId
                });

            if (scheduleTriggers != null && scheduleTriggers.Any()) {
                foreach (var schedule in scheduleTriggers) {
                    var item = schedule.GetData<AutomationTrigger_Schedule>();
                    RecurringJob.AddOrUpdate(
                        schedule.AutomationItemId.ToString(),
                        () => AutomationService.TriggerAutomation(schedule.AutomationItemId, null),
                    item.CronExpression
                    );
                }
            }

            return Ok();
        }

        [HttpPost("{automationId}/delete")]
        public IActionResult Delete([FromQuery] long automationId)
        {
            if (automationId < 1) return BadRequest();

            var model = AutomationService.GetById(automationId);
            if (model.ClientId != CurrentUser.CurrentClientId) return BadRequest();

            AutomationService.Delete(automationId); 


            if (automationId < 0)
                RecurringJob.RemoveIfExists("DEBUG-TEST-JOB");

            return Ok();
        }


        [HttpPost("{automationId}/trigger")]
        public IActionResult Trigger([FromQuery] long automationId)
        {
            if (automationId < 1) return BadRequest();

            var triggers = AutomationTriggerService.GetList(
                new AutomationTriggerRequest {
                    AutomationId = automationId,
                    ClientId = CurrentUser.CurrentClientId
                });

            foreach (var trigger in triggers) {
                RecurringJob.Trigger(trigger.AutomationItemId.ToString());
            }

            return Ok();
        }
    }
}
