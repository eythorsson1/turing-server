using Hangfire;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Turing.Core;
using Turing.Core.Service;
using Turing.Domain.Domain.Enums.User;
using Turing.Web.Configuration.AutoMapper;
using VueCliMiddleware;

namespace Turing.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                .AddNewtonsoftJson(opt => opt.SerializerSettings.ContractResolver = new DefaultContractResolver());
                //.AddJsonOptions(opt => opt.JsonSerializerOptions.PropertyNamingPolicy = null);

            services.AddSpaStaticFiles(configuration => {
                configuration.RootPath = "App";
            });

            // INITIALIZE THE SERVICES
            var serviceContext = new ServiceContext(Configuration.GetConnectionString("TURING"));
            TuringAppContext.Current = new TuringAppContext(serviceContext);


            // HANGFIRE
            services.AddHangfire(configuration => configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseSqlServerStorage(Configuration.GetConnectionString("HANGFIRE"), new SqlServerStorageOptions {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    DisableGlobalLocks = true
                }));

            services.AddHangfireServer();


            // SETUP COOKIE AUTHENTICATION
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options => {
                    options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                    options.Cookie.Name = "Turing.Identity";
                    options.Cookie.HttpOnly = true;

                    options.LoginPath = string.Empty;
                    options.LogoutPath = string.Empty;
                    options.AccessDeniedPath = string.Empty;

                    options.Events = new CookieAuthenticationEvents {
                        OnRedirectToLogin = ctx => {
                            ctx.Response.StatusCode = StatusCodes.Status401Unauthorized;
                            return Task.FromResult(0);
                        }
                    };
                });


            services.SetupAutoMapper();


            //services.SetupEmailService(Configuration, localhost: true);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseRouting();
            //app.UseSpaStaticFiles();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });

            //app.UseSpa(spa => {
            //    if (env.IsDevelopment())
            //        spa.Options.SourcePath = "App/";
            //    else
            //        spa.Options.SourcePath = "dist";

            //    if (env.IsDevelopment()) {
            //        spa.UseVueCli(npmScript: "serve");
            //    }

            //});
        }
    }
}
