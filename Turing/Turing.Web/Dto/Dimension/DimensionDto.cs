﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Turing.Domain.Domain.Enums.Dimension;

namespace Turing.Web.Dto.Dimension
{
    public class DimensionDto
    {
        public long DimensionId { get; set; }
        public DimensionTypeEnum Type { get; }
        public long ClientId { get; }

        public List<DimensionRegisterDto> Register { get; set; }
    }
}
