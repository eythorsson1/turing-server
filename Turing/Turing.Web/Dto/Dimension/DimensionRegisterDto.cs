﻿using System.Linq;
using Turing.Domain.Domain.Enums.Dimension;
using Turing.Domain.Infrastructure.Extensions;

namespace Turing.Web.Dto.Dimension
{
    public class DimensionRegisterDto
    {
        public long DimensionRegisterId { get; set; }
        public DimensionTypeEnum Type { get; }
        public string Code { get; set; }
        public string Description { get; set; }

        public string Value => new string[] { Code, Description }.Where(x => !string.IsNullOrWhiteSpace(x)).Join(" ");
    }
}
