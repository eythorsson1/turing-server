﻿using System.Collections.Generic;

namespace Turing.Web.Dto.Device
{
    public class DeviceDto
    {
        public long DeviceId { get; set; }
        public string Name { get; set; }
        public string DataPresetTempalte { get; set; }

        public Dictionary<long, string> Data { get; set; }
        public List<DeviceFeatureDto> Features { get; set; }
    }
}
