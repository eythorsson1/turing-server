﻿using Turing.Domain.Domain.Enums.Device;

namespace Turing.Web.Dto.Device
{
    public class DeviceFeatureDto
    {
        public long DeviceFeatureId { get; set; }
        public long DeivceTypeId { get; set; }
        public DeviceFeatureEnum Type { get; set; }
        public string Name { get; set; }

        public long DimensionId { get; set; }
    }
}