﻿using Turing.Domain.Domain.Enums.Device;

namespace Turing.Web.Dto.Device
{
    public class DeviceFeatureDataDto
    {
        public long DeviceFeatureDataId { get; set; }
        public long DeviceId { get; set; }
        public long DeviceFeatureId { get; set; }
        public string Value { get; set; }
    }
}
