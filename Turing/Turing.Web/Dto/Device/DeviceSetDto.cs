﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Turing.Web.Dto.Device
{
    public class DeviceSetDto
    {
        public Dictionary<string, object> Data { get; set; }
    }
}
