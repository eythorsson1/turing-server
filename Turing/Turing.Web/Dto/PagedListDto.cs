﻿using System;
using System.Collections.Generic;

namespace Turing.Web.Dto
{
    public class PagedListDto<T>
    {
        public List<T> Items { get; set; }
        public long CurrentPage { get; }
        public long TotalNumberOfItems { get; }
        public long ItemsPerPage { get; }
        public long TotalNumberOfPages => (long)Math.Ceiling((decimal)TotalNumberOfItems / ItemsPerPage);

        public PagedListDto(List<T> items, long count, long currentPage, long itemsPerPage)
        {
            this.Items = items;
            this.TotalNumberOfItems = count;
            this.CurrentPage = currentPage;
            this.ItemsPerPage = itemsPerPage;
        }
    }
}
