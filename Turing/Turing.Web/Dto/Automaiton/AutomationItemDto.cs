﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Turing.Domain.Enums.Automation;
using Turing.Domain.Enums.Automation.AutomationItemType;
using Turing.Domain.Models.Automation.Item;
using Turing.Domain.Infrastructure.Extensions;

namespace Turing.Web.Dto.Automaiton
{
    public class AutomationItemDto
    {
        public long AutomationItemId { get; set; }
        public long ParentAutomationItemId { get; set; } // Aggregated

        public AutomationTypeEnum AutomationType { get; set; }
        public int Type { get; set; }
        public object Data { get; set; }

        public string AutomationDescription => AutomationType.GetDescription();

        public string TypeDescription {
            get {
                string description = "";

                Type type = null;
                if (AutomationType == AutomationTypeEnum.Action) type = typeof(AutomationActionEnum);
                else if (AutomationType == AutomationTypeEnum.Action) type = typeof(AutomationActionEnum);
                else if (AutomationType == AutomationTypeEnum.Action) type = typeof(AutomationActionEnum);

                if (type != null && Enum.TryParse(type, Type.ToString(), out object enumVal))
                    description = enumVal.GetDescription();

                return description;
            }
        }
    }
}
