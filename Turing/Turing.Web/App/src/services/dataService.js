//import apiService  from './apiService';
var apiService = require('./apiService').default;
import tableColumnService from './tableColumnService';

const automation = {
    Trigger: (automationId) => apiService.Post("automation/" + automationId + "/trigger"),
    GetList: (filter) => apiService.Get("automation/all", filter),
    GetPagedList: (filter) => apiService.Get("automation", filter),
    //Insert: (item) => apiService.Post("automation", item),
    //Update: (item) => apiService.Post("automation/" + item.AutomationId, item),
    Delete: (automationId) => apiService.Post("automation/" + automationId + "/delete"),

    //Item: {
    //    GetList: (filter) => apiService.Get("automationItem/all", filter),
    //    GetPagedList: (filter) => apiService.Get("automationItem", filter),
    //}
};

const device = (function () {
    var base = function (deviceId) {
        return {
            Set: (request) => apiService.Post("device/" + deviceId + "/set", request),
        };
    };

    base.GetLookup = (filter) => apiService.Get("device/lookup", filter);
    base.GetPagedList = (filter) => apiService.Get("device", filter);
    base.GetList = (filter) => apiService.Get("device/all", filter);

    return base;
})();

const dimension = (function(){
    var base = {};

    base.GetPagedList = (filter) => apiService.Get("dimension", filter);
    base.GetList = (filter) => apiService.Get("dimension/all", filter);


    return base;
})();

const user = (function () {
    var base = {};

    base.TablePreference = (function (tableId) {
        // THE tablepreferenceStoreLink IS USED TO GET THE CORRECT PREFERENCE STORE DATA BASED ON THE TableId
        var tablePreferenceStoreLink = {
            1: "Automation",
        }

        // MERGE INN THE USER PREFERENCE DATA WITH THE CORRECT STORE DATA SO THAT THE USERS COLUMN 
        // PREFERENCE IS MERGED WITH IT.
        function formatTableColumns(columnOrder) {
            var tableStoreName = tablePreferenceStoreLink[tableId];
            var tableStore = tableColumnService[tableStoreName];

            return columnOrder
                .filter(function (x) { return tableStore[x.ColumnId] !== undefined })
                .map(function (x) { return { ...x, ...tableStore[x.ColumnId] } });
        }

        return {
            GetById: () => apiService.Get("user/tablePreference/" + tableId).then(formatTableColumns),
            Update: (columnOrder) => apiService.Post("user/tablePreference/" + tableId, JSON.stringify(columnOrder)),
            Restore: () => apiService.Post("user/tablePreference/" + tableId + "/restore", null).then(formatTableColumns),
        }
    });

    return base;
});


export default {
    Automation: automation,
    Device: device,
    Dimension: dimension,
    User: user,
};