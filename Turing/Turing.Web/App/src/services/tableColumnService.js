////var _defaults = {
////    CreatedDate: { },
////    CreatedBy: { HeaderText: "Created By", FilterType: "select", FilterName: "CreatedByUserIds", Style: { "width": "10rem" } },
////    DeletedBy: { HeaderText: "Deleted By", FilterType: "select", FilterName: "DeletedByUserIds", Style: { "width": "10rem" } },
////}

// { HeaderText, Type, FilterType, FilterName, Class, HeaderClass, FilterClass, BodyClass, Style  }
// TableId: 1

const Automation = {
    1: { HeaderText: "Name"},
    2: { HeaderText: "Created", FilterType: "shortdate", BodyType: "shortdate" },
    3: { HeaderText: "Created By"  },
}

export default {
    Automation,
}
