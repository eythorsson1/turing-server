//import Vue from 'vue'
import { createApp } from 'vue'
import { store } from './stores/storeContext';
import App from './App.vue'

import router from './router/router';

// Styleing
import './assets/style/site.scss'

// Directives
import ClickOutsideDirective from '@/directives/ClickOutside';
import OnSubmitDirective from '@/directives/OnSubmit';
//Vue


// GLOBAL COMPONENTS
// GLOBAL COMPONENTS
import inputGroup from './components/Form/Inputs/InputGroup';
import inputText from './components/Form/Inputs/InputText';
import inputPassword from './components/Form/Inputs/InputPassword';
import inputCheckbox from './components/Form/Inputs/InputCheckbox';
import inputSwitch from './components/Form/Inputs/InputSwitch';

import inputSlider from './components/Form/Inputs/inputSlider.vue';
//import DataPresetEditor from './components/Form/DataPresetEditor/DataPresetEditor.vue';

import ColorWheel from './components/ColorWheel/ColorWheel.vue';

import FlowDesigner from './components/FlowDesigner/FlowDesigner.vue';

import modal from './components/Modal/Modal.vue';
import tableElement from './components/Table/Table.vue';
//import tableElement from './components/Table/Table1.vue';
//import tabContainer from './components/Tab/TabContainer';
//import tabContent from './components/Tab/TabContent';
//import tabItem from './components/Tab/TabItem';

// PLUGINS
import { SetupCalendar, /*Calendar,*/ DatePicker } from 'v-calendar';

const vueApp = createApp(App);
vueApp.component('InputGroup', inputGroup)
vueApp.component('InputText', inputText)
vueApp.component('InputSlider', inputSlider)
vueApp.component('InputPassword', inputPassword)
vueApp.component('InputCheckbox', inputCheckbox)
vueApp.component('InputSwitch', inputSwitch)

vueApp.component('ColorWheel', ColorWheel)
//vueApp.component('DataPresetEditor', DataPresetEditor)

vueApp.component('modalElement', modal)

vueApp.component('FlowDesigner', FlowDesigner)
//vueApp.component('tabContainer', tabContainer)
//vueApp.component('tabContent', tabContent)
//vueApp.component('tabItem', tabItem)
vueApp.component('tableElement', tableElement)
vueApp.directive('click-outside', ClickOutsideDirective)
vueApp.directive('on-submit', OnSubmitDirective)
vueApp.use(store)
vueApp.use(router)

// PLUGINS
vueApp.use(SetupCalendar, {});
// Use the components
//vueApp.component('Calendar', Calendar);
vueApp.component('DatePicker', DatePicker);


vueApp.mount('#app');

// Services
import apiService from './services/apiService';
import dataService from './services/dataService';
import dayjs from "dayjs";

vueApp.config.globalProperties.$apiService = apiService
vueApp.config.globalProperties.$dataService = dataService;
vueApp.config.globalProperties.$date = dayjs;
