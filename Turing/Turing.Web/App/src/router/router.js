﻿import { store } from '../stores/storeContext';

import { createWebHistory, createRouter } from 'vue-router';

const router = createRouter({
    //base: process.env.BASE_URL,
    history: createWebHistory(),
    routes: [
        {
            path: '/Login',
            name: 'Login',
            component: () => import('../pages/Login.vue'),
            meta: {
                AllowAnonymous: true,
                PreventVerifyed: true,
            }
        },
        {
            path: '/',
            name: '',
            component: () => import('../layouts/default.vue'),
            children: [
                {
                    path: '',
                    name: 'Dashboard',
                    component: () => import('../pages/Dashboard.vue'),
                    meta: {
                        AllowAnonymous: false,
                        RequiredPremissions: [1]
                    },
                    //children: [ { path, name, component }]
                },
                {
                    path: '/Device',
                    name: 'DeviceList',
                    component: () => import('../pages/Device/DeviceList.vue'),
                    meta: {
                        AllowAnonymous: false,
                        RequiredPremissions: [2]
                    },
                    //children: [ { path, name, component }]
                },
                {
                    path: '/Automation',
                    name: 'AutomationList',
                    component: () => import('../pages/Automation/List.vue'),
                    meta: {
                        AllowAnonymous: false,
                        RequiredPremissions: [3]
                    },
                    //children: [ { path, name, component }]
                },
                {
                    path: '/Automation/:id',
                    name: 'AutomationDetails',
                    component: () => import('../pages/Automation/Details.vue'),
                    meta: {
                        AllowAnonymous: false,
                        RequiredPremissions: [3]
                    },
                    //children: [ { path, name, component }]
                },
            ]
        }

    ],
});

// Gards
router.beforeEach((to, from, next) => {
    var isLoggedIn = store.getters['Auth/isAuthorized'];

    if (to.meta.PreventVerifyed && isLoggedIn) {
        return next({ path: "/" });
    }
    if (to.meta.AllowAnonymous || isLoggedIn) {
        next();
    }
    else {
        return next({ path: "/Login" });
    }
});

export default router;