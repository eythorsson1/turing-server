////const createElement = function (tag, attr) {
////    var item = document.createElementNS('http://www.w3.org/2000/svg', tag);

////    Object.keys(attr || {}).forEach(key => {
////        item.setAttributeNS(null, key, attr[key]);
////    })

////    return item;
////};

////var document = attr => createElement("svg", attr);

////document.Defs = attr => createElement("defs", attr);
////document.Path = attr => createElement("path", attr);
////document.Circle = attr => createElement("circle", attr);
////document.Pattern = attr => createElement("pattern", attr);
////document.Rect = attr => createElement("rect", attr);


export class SVGDocument {
    options = {
        Tag: "",
        Children: [], // [ { tag: children: [ ... ] } ]
    }
    context = undefined;

    constructor(ctx, options) {
        this.options = options;

        options.Tag = "svg";

        this.context = this.CreateElement(options);
        ctx.appendChild(this.context);
    }

    CreateElement(options) { // { Tag, < attributes > , Children }
        if (!options.Tag) throw "Tag is not set";
        var item = document.createElementNS('http://www.w3.org/2000/svg', options.Tag);

        Object.keys(options || {})
            .filter(x => x !== "Tag" && x !== "Children")
            .forEach(key => {
                var value = options[key];

                item.setAttributeNS(null, key, value);
            });

        options.Children?.forEach(x => item.append(this.CreateElement(x)));

        return item;
    }
}