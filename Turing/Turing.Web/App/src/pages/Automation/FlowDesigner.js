import { SVGDocument } from './utils.js';

export default class FlowDesigner {
    target = undefined;
    #options = {
        grid: {
            gap: 15,
            dotSize: .8,
            dotColor: 'none'
        },
        svgIcons: {

        }
    };
    #data = [];

    target = undefined;
    svg = undefined;

    constructor(target, options) {
        if (!target) throw "The target is not set";
        this.target = target;

        this.target.style.overflow = "hidden";
        this.target.style.width = "100%";
        this.target.style.height = "100%";
        this.target.style.touchAction = "none";

        if (options) this.Options = options;
        else this.Draw();


        window.addEventListener('resize', this.onResize.bind(this));
        this.target.addEventListener("contextmenu", this.onContextMenu.bind(this));
        this.target.addEventListener('wheel', this.onWheel.bind(this), { passive: false });
    }

    Draw() {
        var data = {
            "preserveAspectRatio": "xMinYMin",
            'style': 'background-color: #F3F5F7',
            Children: [
                {
                    Tag: 'defs',
                    Children: [{
                        Tag: 'pattern',
                        'id': 'pattern-circles',
                        'x': '0',
                        'y': '0',
                        'width': this.Options.grid.gap,
                        'height': this.Options.grid.gap,
                        'patternUnits': 'userSpaceOnUse',
                        'patternContentUnits': 'userSpaceOnUse',
                        Children: [{
                            Tag: 'circle',
                            'id': "pattern-circle",
                            'cx': (this.Options.grid.gap - this.Options.grid.dotSize) / 2,
                            'cy': (this.Options.grid.gap - this.Options.grid.dotSize) / 2,
                            'r': this.Options.grid.dotSize,
                            'fill': this.Options.grid.dotColor,
                        }]
                    }]
                },
                {
                    Tag: 'rect',
                    "id": "background",
                    "x": "0",
                    "y": "0",
                    "width": "100%",
                    "height": "100%",
                    "fill": "url(#pattern-circles)"
                }
            ]
        }; 

        //<svg width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
        //    <g filter="url(#filter0_d)">
        //        <rect x="4.5" y="4.11043" width="55" height="55" rx="27.5" fill="white" />
        //        <g clip-path="url(#clip0)">
        //            <path d="M24.1794 25.4774C24.1756 25.5673 24.1902 25.6571 24.2223 25.7412C24.2545 25.8252 24.3035 25.9019 24.3664 25.9663C24.4293 26.0307 24.5047 26.0816 24.5879 26.1159C24.6712 26.1501 24.7606 26.1669 24.8506 26.1653H27.1484C27.5328 26.1653 27.8391 25.8506 27.8893 25.469C28.1399 23.6419 29.3933 22.3106 31.627 22.3106C33.5377 22.3106 35.2868 23.2659 35.2868 25.5637C35.2868 27.3323 34.2451 28.1456 32.599 29.3822C30.7246 30.7442 29.2401 32.3345 29.3459 34.9164L29.3543 35.5208C29.3572 35.7035 29.4318 35.8778 29.5621 36.006C29.6924 36.1342 29.8678 36.206 30.0506 36.206H32.3094C32.494 36.206 32.6712 36.1326 32.8017 36.002C32.9323 35.8714 33.0057 35.6943 33.0057 35.5097V35.2172C33.0057 33.2174 33.766 32.6353 35.8187 31.0784C37.5149 29.7889 39.2835 28.3573 39.2835 25.352C39.2835 21.1436 35.7296 19.1104 31.8387 19.1104C28.3098 19.1104 24.444 20.7537 24.1794 25.4774V25.4774ZM28.5159 41.5285C28.5159 43.013 29.6996 44.1104 31.329 44.1104C33.0252 44.1104 34.1922 43.013 34.1922 41.5285C34.1922 39.9911 33.0224 38.9104 31.3262 38.9104C29.6996 38.9104 28.5159 39.9911 28.5159 41.5285Z" fill="#212529" />
        //        </g>
        //    </g>
        //    <defs>
        //        <filter id="filter0_d" x="0.5" y="0.110432" width="63" height="63" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        //            <feFlood flood-opacity="0" result="BackgroundImageFix" />
        //            <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
        //            <feOffset />
        //            <feGaussianBlur stdDeviation="2" />
        //            <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.05 0" />
        //            <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
        //            <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
        //        </filter>
        //        <clipPath id="clip0">
        //            <rect width="25" height="25" fill="white" transform="translate(19.5 19.1104)" />
        //        </clipPath>
        //    </defs>
        //</svg>




        this.svg = new SVGDocument(this.target, data);
        this.onResize();
        this.svg.context.style.position = "absolute";

        //this.target.style.backgroundColor = "#fdfdfd";
        //this.target.style.backgroundImage = "radial-gradient(circle, #ddd 1px, rgba(0,0,0,0) 1px)";
        //this.target.style.backgroundSize = "1rem 1rem";

        //background-color: #fdfdfd;
        //background-image: radial-gradient(circle, #ddd 1px, rgba(0,0,0,0) 1px);
        //background-size: 1rem 1rem;
    }

    onResize() {
        var height = this.target.offsetHeight;
        var width = this.target.offsetWidth;

        if (this.svg) {
            var svg = this.svg.context;
            svg.setAttributeNS(null, "height", height + "px");
            svg.setAttributeNS(null, "width", width + "px");

            svg.getAttributeNS(null, "viewBox") || "0 0 " + width + " " + height;
        }
    }

    onWheel(event) {
        event.preventDefault();
        event.stopPropagation();

        if (this.svg) {
            var svg = this.svg.context;

            if (!svg.getAttributeNS(null, "viewBox") || event.deltaY == 0) {
                var height = this.target.offsetHeight;
                var width = this.target.offsetWidth;

                svg.setAttributeNS(null, "viewBox", "0 0 " + width + " " + height);
            }

            //var viewBox = svg.viewBox.baseVal
            var viewBox = svg.getAttributeNS(null, "viewBox").split(" ");
            viewBox = {
                x: viewBox[0] * 1,
                y: viewBox[1] * 1,
                width: viewBox[2] * 1,
                height: viewBox[3] * 1,
            }

            if (event.ctrlKey) {
                if (event.deltaY > 0) {
                    // ZOOM OUT
                    //viewBox.x = viewBox.x - viewBox.width / 4;
                    //viewBox.y = viewBox.y - viewBox.height / 4;
                    viewBox.width = viewBox.width * 1.5;
                    viewBox.height = viewBox.height * 1.5;
                }
                else if (event.deltaY < 0) {
                    // ZOOM IN
                    //viewBox.x = viewBox.x + viewBox.width / 4;
                    //viewBox.y = viewBox.y + viewBox.height / 4;
                    viewBox.width = viewBox.width / 1.5;
                    viewBox.height = viewBox.height / 1.5;
                }
            }
            // Move
            else if (event.shiftKey) {
                if (viewBox.x - event.deltaY >= 0) 
                    viewBox.x = viewBox.x - event.deltaY;
            }
            else {
                if (viewBox.y - event.deltaY >= 0)
                    viewBox.y = viewBox.y - event.deltaY;
            }


            svg.setAttributeNS(null, "viewBox", viewBox.x + " " + viewBox.y + " " + viewBox.width + " " + viewBox.height)
        }
    }


    onContextMenu() {
       console.log(this.svg)
    }

    get Options() {
        return this.#options;
    }

    set Options(value) {
        if (!value) return;

        if (value.grid) {
            if (value.grid.gap) this.#options.grid.gap = value.grid.gap;
            if (value.grid.dotSize) this.#options.grid.dotSize = value.grid.dotSize;
            if (value.grid.dotColor) this.#options.grid.dotColor = value.grid.dotColor;
        }

        if (value.data) this.Data = value.data;
    }

    get Data() {
        return this.#data;
    }

    set Data(value) {
        this.#data = value;
        this.Draw();
    }
}