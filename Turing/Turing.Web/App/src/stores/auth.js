﻿import apiService from "../services/apiService"

export default {
	namespaced: true,

	state: () => ({
		user: null,
		status: null,
		isAuthorized: localStorage.getItem("isAuthorized") == "true",
	}),

	mutations: {
		auth_request(state) {
			state.status = { Name: 'auth loading', Success: undefined };

			localStorage.setItem("isAuthorized", undefined);
			state.isAuthorized = undefined;
		},
		auth_success(state) {
			state.status = { Name: 'auth success', Success: true };

			localStorage.setItem("isAuthorized", true);
			state.isAuthorized = true;
		},
		auth_error(state) {
			state.status = { Name: 'auth error', Success: false };

			localStorage.setItem("isAuthorized", false);
			state.isAuthorized = false;

			console.log(this)
		},

		signout(state) {
			state.status = null;
			state.user = null;
			state.isAuthorized = null;
			localStorage.removeItem("isAuthorized")
		},

		user_request(state) {
			state.status = { Name: 'auth loading', Success: undefined };
		},
		user_success(state) {
			state.status = { Name: 'user success', Success: true };
		},
		user_error(state) {
			state.status = { Name: 'user error', Success: false };
		},
	},

	actions: {
		signin: function ({ commit, dispatch }, userData = { Username: "", Password: "", IsPresistant: false }) {
			return new Promise((resolve, reject) => {
				commit('auth_request');

				apiService.BlindPost("user/security/signin", userData)
					.then(response => {
						commit('auth_success');
						dispatch('fetchUser');

						resolve(response);
					})
					.catch(err => {
						commit('auth_error');

						reject(err);
					})
			})
		},
		signup: function ({ commit, dispatch }, userData) {
			return new Promise((resolve, reject) => {
				commit('auth_request');

				apiService.BlindPost("user/security/signup", userData)
					.then(response => {
						dispatch("fetchUser");
						resolve(response);
					})
					.catch(err => {
						commit('auth_error', err)
						reject(err)
					})
			})
		},
		signout: function ({ commit }) {
			return new Promise((resolve) => {
				commit('signout');
				resolve();
			})
		},
		fetchUser: async function ({ commit, dispatch }) {
			return new Promise((resolve, reject) => {
				commit('user_request');

				apiService.BlindGet("user/current")
					.then(response => {
						commit("user_success", response);

						resolve();
					})
					.catch(err => {
						commit("user_error");

						dispatch("signout");
						reject(err);
					})
			})
		}
	},

	getters: {
		isAuthorized: function (state) { return state.isAuthorized },
		authStatus: function (state) { return state.status },

		// Used to hide parts of the webpage if the user does not have the nessasary premissions.
		// hasCategoryPremission: function (state, categoryId, premissionId) {
		// 	return state.user.Premissions.indexOf(a => a.CategoryId === categoryId && a.PremissionId === premissionId) > - 1;
		// },

	},
}