﻿import { createStore } from 'vuex';

import AuthStore from './auth';

export const store = createStore({
    modules: {
        Auth: AuthStore,
    }
});
