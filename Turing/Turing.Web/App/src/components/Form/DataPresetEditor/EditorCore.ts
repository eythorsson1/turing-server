
class DOMElement {
    constructor(type: string, attributes: object) {
}
    
}



export class EditorCore  {
    private _target: HTMLElement;
    //private _valueStr: string;

    private _options: object = {
        Language: "JSON"
    }

    private _plugins: object = {
        "JSON": {

        },
        "JavaScript": {

        }
    }

    constructor(target: HTMLElement, options: object = {}) {
        this._target = target;
        this._options = { ...this._options, ...options };

        var editor = this._CreateElement("div", { class: "editor" });

        target.append(editor);
    }

    private _CreateElement(type: string, attributes: object = {}, children: HTMLElement[] = []) : HTMLElement {
        var element = document.createElement(type);
        var att: any = attributes;

        Object.keys(att)
            .forEach((key: string) => element.setAttribute(key, att[key]));

        children.forEach((child: HTMLElement) => element.append(child));

        return element
    }

    public Parse(strValue : string) {

    }

    //public Format() {

    //}

    //set plugins(value: object) {

    //}

    //get Value() : string {
    //    return this._valueStr;
    //}

    //set Value(value: string) {
    //}
}