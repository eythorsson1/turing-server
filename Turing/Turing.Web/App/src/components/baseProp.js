var baseProp = function (required, defaultValue) {
    return {
        required: required,
        default: defaultValue,
    }
};

export default {
    Boolean: function (required, defaultValue) {
        return {
            ...baseProp(required, defaultValue),
            type: [Boolean, String],
            validator: val => ["true", "false"].indexOf(val.toString()) > -1,
        }
    },
    Number: function (required, defaultValue) {
        return {
            ...baseProp(required, defaultValue),
            type: [Number, String],
            validator: val => (0 <= val || val < 0)
        }
    },
    String: function (required) {
        return {
            ...baseProp(required),
            type: String
        }
    }
}