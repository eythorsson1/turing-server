﻿using AutoMapper;
using Turing.Domain.Domain.Models.Device.Feature;
using Turing.Domain.Domain.Models.Device.Feature.Data;
using Turing.Domain.Domain.Models.Dimension;
using Turing.Domain.Models.Device;
using Turing.Domain.Models.User;
using Turing.Web.Dto.Device;
using Turing.Web.Dto.Dimension;
using Turing.Web.Dto.User;

namespace Turing.Web.Configuration.AutoMapper.MapperProfiles
{
    public class DefaultMapper : Profile
    {
        public DefaultMapper()
        {
            // USER 
            CreateMap<UserModel, UserDto>();

            // Device
            CreateMap<DeviceModel, DeviceDto>();

            CreateMap<DeviceFeatureModel, DeviceFeatureDto>();
            CreateMap<DeviceFeatureDataModel, DeviceFeatureDataDto>();

            CreateMap<DimensionModel, DimensionDto>();
            CreateMap<DimensionRegisterModel, DimensionRegisterDto>();

        }
    }
}
