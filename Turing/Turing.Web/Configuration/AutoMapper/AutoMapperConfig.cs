﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using Turing.Web.Dto;
using Turing.Domain.Infrastructure;
using Turing.Domain.Infrastructure.Extensions;
using Turing.Domain.Models;
using Turing.Web.Configuration.AutoMapper.MapperProfiles;
using System.Linq;

namespace Turing.Web.Configuration.AutoMapper
{
    public static class AutoMapperConfig
    {
        private static IMapper _mapper { get; set; }

        public static void SetupAutoMapper(this IServiceCollection services)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.AddProfile(new DefaultMapper());
            });

            _mapper = config.CreateMapper();
        }

        public static T MapTo<T>(this IModel data)
        {
            return _mapper.Map<T>(data);
        }

        public static List<T> MapTo<T>(this IEnumerable<IModel> data)
        {
            return _mapper.Map<List<T>>(data);
        }

        public static PagedListDto<T> MapTo<T, TFrom>(this PagedList<TFrom> data) where TFrom : IModel 
        {
            return new PagedListDto<T>(
                _mapper.Map<List<T>>(data.Items),
                data.TotalNumberOfItems,
                data.CurrentPage,
                data.ItemsPerPage
            );
        }
        //public static class Mapper
        //{
        //    public static T MapTo<T>(object data)
        //    {
        //        return _mapper.Map<T>(data);
        //    }

        //    public static List<T> MapList<T>(List<object> data)
        //    {
        //        return _mapper.Map<List<T>>(data);
        //    }

        //    public static PagedListDto<T> MapPagedList<T>(IPagedList<object> data)
        //    {
        //        return new PagedListDto<T>(
        //            MapList<T>(data.Items),
        //            data.TotalNumberOfItems,
        //            data.CurrentPage,
        //            data.ItemsPerPage
        //        );
        //    }
        //}

    }
}
