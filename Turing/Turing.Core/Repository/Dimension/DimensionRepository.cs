﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Turing.Core.Request.Dimension;
using Turing.Domain.Domain.Models.Dimension;
using Turing.Domain.Infrastructure;

namespace Turing.Core.Repository.Dimension
{

    internal class DimensionRepository : BaseRepository
    {
        public DimensionRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }

        private string GetSql(DimensionRequest request, bool usePaging = false)
        {
            var sql = @$"
                SELECT 
                    D.[DimensionId], D.[Type], D.[ClientId]
                FROM [DIMENSION_TAB] D

                {GetSqlWhere(request)}

                ORDER BY
                    CASE WHEN @OrderBy = {(int)DimensionOrderByEnum.DimensionId} THEN D.[DimensionId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DimensionOrderByEnum.Type} THEN D.[Type] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DimensionOrderByEnum.ClientId} THEN D.[ClientId] END {request.OrderByDirectionStr}

                {UsePagingSql(usePaging)}
            ";

            return sql;
        }

        private string GetSqlWhere(DimensionRequest request)
        {
            var searchParams = new List<string>();

            if (request.DimensionId > 0)
                searchParams.Add("D.[DimensionId] = @DimensionId");
            else {
                if (request.ClientId > 0) searchParams.Add("D.[ClientId] = @ClientId");
                if (request.Type > 0) searchParams.Add("D.[Type] = @Type");

                if (request.DimensionIds?.Any() ?? false) searchParams.Add("D.[DimensionId] IN @DimensionIds");
            }

            return @$"
                {(searchParams.Any() ? " WHERE " + string.Join(" AND ", searchParams) : "")}
            ";
        }

        internal long Count(DimensionRequest request)
        {
            string sql = @$"
                SELECT COUNT(*) FROM [DIMENSION_TAB]
                {GetSqlWhere(request)}
            ";

            return FirstOrDefault<long>(sql, request);
        }

        internal PagedList<DimensionModel> GetPagedList(DimensionRequest request)
        {
            var items = QueryList<DimensionModel>(GetSql(request, usePaging: true), request);
            return new PagedList<DimensionModel>(items, Count(request), request);
        }

        internal List<DimensionModel> GetList(DimensionRequest request)
        {
            return QueryList<DimensionModel>(GetSql(request), request);
        }

        internal DimensionModel FirstOrDefault(DimensionRequest request)
        {
            return FirstOrDefault<DimensionModel>(GetSql(request), request);
        }

        internal DimensionModel GetById(long dimensionId)
        {
            var request = new DimensionRequest { DimensionId = dimensionId };
            return FirstOrDefault<DimensionModel>(GetSql(request), request);
        }

        internal long Insert(DimensionModel model)
        {
            var sql = @"
                INSERT INTO [DIMENSION_TAB] (
                    [Type], [ClientId]
                )
                OUTPUT INSERTED.[DimensionId]
                VALUES (
                    @Type, @ClientId
                )
            ";

            return FirstOrDefault<long>(sql, model);
        }

        //internal bool Delete(long dimensionId)
        //{
        //    var sql = @"
        //        DELETE FROM [DIMENSION_TAB] SET
        //        WHERE [DimensionId] = @dimensionId 
        //    ";

        //    return Execute(sql, new { dimensionId });
        //}
    }
}
