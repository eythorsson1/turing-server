﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Turing.Core.Request.Dimension;
using Turing.Domain.Domain.Models.Dimension;

namespace Turing.Core.Repository.Dimension
{

    internal class DimensionRegisterLinkRepository : BaseRepository
    {
        public DimensionRegisterLinkRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }

        private string GetSql(DimensionRegisterLinkRequest request, bool usePaging = false)
        {
            var sql = @$"
                SELECT 
                    DRL.[DimensionRegisterLinkId], DRL.[DimensionRegisterId], DRL.[DimensionId]
                FROM [DIMENSION_REGISTER_LINK] DRL

                {GetSqlWhere(request)}

                ORDER BY
                    CASE WHEN @OrderBy = {(int)DimensionRegisterLinkOrderByEnum.DimensionRegisterLinkId} THEN DRL.[DimensionRegisterLinkId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DimensionRegisterLinkOrderByEnum.DimensionRegisterId} THEN DRL.[DimensionRegisterId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DimensionRegisterLinkOrderByEnum.DimensionId} THEN DRL.[DimensionId] END {request.OrderByDirectionStr}

                {UsePagingSql(usePaging)}
            ";

            return sql;
        }

        private string GetSqlWhere(DimensionRegisterLinkRequest request)
        {
            var searchParams = new List<string>();

            if (request.DimensionRegisterLinkId > 0)
                searchParams.Add("DRL.[DimensionRegisterLinkId] = @DimensionRegisterLinkId");
            else {
                if (request.DimensionRegisterId > 0) searchParams.Add("DRL.[DimensionRegisterId] = @DimensionRegisterId");
                if (request.DimensionId > 0) searchParams.Add("DRL.[DimensionId] = @DimensionId");

                if (request.DimensionIds != null && request.DimensionIds.Any()) searchParams.Add("DRL.[DimensionId] IN @DimensionIds");
            }

            return @$"
                {(searchParams.Any() ? " WHERE " + string.Join(" AND ", searchParams) : "")}
            ";
        }

        internal long Count(DimensionRegisterLinkRequest request)
        {
            string sql = @$"
                SELECT COUNT(*) FROM [DIMENSION_REGISTER_LINK]
                {GetSqlWhere(request)}
            ";

            return FirstOrDefault<long>(sql, request);
        }

        internal List<DimensionRegisterLinkModel> GetList(DimensionRegisterLinkRequest request)
        {
            return QueryList<DimensionRegisterLinkModel>(GetSql(request), request);
        }

        internal DimensionRegisterLinkModel FirstOrDefault(DimensionRegisterLinkRequest request)
        {
            return FirstOrDefault<DimensionRegisterLinkModel>(GetSql(request), request);
        }

        internal DimensionRegisterLinkModel GetById(long dimensionRegisterLink)
        {
            var request = new DimensionRegisterLinkRequest { DimensionRegisterLinkId = dimensionRegisterLink };
            return FirstOrDefault<DimensionRegisterLinkModel>(GetSql(request), request);
        }


        internal long Insert(DimensionRegisterLinkModel model)
        {
            var sql = @"
                INSERT INTO [DIMENSION_REGISTER_LINK] (
                    [DimensionRegisterId], [DimensionId]
                )
                OUTPUT INSERTED.[DimensionRegisterLinkId]
                VALUES (
                    @DimensionRegisterId, @DimensionId
                )
            ";

            return FirstOrDefault<long>(sql, model);
        }

        internal bool Delete(long dimensionRegisterLink)
        {
            var sql = @"
                DELETE FROM [DIMENSION_REGISTER_LINK] SET
                WHERE [DimensionRegisterLinkId] = @dimensionRegisterLink 
            ";

            return Execute(sql, new { dimensionRegisterLink });
        }
    }
}
