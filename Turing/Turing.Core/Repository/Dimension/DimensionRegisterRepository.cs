﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Turing.Core.Request.Dimension;
using Turing.Domain.Domain.Models.Dimension;
using Turing.Domain.Infrastructure;

namespace Turing.Core.Repository.Dimension
{

    internal class DimensionRegisterRepository : BaseRepository
    {
        public DimensionRegisterRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }

        private string GetSql(DimensionRegisterRequest request, bool usePaging = false)
        {
            var sql = @$"
                SELECT 
                    D.[DimensionRegisterId], D.[Type], D.[Code], D.[Description]
                FROM [DIMENSION_REGISTER_TAB] D

                {GetSqlWhere(request)}

                ORDER BY
                    CASE WHEN @OrderBy = {(int)DimensionRegisterOrderByEnum.DimensionRegisterId} THEN D.[DimensionRegisterId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DimensionRegisterOrderByEnum.Type} THEN D.[Type] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DimensionRegisterOrderByEnum.Code} THEN D.[Code] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DimensionRegisterOrderByEnum.Description} THEN D.[Description] END {request.OrderByDirectionStr}

                {UsePagingSql(usePaging)}
            ";

            return sql;
        }

        private string GetSqlWhere(DimensionRegisterRequest request)
        {
            var searchParams = new List<string>();

            if (request.DimensionRegisterId > 0)
                searchParams.Add("D.[DimensionRegisterId] = @DimensionRegisterId");
            else {
                if (request.Type > 0) searchParams.Add("D.[Type] = @Type");

                if (!string.IsNullOrEmpty(request.Code)) searchParams.Add("D.[Code] LIKE '%' + @Code + '%'");
                if (!string.IsNullOrEmpty(request.Description)) searchParams.Add("D.[Code] LIKE '%' + @Description + '%'");

                if (request.DimensionRegisterIds != null && request.DimensionRegisterIds.Any()) searchParams.Add("D.[DimensionRegisterId] IN @DimensionRegisterIds");
            }

            return @$"
                {(searchParams.Any() ? " WHERE " + string.Join(" AND ", searchParams) : "")}
            ";
        }

        internal long Count(DimensionRegisterRequest request)
        {
            string sql = @$"
                SELECT COUNT(*) FROM [DIMENSION_REGISTER_TAB]
                {GetSqlWhere(request)}
            ";

            return FirstOrDefault<long>(sql, request);
        }

        internal PagedList<DimensionRegisterModel> GetPagedList(DimensionRegisterRequest request)
        {
            var items = QueryList<DimensionRegisterModel>(GetSql(request, usePaging: true), request);
            return new PagedList<DimensionRegisterModel>(items, Count(request), request);
        }

        internal List<DimensionRegisterModel> GetList(DimensionRegisterRequest request)
        {
            return QueryList<DimensionRegisterModel>(GetSql(request), request);
        }

        internal DimensionRegisterModel FirstOrDefault(DimensionRegisterRequest request)
        {
            return FirstOrDefault<DimensionRegisterModel>(GetSql(request), request);
        }

        internal DimensionRegisterModel GetById(long dimensionRegisterId)
        {
            var request = new DimensionRegisterRequest { DimensionRegisterId = dimensionRegisterId };
            return FirstOrDefault<DimensionRegisterModel>(GetSql(request), request);
        }

        internal bool Update(DimensionRegisterModel model)
        {
            var sql = @"
                UPDATE [DIMENSION_REGISTER_TAB] SET
                    [Code] = Code, 
                    [Description] = Description
                WHERE 
                    [DimensionRegisterId] = @DimensionRegisterId 
            ";

            return Execute(sql, model);
        }

        internal long Insert(DimensionRegisterModel model)
        {
            var sql = @"
                INSERT INTO [DIMENSION_REGISTER_TAB] (
                    [Type], [Code], [Description]
                )
                OUTPUT INSERTED.[DimensionRegisterId]
                VALUES (
                    @Type, @Code, @Description
                )
            ";

            return FirstOrDefault<long>(sql, model);
        }

        internal bool Delete(long dimensionRegisterId)
        {
            var sql = @"
                DELETE FROM [DIMENSION_REGISTER_TAB] SET
                WHERE [DimensionRegisterId] = @dimensionRegisterId 
            ";

            return Execute(sql, new { dimensionRegisterId });
        }
    }
}
