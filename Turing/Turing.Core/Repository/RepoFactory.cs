﻿using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository.Automation;
using Turing.Core.Repository.Automation.Item;
using Turing.Core.Repository.Client;
using Turing.Core.Repository.ClientUserLink.UserLink;
using Turing.Core.Repository.Data.Value;
using Turing.Core.Repository.Device;
using Turing.Core.Repository.Device.Type.Feature;
using Turing.Core.Repository.Device.Type.Feature.Data;
using Turing.Core.Repository.DeviceLog;
using Turing.Core.Repository.Dimension;
using Turing.Core.Repository.Preset.Data;
using Turing.Core.Repository.User;
using Turing.Core.Repository.User.Preference;

namespace Turing.Core.Repository
{
    public class RepoFactory
    {
        // USER
        internal UserRepository UserRepository(IUnitOfWork uow) => new UserRepository(uow.IDBConnection, uow.IDBTransaction);
        internal UserTablePreferenceRepository UserTablePreferenceRepository(IUnitOfWork uow) => new UserTablePreferenceRepository(uow.IDBConnection, uow.IDBTransaction);

        // CLIENT
        internal ClientRepository ClientRepository(IUnitOfWork uow) => new ClientRepository(uow.IDBConnection, uow.IDBTransaction);
        internal ClientUserLinkRepository ClientUserLinkRepository(IUnitOfWork uow) => new ClientUserLinkRepository(uow.IDBConnection, uow.IDBTransaction);


        // DIMENSION
        internal DimensionRegisterLinkRepository DimensionRegisterLinkRepository(IUnitOfWork uow) => new DimensionRegisterLinkRepository(uow.IDBConnection, uow.IDBTransaction);
        internal DimensionRegisterRepository DimensionRegisterRepository(IUnitOfWork uow) => new DimensionRegisterRepository(uow.IDBConnection, uow.IDBTransaction);
        internal DimensionRepository DimensionRepository(IUnitOfWork uow) => new DimensionRepository(uow.IDBConnection, uow.IDBTransaction);


        // DATA
        internal DataPresetRepository DataPresetRepository(IUnitOfWork uow) => new DataPresetRepository(uow.IDBConnection, uow.IDBTransaction);
        internal DataValueRepository DataValueRepository(IUnitOfWork uow) => new DataValueRepository(uow.IDBConnection, uow.IDBTransaction);


        // DEVICE
        internal DeviceLogRepository DeviceLogRepository(IUnitOfWork uow) => new DeviceLogRepository(uow.IDBConnection, uow.IDBTransaction);
        internal DeviceFeatureDataRepository DeviceFeatureDataRepository(IUnitOfWork uow) => new DeviceFeatureDataRepository(uow.IDBConnection, uow.IDBTransaction);
        internal DeviceFeatureRepository DeviceFeatureRepository(IUnitOfWork uow) => new DeviceFeatureRepository(uow.IDBConnection, uow.IDBTransaction);
        internal DeviceRepository DeviceRepository(IUnitOfWork uow) => new DeviceRepository(uow.IDBConnection, uow.IDBTransaction);


        // AUTOMATION
        internal AutomationItemLinkRepository AutomationItemLinkRepository(IUnitOfWork uow) => new AutomationItemLinkRepository(uow.IDBConnection, uow.IDBTransaction);
        internal AutomationItemRepository AutomationItemRepository(IUnitOfWork uow) => new AutomationItemRepository(uow.IDBConnection, uow.IDBTransaction);
        internal AutomationRepository AutomationRepository(IUnitOfWork uow) => new AutomationRepository(uow.IDBConnection, uow.IDBTransaction);

    }
}
