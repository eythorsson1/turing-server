﻿using Dapper;
using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Domain.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Turing.Domain.Models;
using Turing.Core.Request;

namespace Turing.Core.Repository
{
    public abstract class BaseRepository
    {
        protected readonly IDbConnection _db;
        protected readonly IDbTransaction _ta;
        public BaseRepository(IDbConnection dbConnection, IDbTransaction dbTransaction)
        {
            _db = dbConnection;
            _ta = dbTransaction;
        }

        protected private string UsePagingSql(bool usePaging)
        {
            if (usePaging){
                return @"
                    OFFSET (@ItemsPerPage * @CurrentPage) ROWS
                    FETCH NEXT @ItemsPerPage ROWS ONLY
                ";
            }

            return "";
        }

        protected private List<T> QueryList<T>(string sql, object request)
        {
            return _db.Query<T>(sql, request, transaction: _ta).ToList();
        }

        protected private T FirstOrDefault<T>(string sql, object request)
        {
            return _db.QueryFirstOrDefault<T>(sql, request, transaction: _ta);
        }

        protected private T FirstOrDefault<T>(string sql, IModel model)
        {
            //model.Validate();
            return _db.QueryFirstOrDefault<T>(sql, GetParameters(model), transaction: _ta);
        }

        protected private T SingleOrDefault<T>(string sql,object request)
        {
            return _db.QuerySingleOrDefault<T>(sql, request, transaction: _ta);
        }

        protected private T SingleOrDefault<T>(string sql, IModel model)
        {
            //model.Validate();
            return _db.QuerySingleOrDefault<T>(sql, GetParameters(model), transaction: _ta);
        }

        protected private bool Execute(string sql, object request)
        {
            return _db.Execute(sql, request, transaction: _ta) > 0;
        }

        protected private bool Execute(string sql, IModel model)
        {
            //model.Validate();
            return _db.Execute(sql, GetParameters(model), transaction: _ta) == 1;
        }

        protected virtual DynamicParameters GetParameters(object model)
        {
            var parameters = new DynamicParameters();

            foreach(var property in model.GetType().GetProperties()) {
                var value = property.GetValue(model);

                if (IsNullOrDefault(value)) 
                    parameters.Add(property.Name, value: null);
                else
                    parameters.Add(property.Name, value: value);
            }

            return parameters;
        }

        private bool IsNullOrDefault<T>(T argument)
        {
            // deal with normal scenarios
            if (argument == null) return true;
            if (object.Equals(argument, default(T))) return true;

            // deal with non-null nullables
            Type methodType = typeof(T);
            if (Nullable.GetUnderlyingType(methodType) != null) return false;

            // deal with boxed value types
            Type argumentType = argument.GetType();
            if (argumentType.IsValueType && argumentType != methodType) {
                object obj = Activator.CreateInstance(argument.GetType());
                return obj.Equals(argument);
            }

            return false;
        }
    }


    public abstract class BasePagedRepository<TOrderByEnum, TRequest, TModel> : BaseRepository
        where TOrderByEnum : Enum
        where TRequest : BasePagedRequest<TOrderByEnum>
        where TModel : IModel
    {
        protected BasePagedRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {}

        protected abstract string GetSql(TRequest request, bool usePaging = false);
        protected abstract string GetSqlWhere(TRequest request);

        protected internal abstract long Count(TRequest request);
        protected internal abstract bool Update(TModel model);
        protected internal abstract long Insert(TModel model);
        protected internal abstract bool Delete(TModel model);

        protected internal TModel FirstOrDefault(TRequest request) => FirstOrDefault<TModel>(GetSql(request), request);
        protected internal TModel SingleOrDefault(TRequest request) => SingleOrDefault<TModel>(GetSql(request), request);
        protected internal List<TModel> GetList(TRequest request) => QueryList<TModel>(GetSql(request, usePaging: true), request); 
        protected internal PagedList<TModel> GetPagedList(TRequest request) => new PagedList<TModel>(GetList(request), Count(request), request);
    }
}
