﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Turing.Core.Request.Data;
using Turing.Domain.Domain.Models.Data.Preset;

namespace Turing.Core.Repository.Preset.Data
{

    internal class DataPresetRepository : BaseRepository
    {
        public DataPresetRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }

        private string GetSql(DataPresetRequest request, bool usePaging = false)
        {
            var sql = @$"
                SELECT 
                    DP.[DataPresetId], DP.[DataPresetStr], DP.[ClientId]
                FROM [DATA].[DATA_PRESET_TAB] DP

                {GetSqlWhere(request)}

                ORDER BY
                    CASE WHEN @OrderBy = {(int)DataPresetOrderByEnum.DataPresetId} THEN DP.[DataPresetId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DataPresetOrderByEnum.ClientId} THEN DP.[ClientId] END {request.OrderByDirectionStr}

                {UsePagingSql(usePaging)}
            ";

            return sql;
        }

        private string GetSqlWhere(DataPresetRequest request)
        {
            var searchParams = new List<string>();

            if (request.DataPresetId > 0)
                searchParams.Add("DP.[DataPresetId] = @DataPresetId");
            else {
                if (request.DataPresetIds != null && request.DataPresetIds.Any()) searchParams.Add("DP.[DataPresetId] IN @DataPresetIds");
                if (request.ClientId > 0) searchParams.Add("DP.[ClientId] = @ClientId");
            }

            return @$"
                {(searchParams.Any() ? " WHERE " + string.Join(" AND ", searchParams) : "")}
            ";
        }

        internal long Count(DataPresetRequest request)
        {
            string sql = @$"
                SELECT COUNT(*) FROM [DATA].[DATA_PRESET_TAB]
                {GetSqlWhere(request)}
            ";

            return FirstOrDefault<long>(sql, request);
        }

        internal List<DataPresetModel> GetList(DataPresetRequest request)
        {
            return QueryList<DataPresetModel>(GetSql(request), request);
        }

        internal DataPresetModel FirstOrDefault(DataPresetRequest request)
        {
            return FirstOrDefault<DataPresetModel>(GetSql(request), request);
        }

        internal DataPresetModel GetById(long dataPresetIdId)
        {
            var request = new DataPresetRequest { DataPresetId = dataPresetIdId };
            return FirstOrDefault<DataPresetModel>(GetSql(request), request);
        }

        internal bool Update(DataPresetModel model)
        {
            var sql = @"
                UPDATE [DATA].[DATA_PRESET_TAB] SETs
                    [DataPresetStr] = @DataPresetStr
                WHERE 
                    [DataPresetId] = @DataPresetId  
            ";

            return Execute(sql, model);
        }

        internal long Insert(DataPresetModel model)
        {
            var sql = @"
                INSERT INTO [DATA].[DATA_PRESET_TAB] (
                    [DataPresetStr], [ClientId]
                )
                OUTPUT INSERTED.[DataPresetId]
                VALUES (
                    @DataPresetStr, @ClientId
                )
            ";

            return FirstOrDefault<long>(sql, model);
        }

        internal bool Delete(long dataPresetIdId)
        {
            var sql = @"
                DELETE FROM [DATA].[DATA_PRESET_TAB] SET
                WHERE [DataPresetId] = @dataPresetIdId 
            ";

            return Execute(sql, new { dataPresetIdId });
        }
    }
}
