﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Turing.Core.Request.Data.Value;
using Turing.Domain.Domain.Models.Data.Value;

namespace Turing.Core.Repository.Data.Value
{

    internal class DataValueRepository : BaseRepository
    {
        public DataValueRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }

        private string GetSql(DataValueRequest request, bool usePaging = false)
        {
            var sql = @$"
                SELECT 
                    DV.[DataValueId], DV.[DataPresetId], DV.[DataValueStr], DV.[ClientId]
                FROM [DATA].[DATA_VALUE_TAB] DV

                {GetSqlWhere(request)}

                ORDER BY
                    CASE WHEN @OrderBy = {(int)DataValueOrderByEnum.DataValueId} THEN DV.[DataValueId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DataValueOrderByEnum.DataPresetId} THEN DV.[DataPresetId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DataValueOrderByEnum.ClientId} THEN DV.[ClientId] END {request.OrderByDirectionStr}

                {UsePagingSql(usePaging)}
            ";

            return sql;
        }

        private string GetSqlWhere(DataValueRequest request)
        {
            var searchParams = new List<string>();

            if (request.DataValueId > 0)
                searchParams.Add("DV.[DataValueId] = @DataValueId");
            else {
                if (request.DataPresetId > 0) searchParams.Add("DV.[DataPresetId] = @DataPresetId");
                if (request.ClientId > 0) searchParams.Add("DV.[ClientId] = @ClientId");

                if (request.DataValueIds != null && request.DataValueIds.Any()) searchParams.Add("DV.[DataValueId] IN @DataValueIds");
            }

            return @$"
                {(searchParams.Any() ? " WHERE " + string.Join(" AND ", searchParams) : "")}
            ";
        }

        internal long Count(DataValueRequest request)
        {
            string sql = @$"
                SELECT COUNT(*) FROM [DATA].[DATA_VALUE_TAB]
                {GetSqlWhere(request)}
            ";

            return FirstOrDefault<long>(sql, request);
        }

        internal List<DataValueModel> GetList(DataValueRequest request)
        {
            return QueryList<DataValueModel>(GetSql(request), request);
        }

        internal DataValueModel FirstOrDefault(DataValueRequest request)
        {
            return FirstOrDefault<DataValueModel>(GetSql(request), request);
        }

        internal DataValueModel GetById(long dataValueId)
        {
            var request = new DataValueRequest { DataValueId = dataValueId };
            return FirstOrDefault<DataValueModel>(GetSql(request), request);
        }

        internal bool Update(DataValueModel model)
        {
            var sql = @"
                UPDATE [DATA].[DATA_VALUE_TAB] SET
                    [DataPresetId] = @DataPresetId,
                    [DataValueStr] = @DataValueStr
                WHERE 
                    [DataValueId] = @DataValueId 
            ";

            return Execute(sql, GetParameters(model));
        }

        internal long Insert(DataValueModel model)
        {
            var sql = @"
                INSERT INTO [DATA].[DATA_VALUE_TAB] (
                    [DataPresetId], [DataValueStr], [ClientId]
                )
                OUTPUT INSERTED.[DataValueId]
                VALUES (
                    @DataPresetId, @DataValueStr, @ClientId
                )
            ";

            return FirstOrDefault<long>(sql, GetParameters(model));
        }

        internal bool Delete(long dataValueId)
        {
            var sql = @"
                DELETE FROM [DATA].[DATA_VALUE_TAB] SET
                WHERE [DataValueId] = @dataValueId 
            ";

            return Execute(sql, new { dataValueId });
        }

        public DynamicParameters GetParameters(DataValueModel model)
        {
            var p = base.GetParameters(model);
            p.Add(nameof(model.Items), null);

            return p;
        }
    }
}
