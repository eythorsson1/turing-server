﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Turing.Core.Request.Automation.Item;
using Turing.Domain.Enums.Automation;
using Turing.Domain.Infrastructure;
using Turing.Domain.Models.Automation.Item;

namespace Turing.Core.Repository.Automation.Item
{
    public class AutomationItemRepository : BaseRepository
    {
        public AutomationItemRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }


        private string GetSql<AutomationItemTypeEnum>(BaseAutomationItemRequest<AutomationItemTypeEnum> request, bool usePaging = false) where AutomationItemTypeEnum : Enum
        {
            var sql = @$"
                SELECT 
                    AI.[AutomationItemId], AI.[AutomationId], AI.[AutomationType], AI.[Type], AI.[DataStr], AI.[DataPresetId]
                FROM [AUTOMATION].[AUTOMATION_ITEM_TAB] AI
                
                {GetSqlWhere(request)}

                ORDER BY
                    CASE WHEN @OrderBy = {(int)AutomationItemOrderByEnum.AutomationItemId} THEN AI.[AutomationItemId] END {request.OrderByDirectionStr}
                                                         

                {UsePagingSql(usePaging)}
            ";

            return sql;
        }

        private string GetSqlWhere<AutomationItemTypeEnum>(BaseAutomationItemRequest<AutomationItemTypeEnum> request) where AutomationItemTypeEnum : Enum
        {
            var searchParams = new List<string>();

            if (request.AutomationItemId > 0) {
                searchParams.Add("AI.[AutomationItemId] = @AutomationItemId");
            }
            else {
                if (request.ClientId > 0) searchParams.Add("AI.[ClientId] = @ClientId");
                if (request.AutomationId > 0) searchParams.Add("AI.[AutomationId] = @AutomationId");
                if (request.AutomationType > 0) searchParams.Add("AI.[AutomationType] = @AutomationType");
                if ((int)Enum.Parse(request.Type.GetType(), request.Type.ToString()) > 0) searchParams.Add("AI.[Type] = @Type");

                if (!string.IsNullOrEmpty(request.DataStr)) searchParams.Add("AI.[DataStr] = @DataStr");
            }

            return @$"
                {(searchParams.Any() ? "WHERE " + string.Join(" AND ", searchParams) : "")}
            ";
        }

        internal long Count<AutomationItemTypeEnum>(BaseAutomationItemRequest<AutomationItemTypeEnum> request) where AutomationItemTypeEnum : Enum
        {
            string sql = $@"
                SELECT COUNT(*) FROM [AUTOMATION].[AUTOMATION_ITEM_TAB]
                {GetSqlWhere(request)}
            ";

            return FirstOrDefault<long>(sql, request);
        }

        internal PagedList<TModel> GetPagedList<AutomationItemTypeEnum, TModel>(BaseAutomationItemRequest<AutomationItemTypeEnum> request) 
            where AutomationItemTypeEnum : Enum
            where TModel : BaseAutomationItemModel<AutomationItemTypeEnum>
        {
            var items = QueryList< TModel >(GetSql(request, usePaging: true), request);
            return new PagedList<TModel>(items, Count(request), request);
        }

        internal List<TModel> GetList<AutomationItemTypeEnum, TModel>(BaseAutomationItemRequest<AutomationItemTypeEnum> request) 
            where AutomationItemTypeEnum : Enum
            where TModel : BaseAutomationItemModel<AutomationItemTypeEnum>
        {
            return QueryList<TModel> (GetSql(request), request);
        }

        internal TModel FirstOrDefault<AutomationItemTypeEnum, TModel>(BaseAutomationItemRequest<AutomationItemTypeEnum> request) 
            where AutomationItemTypeEnum : Enum
            where TModel : BaseAutomationItemModel<AutomationItemTypeEnum>
        {
            return FirstOrDefault<TModel>(GetSql(request), request);
        }

        internal bool Update<AutomationItemTypeEnum, TModel>(TModel model)
            where AutomationItemTypeEnum : Enum
            where TModel : BaseAutomationItemModel<AutomationItemTypeEnum>
        {
            var sql = @"
                UPDATE [AUTOMATION].[AUTOMATION_ITEM_TAB] SET
                    [AutomationType] = @AutomationType,
                    [ItemType] = @ItemType,
                    [DataStr] = @DataStr
                WHERE
                    [AutomationItemId] = @AutomationItemId
            ";

            return Execute(sql, model);
        }

        internal long Insert<AutomationItemTypeEnum, TModel>(TModel model)
            where AutomationItemTypeEnum : Enum
            where TModel : BaseAutomationItemModel<AutomationItemTypeEnum>
        {
            var sql = @"
                INSERT INTO [AUTOMATION].[AUTOMATION_ITEM_TAB] (
                    [AutomationId], [AutomationType], [Type], [DataStr], [ClientId]
                )
                OUTPUT INSERTED.[AutomationItemId]
                VALUES (
                    @AutomationId, @AutomationType, @Type, @DataStr, @ClientId
                )
            ";

            return FirstOrDefault<long>(sql, model);
        }

        internal bool Delete(long automationItemId, AutomationTypeEnum automationType)
        {
            var sql = @"
                DELETE FROM [AUTOMATION].[AUTOMATION_ITEM_TAB]
                WHERE 
                    [AutomationItemId] = @automationItemId AND 
                    [AutomationType] = @automationType
            ";

            return Execute(sql, new { automationItemId, automationType });
        }
    }
}
