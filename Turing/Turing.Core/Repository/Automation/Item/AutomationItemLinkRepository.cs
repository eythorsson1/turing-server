﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Turing.Core.Request.Automation.Item;
using Turing.Domain.Models.Automation.Item;
using Turing.Domain.Infrastructure;

namespace Turing.Core.Repository.Automation.Item
{
    class AutomationItemLinkRepository : BaseRepository
    {
        public AutomationItemLinkRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        { }

        private string GetSql(AutomationItemLinkRequest request)
        {
            var sql = @$"
                SELECT 
                    AIL.[AutomationItemLinkId], AIL.[ParentAutomationItemId], AIL.[ChildAutomationItemId], AIL.[AutomationId], AIL.[ClientId],
                    AI_P.[AutomationType] As 'ParentAutomationType', AI_P.[Type] As 'ParentAutomationItemType',
                    AI_C.[AutomationType] As 'ChildAutomationType', AI_C.[Type] As 'ChildAutomationItemType'


                FROM [AUTOMATION].[AUTOMATION_ITEM_LINK] AIL
                    LEFT OUTER JOIN [AUTOMATION].[AUTOMATION_ITEM_TAB] AI_P ON AI_P.[AutomationItemId] = AIL.[ParentAutomationItemId]
                    LEFT OUTER JOIN [AUTOMATION].[AUTOMATION_ITEM_TAB] AI_C ON AI_C.[AutomationItemId] = AIL.[ChildAutomationItemId]
                
                {GetSqlWhere(request)}
            ";

            return sql;
        }

        private string GetSqlWhere(AutomationItemLinkRequest request)
        {
            var searchParams = new List<string>();

            if (request.AutomationItemLinkId > 0) {
                searchParams.Add("AIL.[AutomationItemLinkId] = @AutomationItemLinkId");
            }
            else {
                if (request.ClientId > 0) searchParams.Add("AIL.[ClientId] = @ClientId");
                if (request.AutomationId > 0) searchParams.Add("AIL.[AutomationId] = @AutomationId");
                if (request.ParentAutomationItemId > 0) searchParams.Add("AIL.[ParentAutomationItemId] = @ParentAutomationItemId");
                if (request.ChildAutomationItemId > 0) searchParams.Add("AIL.[ChildAutomationItemId] = @ChildAutomationItemId");
            }

            return @$"
                {(searchParams.Any() ? "WHERE " + string.Join(" AND ", searchParams) : "")}
            ";
        }

        internal long Count(AutomationItemLinkRequest request)
        {
            string sql = $@"
                SELECT COUNT(*) FROM [AUTOMATION].[AUTOMATION_ITEM_LINK]
                {GetSqlWhere(request)}
            ";

            return FirstOrDefault<long>(sql, request);
        }

        internal List<AutomationItemLinkModel> GetList(AutomationItemLinkRequest request)
        {
            return QueryList<AutomationItemLinkModel>(GetSql(request), request);
        }

        internal AutomationItemLinkModel GetById(long automationItemLinkId)
        {
            var request = new AutomationItemLinkRequest { AutomationItemLinkId = automationItemLinkId };
            return FirstOrDefault<AutomationItemLinkModel>(GetSql(request), request);
        }

        internal long Insert(AutomationItemLinkModel model)
        {
            var sql = @"
                INSERT INTO [AUTOMATION].[AUTOMATION_ITEM_LINK] (
                    [ParentAutomationItemId], [ChildAutomationItemId], [AutomationId], [ClientId]
                )
                OUTPUT INSERTED.[AutomationItemLinkId]
                VALUES (
                    @ParentAutomationItemId, @ChildAutomationItemId, @AutomationId, @ClientId
                )
            ";

            return FirstOrDefault<long>(sql, model);
        }

        internal bool Delete(long automationItemLinkId)
        {
            var sql = @"
                DELETE FROM [AUTOMATION].[AUTOMATION_ITEM_LINK]
                WHERE [AutomationItemLinkId] = @automationItemLinkId
            ";

            return Execute(sql, new { automationItemLinkId });
        }
    }
}
