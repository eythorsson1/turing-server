﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Turing.Core.Request.Automation;
using Turing.Domain.Models.Automation;
using Turing.Domain.Infrastructure;

namespace Turing.Core.Repository.Automation
{
    internal class AutomationRepository : BaseRepository
    {
        public AutomationRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {}

        private string GetSql(AutomationRequest request, bool usePaging = false)
        {
            var sql = @$"
                SELECT 
                    A.[AutomationId], A.[Name], A.[CreatedDateUTC], A.[LastModifiedDateUTC], A.[ClientId]
                    U_C.[UserId] As 'CreatedByUserId', U_C.[FirstName] As 'CreatedByFirstName', U_C.[LastName] As 'CreatedByLastName',
                    U_M.[UserId] As 'LastModifiedByUserId', U_M.[FirstName] As 'LastModifiedByFirstName', U_M.[LastName] As 'LastModifiedByLastName'
                FROM [AUTOMATION].[AUTOMATION_TAB] A
                    LEFT OUTER JOIN [USER_TAB] U_T ON U_T.[UserId] = A.[CreatedByUserId]
                    LEFT OUTER JOIN [USER_TAB] U_M ON U_M.[UserId] = A.[LastModifiedByUserId]
                
                {GetSqlWhere(request)}

                ORDER BY
                    CASE WHEN @OrderBy = {(int)AutomationOrderByEnum.Name} THEN A.[Name] END {request.OrderByDirectionStr},

                    CASE WHEN @OrderBy = {(int)AutomationOrderByEnum.CreatedDateUTC} THEN A.[CreatedDateUTC] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)AutomationOrderByEnum.CreatedByFullName} THEN A.[CreatedDateUTC] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)AutomationOrderByEnum.CreatedByFullName} THEN A.[CreatedByLastName] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)AutomationOrderByEnum.CreatedByFirstName} THEN A.[CreatedDateUTC] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)AutomationOrderByEnum.CreatedByLastName} THEN A.[CreatedByLastName] END {request.OrderByDirectionStr},

                    CASE WHEN @OrderBy = {(int)AutomationOrderByEnum.LastModifiedDateUTC} THEN A.[LastModifiedDateUTC] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)AutomationOrderByEnum.LastModifiedByFullName} THEN A.[LastModifiedByFirstName] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)AutomationOrderByEnum.LastModifiedByFullName} THEN A.[LastModifiedByLastName] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)AutomationOrderByEnum.LastModifiedByFirstName} THEN A.[LastModifiedByFirstName] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)AutomationOrderByEnum.LastModifiedByLastName} THEN A.[LastModifiedByLastName] END {request.OrderByDirectionStr}

                {UsePagingSql(usePaging)}
            ";

            return sql;
        }

        private string GetSqlWhere(AutomationRequest request)
        {
            var searchParams = new List<string>();

            if (request.AutomationId > 0) {
                searchParams.Add("A.[AutomationId] = @AutomationId");
            }
            else {
                if (request.ClientId > 0) searchParams.Add("A.[ClientId] = @ClientId");

                if (!string.IsNullOrEmpty(request.Name)) searchParams.Add("A.[Name] LIKE '%' + @Name + '%'");


                if (request.CreatedDateUTC > DateTime.MinValue) searchParams.Add("A.[CreatedDateUTC] = @CreatedDateUTC");
                if (request.CreatedByUserId > 0) searchParams.Add("A.[CreatedByUserId] = @CreatedByUserId");
                if (!string.IsNullOrEmpty(request.CreatedByFirstName)) searchParams.Add("U_C.[FirstName] LIKE '%' + @CreatedByFirstName + '%'");
                if (!string.IsNullOrEmpty(request.CreatedByLastName)) searchParams.Add("U_C.[LastName] LIKE '%' + @CreatedByLastName + '%'");

                if (request.LastModifiedDateUTC > DateTime.MinValue) searchParams.Add("A.[LastModifiedDateUTC] = @LastModifiedDateUTC");
                if (request.LastModifiedByUserId > 0) searchParams.Add("A.[CreatedByUserId] = @LastModifiedByUserId");
                if (!string.IsNullOrEmpty(request.LastModifiedByFirstName)) searchParams.Add("U_M.[FirstName] LIKE '%' + @LastModifiedByFirstName + '%'");
                if (!string.IsNullOrEmpty(request.LastModifiedByLastName)) searchParams.Add("U_M.[LastName] LIKE '%' + @LastModifiedByLastName + '%'");
            }

            return @$"
                {(searchParams.Any() ? "WHERE " + string.Join(" AND ", searchParams) : "")}
            ";
        }

        internal long Count(AutomationRequest request)
        {
            string sql = $@"
                SELECT COUNT(*) FROM [AUTOMATION].[AUTOMATION_TAB]
                {GetSqlWhere(request)}
            ";

            return FirstOrDefault<long>(sql, request);
        }

        internal PagedList<AutomationModel> GetPagedList(AutomationRequest request)
        {
            var items = QueryList<AutomationModel>(GetSql(request, usePaging: true), request);
            return new PagedList<AutomationModel>(items, Count(request), request);
        }

        internal List<AutomationModel> GetList(AutomationRequest request)
        {
            return QueryList<AutomationModel>(GetSql(request), request);
        }

        internal AutomationModel GetById(long automationId)
        {
            var request = new AutomationRequest { AutomationId = automationId };
            return FirstOrDefault<AutomationModel>(GetSql(request), request);
        }

        internal AutomationModel FirstOrDefault (AutomationRequest request)
        {
            return FirstOrDefault<AutomationModel>(GetSql(request), request);
        }

        internal bool Update(AutomationModel model)
        {
            var sql = @"
                UPDATE [AUTOMATION].[AUTOMATION_TAB] SET
                    [Name] = @Name,
                    [LastModifiedByUserId] = @LastModifiedByUserId,
                    [LastModifiedDateUTC] = GETUTCDATE()
                WHERE
                    [AutomationId] = @AutomationId
            ";

            return Execute(sql, model);
        }

        internal long Insert(AutomationModel model)
        {
            var sql = @"
                INSERT INTO [AUTOMATION].[AUTOMATION_TAB] (
                    [Name], [CreatedDateUTC], [CreatedByUserId], [ClientId]
                )
                OUTPUT INSERTED.[AutomationId]
                VALUES (
                    @Name, @CreatedDateUTC, @CreatedByUserId, @ClientId
                )
            ";

            return FirstOrDefault<long>(sql, model);
        }

        internal bool Delete(long automationId)
        {
            var sql = @"
                DELETE FROM [AUTOMATION].[AUTOMATION_TAB]
                WHERE [AutomationId] = @automationId
            ";

            return Execute(sql, new { automationId });
        }
    }
}
