﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Data;
using Turing.Domain.Domain.Enums.User;
using Turing.Domain.Domain.Models.User.Preference;

namespace Turing.Core.Repository.User.Preference
{
    internal class UserTablePreferenceRepository : BaseRepository
    {
        public UserTablePreferenceRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }

        internal UserTablePreferenceModel<TableColumnOrderByEnum> GetById<TableColumnOrderByEnum>(long userId, UserTablePreferenceEnum tableId)
            where TableColumnOrderByEnum : Enum
        {
            // BY SETTING THE UserId TO NULL IN THE TABLE IS IT POSSABLE TO CREATE A FALLBACK LAYOUT
            var sql = @"
                ; WITH TABLE_PREFERENCE As (
                    SELECT UserId, TableId, ColumnOrderStr
                    FROM [USER].[USER_TABLE_PREFERENCE_TAB]
                    WHERE UserId IS NULL AND TableId = @TableId
                    UNION 
                    SELECT UserId, TableId, ColumnOrderStr
                    FROM [USER].[USER_TABLE_PREFERENCE_TAB]
                    WHERE UserId = @userId AND TableId = @tableId
                )
 
                SELECT UserId, TableId, ColumnOrderStr
                FROM TABLE_PREFERENCE
                ORDER BY UserId DESC
            ";

            return FirstOrDefault<UserTablePreferenceModel<TableColumnOrderByEnum>>(sql, new { userId, tableId });
        }

        internal bool Insert<TableColumnOrderByEnum>(UserTablePreferenceModel<TableColumnOrderByEnum> model)
            where TableColumnOrderByEnum : Enum
        {
            var sql = $@"
               INSERT INTO [USER].[USER_TABLE_PREFERENCE_TAB] (
                    UserId, TableId, ColumnOrderStr
                ) 
                VALUES(
                    @UserId, @TableId, '{JsonConvert.SerializeObject(model.ColumnOrder)}'
                )
            ";

            return Execute(sql, GetParameters(model));
        }

        internal bool Update<TableColumnOrderByEnum>(UserTablePreferenceModel<TableColumnOrderByEnum> model)
            where TableColumnOrderByEnum : Enum
        {
            var sql = $@"
                UPDATE [USER].[USER_TABLE_PREFERENCE_TAB] SET
                    ColumnOrderStr = '{JsonConvert.SerializeObject(model.ColumnOrder)}'
                WHERE 
                    UserId = @UserId AND 
                    TableId = @TableId
            ";

            return Execute(sql, GetParameters(model));
        }

        internal bool Delete(long userId, UserTablePreferenceEnum tableId)
        {
            var sql = $@"
                DELETE FROM [USER].[USER_TABLE_PREFERENCE_TAB] 
                WHERE UserId = @userId AND TableId = @tableId
            ";

            return Execute(sql, new { userId, tableId });
        }

        private DynamicParameters GetParameters<TableColumnOrderByEnum>(UserTablePreferenceModel<TableColumnOrderByEnum> model)
            where TableColumnOrderByEnum : Enum
        {
            var p = base.GetParameters(model);
            p.Add("UserId", model.UserId);
            p.Add("TableId", model.TableId);
            p.Add("ColumnOrder", null);
            p.Add("ColumnOrderStr", JsonConvert.SerializeObject(model.ColumnOrder));

            return p;
        }
    }
}
