﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Turing.Core.Request.User;
using Turing.Domain.Models.User;
using Turing.Domain.Infrastructure;

namespace Turing.Core.Repository.User
{
    internal class UserRepository : BaseRepository
    {
        public UserRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }

        private string GetSql(UserRequest request, bool usePaging = false)
        {
            var sql = @$"
                SELECT 
                    U.[UserId], U.[Username], U.[Email], U.[FirstName], U.[LastName], U.[PasswordHash], U.[CurrentClientId]
                FROM [USER_TAB] U

                {GetSqlWhere(request)}

                ORDER BY
                    CASE WHEN @OrderBy = {(int)UserOrderByEnum.UserId} THEN U.[UserId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)UserOrderByEnum.Username} THEN U.[Username] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)UserOrderByEnum.Email} THEN U.[Email] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)UserOrderByEnum.FirstName} THEN U.[FirstName] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)UserOrderByEnum.LastName} THEN U.[LastName] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)UserOrderByEnum.CurrentClientId} THEN U.[CurrentClientId] END {request.OrderByDirectionStr}

                {UsePagingSql(usePaging)}
            ";

            return sql;
        }

        private string GetSqlWhere(UserRequest request)
        {
            var searchParams = new List<string>();

            if (request.UserId > 0)
            {
                searchParams.Add("U.[UserId] = @UserId");
            }
            else
            {
                if (request.ClientId > 0) searchParams.Add("U.[ClientId] = @ClientId");
                if (!string.IsNullOrEmpty(request.Username)) searchParams.Add("U.[Username] LIKE '%' + @Username + '%'");
                if (!string.IsNullOrEmpty(request.Email)) searchParams.Add("U.[Email] LIKE '%' + @Email + '%'");
                if (!string.IsNullOrEmpty(request.FirstName)) searchParams.Add("U.[FirstName] LIKE '%' + @FirstName + '%'");
                if (!string.IsNullOrEmpty(request.LastName)) searchParams.Add("U.[LastName] LIKE '%' + @LastName + '%'");
            }

            return @$"
                {(searchParams.Any() ? "WHERE " + string.Join(" AND ", searchParams) : "")}
            ";
        }

        internal long Count(UserRequest request)
        {
            string sql = $@"
                SELECT COUNT(*) 
                FROM [USER_TAB]
                {GetSqlWhere(request)}
            ";

            return FirstOrDefault<long>(sql, request);
        }

        internal List<LookupItem> GetLookup(UserRequest request)
        {
            string sql = $@"
                SELECT 
                    UserId As 'Id', FirstName As 'Code', LastName As 'Description'
                FROM [USER_TAB]
                {GetSqlWhere(request)}
            ";

            return QueryList<LookupItem>(sql, request);
        }

        internal PagedList<UserModel> GetPagedList(UserRequest request)
        {
            var items = QueryList<UserModel>(GetSql(request, usePaging: true), request);
            return new PagedList<UserModel>(items, Count(request), request);
        }

        internal List<UserModel> GetList(UserRequest request)
        {
            return QueryList<UserModel>(GetSql(request), request);
        }

        internal UserModel GetById(long userId)
        {
            var request = new UserRequest { UserId = userId };
            return FirstOrDefault<UserModel>(GetSql(request), request);
        }

        internal UserModel FirstOrDefault(UserRequest request)
        {
            return FirstOrDefault<UserModel>(GetSql(request), request);
        }

        internal bool Update(UserModel model)
        {
            string sql = @"
                UPDATE [USER_TAB] SET
                    [Email] = @Email, 
                    [FirstName] = @FirstName,
                    [LastName] = @LastName,
                    [CurrentClientId] = @CurrentClientId,
                    [PasswordHash] = @PasswordHash
                WHERE
                    [UserId] = @UserId
            ";
            return Execute(sql, model);
        }

        internal long Insert(UserModel model)
        {
            string sql = @"
                INSERT INTO [USER_TAB] (
                    [Username], [Email], [FirstName], [LastName], [PasswordHash], [CurrentClientId]
                )
                OUTPUT INSERTED.[UserId]
                VALUES (
                    @Username, @Email, @FirstName, @LastName, @PasswordHash, @CurrentClientId
                )
            ";
            return FirstOrDefault<long>(sql, model);
        }
    }
}
