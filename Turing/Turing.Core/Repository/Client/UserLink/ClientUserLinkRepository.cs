﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Turing.Core.Request.Client.UserLink;
using Turing.Domain.Domain.Models.Client.ClientUserLink;
using Turing.Domain.Infrastructure;

namespace Turing.Core.Repository.ClientUserLink.UserLink
{
    internal class ClientUserLinkRepository : BaseRepository
    {
        public ClientUserLinkRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }

        private string GetSql(ClientUserLinkRequest request, bool usePaging = false)
        {
            var sql = @$"
                SELECT 
                    CUL.[ClientUserLinkId], CUL.[ClientId], CUL.[UserId]
                FROM [CLIENT_USER_LINK] CUL

                {GetSqlWhere(request)}

                ORDER BY
                    CASE WHEN @OrderBy = {(int)ClientUserLinkOrderByEnum.ClientUserLinkId} THEN CUL.[ClientUserLinkId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)ClientUserLinkOrderByEnum.ClientId} THEN CUL.[ClientId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)ClientUserLinkOrderByEnum.UserId} THEN CUL.[UserId] END {request.OrderByDirectionStr}

                {UsePagingSql(usePaging)}
            ";

            return sql;
        }

        private string GetSqlWhere(ClientUserLinkRequest request)
        {
            var searchParams = new List<string>();

            if (request.ClientUserLinkId > 0)
                searchParams.Add("CUL.[ClientUserLinkId] = @ClientUserLinkId");
            else {
                if (request.ClientId > 0) searchParams.Add("CUL.[ClientId] = @ClientId");
                if (request.UserId > 0) searchParams.Add("CUL.[UserId] = @UserId");
            }

            return @$"
                {(searchParams.Any() ? " WHERE " + string.Join(" AND ", searchParams) : "")}
            ";
        }

        internal long Count(ClientUserLinkRequest request)
        {
            string sql = $@"
                SELECT COUNT(*) FROM [CLIENT_USER_LINK]
                {GetSqlWhere(request)}
            ";

            return FirstOrDefault<long>(sql, request);
        }

        internal List<ClientUserLinkModel> GetList(ClientUserLinkRequest request)
        {
            return QueryList<ClientUserLinkModel>(GetSql(request), request);
        }

        internal ClientUserLinkModel GetById(long ClientUserLinkId)
        {
            var request = new ClientUserLinkRequest { ClientUserLinkId = ClientUserLinkId };
            return FirstOrDefault<ClientUserLinkModel>(GetSql(request), request);
        }

        internal ClientUserLinkModel FirstOrDefault(ClientUserLinkRequest request)
        {
            return FirstOrDefault<ClientUserLinkModel>(GetSql(request), request);
        }

        internal long Insert(ClientUserLinkModel model)
        {
            string sql = @"
                INSERT INTO [CLIENT_USER_LINK] (
                    [ClientId], [UserId]
                )
                OUTPUT INSERTED.[ClientUserLinkId]
                VALUES (
                    @ClientId, @UserId
                )
                
            ";

            return FirstOrDefault<long>(sql, model);
        }

        internal bool Delete(long clientUserLinkId)
        {
            string sql = @"
                DELETE FROM [CLIENT_USER_LINK]
                WHERE [ClientUserLinkId] = @clientUserLinkId
            ";

            return Execute(sql, new { clientUserLinkId });
        }
    }
}
