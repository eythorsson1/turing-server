﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Turing.Core.Request.Client;
using Turing.Domain.Models.Client;
using Turing.Domain.Infrastructure;

namespace Turing.Core.Repository.Client
{
    internal class ClientRepository : BaseRepository
    {
        public ClientRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }

        private string GetSql(ClientRequest request, bool usePaging = false)
        {
            var sql = @$"
                SELECT 
                    C.[Name], C.[UniqueIdentifier], C.[CreatedDateUTC], C.[CreatedByUserId], C.[CreatedByFirstName], C.[CreatedByLastName], C.[CreatedByEmail]
                FROM [Client_TAB] C

                {GetSqlWhere(request)}

                ORDER BY
                    CASE WHEN @OrderBy = {(int)ClientOrderByEnum.ClientId} THEN C.[ClientId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)ClientOrderByEnum.Name} THEN C.[Name] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)ClientOrderByEnum.UniqueIdentifier} THEN C.[UniqueIdentifier] END {request.OrderByDirectionStr},

                    CASE WHEN @OrderBy = {(int)ClientOrderByEnum.CreatedDateUTC} THEN C.[CreatedDateUTC] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)ClientOrderByEnum.CreatedByUserId} THEN C.[CreatedByUserId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)ClientOrderByEnum.CreatedByFullName} THEN C.[CreatedByFirstName] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)ClientOrderByEnum.CreatedByFullName} THEN C.[CreatedByLastName] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)ClientOrderByEnum.CreatedByFirstName} THEN C.[CreatedByFirstName] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)ClientOrderByEnum.CreatedByLastName} THEN C.[CreatedByLastName] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)ClientOrderByEnum.CreatedByEmail} THEN C.[CreatedByEmail] END {request.OrderByDirectionStr}

                {UsePagingSql(usePaging)}
            ";

            return sql;
        }

        private string GetSqlWhere(ClientRequest request)
        {
            var searchParams = new List<string>();

            if (request.ClientId > 0)
                searchParams.Add("C.[ClientId] = @ClientId");
            else
            {
                if (request.ClientId > 0) searchParams.Add("C.[ClientId] = @ClientId");
                if (!string.IsNullOrEmpty(request.Name)) searchParams.Add("C.[Name] LIKE '%' + @Name + '%'");
                if (!string.IsNullOrEmpty(request.UniqueIdentifier)) searchParams.Add("C.[UniqueIdentifier] = @UniqueIdentifier");

                if (request.CreatedDateUTC > DateTime.MinValue) searchParams.Add("CAST(C.[CreatedDateUTC] As DATE) = CAST(@CreatedDateUTC As DATE)");
                if (request.CreatedByUserId > 0) searchParams.Add("C.[CreatedByUserId] = @CreatedByUserId");
                if (!string.IsNullOrEmpty(request.CreatedByFullName)) searchParams.Add("CONCAT(C.[CreatedByFirstName], ' ', C.[CreatedByLastName]) = @CreatedByFullName");
                if (!string.IsNullOrEmpty(request.CreatedByFirstName)) searchParams.Add("C.[CreatedByFirstName] = @CreatedByFirstName");
                if (!string.IsNullOrEmpty(request.CreatedByLastName)) searchParams.Add("C.[CreatedByLastName] = @CreatedByLastName");
                if (!string.IsNullOrEmpty(request.CreatedByEmail)) searchParams.Add("C.[CreatedByEmail] = @CreatedByEmail");
            }

            return @$"
                {(searchParams.Any() ? " WHERE " + string.Join(" AND ", searchParams) : "")}
            ";
        }

        internal long Count(ClientRequest request)
        {
            string sql = $@"
                SELECT COUNT(*) FROM [Client_TAB]
                {GetSqlWhere(request)}
            ";

            return FirstOrDefault<long>(sql, request);
        }

        internal PagedList<ClientModel> GetPagedList(ClientRequest request)
        {
            var items = QueryList<ClientModel>(GetSql(request, usePaging: true), request);
            return new PagedList<ClientModel>(items, Count(request), request);
        }

        internal List<ClientModel> GetList(ClientRequest request)
        {
            return QueryList<ClientModel>(GetSql(request), request);
        }

        internal ClientModel GetById(long ClientId)
        {
            var request = new ClientRequest { ClientId = ClientId };
            return FirstOrDefault<ClientModel>(GetSql(request), request);
        }

        internal ClientModel FirstOrDefault(ClientRequest request)
        {
            return FirstOrDefault<ClientModel>(GetSql(request), request);
        }

        internal bool Update(ClientModel model)
        {
            string sql = @"
                UPDATE [CLIENT_TAB] SET
                    [Name] = @Name
                WHERE
                    [ClientId] = @ClientId
            ";

            return Execute(sql, model);
        }

        internal long Insert(ClientModel model)
        {
            string sql = @"
                INSERT INTO [CLIENT_TAB](
                    [Name], [UniqueIdentifier], [CreatedDateUTC], [CreatedByUserId], [CreatedByFirstName], [CreatedByLastName], [CreatedByEmail]
                )
                OUTPUT INSERTED.[ClientId]
                SELECT 
                    @Name, @UniqueIdentifier, @CreatedDateUTC, @CreatedByUserId, U.[FirstName], U.[LastName], U.[Email]
                FROM [USER_TAB] U 
                WHERE U.[UserId] = @CreatedByUserId
                
            ";

            return FirstOrDefault<long>(sql, model);
        }
    }
}
