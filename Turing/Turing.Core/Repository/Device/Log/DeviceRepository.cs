﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Turing.Core.Request.Device.Log;
using Turing.Domain.Models.Device.Log;
using Turing.Domain.Infrastructure;
using Dapper;

namespace Turing.Core.Repository.DeviceLog
{
    internal class DeviceLogRepository : BaseRepository
    {
        public DeviceLogRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }

        private string GetSql(DeviceLogFilterRequest request, bool usePaging = false)
        {
            var sql = @$"
                SELECT 
                    DL.[DeviceLogId], DL.[DeviceId], DL.[Action], DL.[ActionDateUTC], DL.[ActionByUserId], DL.[ActionByEmail], DL.[ActionByFirstName], 
                    DL.[ActionByLastName], DL.[Description], DL.[TechnicalDescription], DL.[ClientId]
                    
                    D.[Name] As 'DeviceName'
                FROM [DEVICE].[DEVICE_LOG_TAB] DL
                    INNER JOIN [DEVICE_TAB] D ON D.[DeviceId] = DL.[DeviceId]

                {GetSqlWhere(request)}

                ORDER BY
                    CASE WHEN @OrderBy = {(int)DeviceLogOrderByEnum.DeviceLogId} THEN DL.[DeviceLogId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceLogOrderByEnum.DeviceId} THEN DL.[DeviceId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceLogOrderByEnum.Action} THEN DL.[Action] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceLogOrderByEnum.ActionDateUTC} THEN DL.[ActionDateUTC] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceLogOrderByEnum.ActionByUserId} THEN DL.[ActionByUserId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceLogOrderByEnum.ActionByEmail} THEN DL.[ActionByEmail] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceLogOrderByEnum.ActionByFirstName} THEN DL.[ActionByFirstName] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceLogOrderByEnum.ActionByLastName} THEN DL.[ActionByLastName] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceLogOrderByEnum.Description} THEN DL.[Description] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceLogOrderByEnum.TechnicalDescription} THEN DL.[TechnicalDescription] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceLogOrderByEnum.ClientId} THEN DL.[ClientId] END {request.OrderByDirectionStr},

                    CASE WHEN @OrderBy = {(int)DeviceLogOrderByEnum.DeviceName} THEN D.[Name] END {request.OrderByDirectionStr}

                {UsePagingSql(usePaging)}
            ";

            return sql;
        }

        private string GetSqlWhere(DeviceLogFilterRequest request)
        {
            var searchParams = new List<string>();

            if (request.DeviceLogId > 0)
            {
                searchParams.Add("DL.[DeviceLogId] = @DeviceLogId");
            }
            else
            {
                if (request.ClientId > 0) searchParams.Add("DL.[ClientId] = @ClientId");
                if (request.DeviceId > 0) searchParams.Add("DL.[DeviceId] = @DeviceId");

                if (request.Action > 0) searchParams.Add("DL.[Action] = @Action");
                if (request.ActionDateUTC >  DateTime.MinValue) searchParams.Add("DL.[ActionDateUTC] = @ActionDateUTC");
                if (request.ActionByUserId > 0) searchParams.Add("DL.[ActionByUserId] = @ActionByUserId");
                if (!string.IsNullOrEmpty(request.ActionByEmail)) searchParams.Add("DL.[ActionByEmail] LIKE '%' + @ActionByEmail + '%'");
                if (!string.IsNullOrEmpty(request.ActionByFirstName)) searchParams.Add("DL.[ActionByFirstName] LIKE '%' + @ActionByFirstName + '%'");
                if (!string.IsNullOrEmpty(request.ActionByLastName)) searchParams.Add("DL.[ActionByLastName] LIKE '%' + @ActionByLastName + '%'");

                if (!string.IsNullOrEmpty(request.Description)) searchParams.Add("DL.[Description] LIKE '%' + @ActionByLastName + '%'");
                if (!string.IsNullOrEmpty(request.TechnicalDescription)) searchParams.Add("DL.[TechnicalDescription] LIKE '%' + @TechnicalDescription + '%'");
                
                // AGGREGATED
                if (!string.IsNullOrEmpty(request.DeviceName)) searchParams.Add("D.[Name] LIKE '%' + @DeviceName + '%'");
            }

            return @$"
                {(searchParams.Any() ? " WHERE " + string.Join(" AND ", searchParams) : "")}
            ";
        }

        internal long Count(DeviceLogFilterRequest request)
        {
            string sql = $@"
                SELECT COUNT(*) FROM [DEVICE].[DEVICE_LOG_TAB]
                {GetSqlWhere(request)}
            ";

            return FirstOrDefault<long>(sql, request);
        }

        internal PagedList<DeviceLogModel> GetPagedList(DeviceLogFilterRequest request)
        {
            var items = QueryList<DeviceLogModel>(GetSql(request, usePaging: true), request);
            return new PagedList<DeviceLogModel>(items, Count(request), request);
        }

        internal List<DeviceLogModel> GetList(DeviceLogFilterRequest request)
        {
            return QueryList<DeviceLogModel>(GetSql(request), request);
        }

        internal DeviceLogModel GetById(long DeviceLogId)
        {
            var request = new DeviceLogFilterRequest { DeviceLogId = DeviceLogId };
            return FirstOrDefault<DeviceLogModel>(GetSql(request), request);
        }

        internal long Insert(DeviceLogModel model)
        {
            var sql = @"
                INSERT INTO [DEVICE].[DEVICE_LOG_TAB] (
                    [DeviceId], [Action], [ActionDateUTC], [ActionByUserId], [ActionByEmail], [ActionByFirstName], 
                    [ActionByLastName], [Description], [TechnicalDescription], [ClientId]
                )
                OUTPUT INSERTED.[DeviceLogId]
                VALUES (
                    @DeviceId, @Action, @ActionDateUTC, @ActionByUserId, @ActionByEmail, @ActionByFirstName, 
                    @ActionByLastName, @Description, @TechnicalDescription, @ClientId
                )
            ";

            return FirstOrDefault<long>(sql, model);
        }

        //private DynamicParameters GetParameters(DeviceLogModel model)
        //{
        //    var p = base.GetParameters(model);
        //    p.Add(nameof(model.ActionByUserId), model.ActionByUserId);
        //    return p;
        //}
    }
}
