﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Turing.Core.Request.Device;
using Turing.Domain.Models.Device;
using Turing.Domain.Infrastructure;
using System;

namespace Turing.Core.Repository.Device
{
    internal class DeviceRepository : BaseRepository
    {
        public DeviceRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }

        private string GetSql(DeviceRequest request, bool usePaging = false)
        {
            var sql = @$"
                SELECT 
                    D.[DeviceId], D.[Name], D.[ChipId], D.[SetTopic], D.[StateTopic], D.[DataPresetId], D.[AuthName], D.[AuthPasswordHash], 
                    D.[CreatedDateUTC], D.[CreatedByUserId], D.[ClientId],
                    UC.[FirstName] As 'CreatedByFirstName', UC.[LastName] As 'CreatedByLastName', UC.[Email] As 'CreatedByEmail',
                    UM.[FirstName] As 'LastModifiedByFirstName', UM.[LastName] As 'LastModifiedByLastName', UM.[Email] As 'LastModifiedByEmail'
                FROM [DEVICE].[DEVICE_TAB] D
                    INNER JOIN [USER_TAB] UC ON UC.[UserId] = D.[CreatedByUserId]
                    LEFT OUTER JOIN [USER_TAB] UM ON UM.[UserId] = D.[LastModifiedByUserId]


                {GetSqlWhere(request)}

                ORDER BY
                    CASE WHEN @OrderBy = {(int)DeviceOrderByEnum.DeviceId} THEN D.[DeviceId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceOrderByEnum.Name} THEN D.[Name] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceOrderByEnum.SetTopic} THEN D.[SetTopic] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceOrderByEnum.StateTopic} THEN D.[StateTopic] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceOrderByEnum.AuthName} THEN D.[AuthName] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceOrderByEnum.ClientId} THEN D.[ClientId] END {request.OrderByDirectionStr},

                    CASE WHEN @OrderBy = {(int)DeviceOrderByEnum.CreatedDateUTC} THEN D.[CreatedDateUTC] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceOrderByEnum.CreatedByUserId} THEN D.[CreatedByUserId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceOrderByEnum.CreatedByFullName} THEN UC.[FirstName] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceOrderByEnum.CreatedByFullName} THEN UC.[LastName] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceOrderByEnum.CreatedByFirstName} THEN UC.[FirstName] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceOrderByEnum.CreatedByLastName} THEN UC.[LastName] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceOrderByEnum.CreatedByEmail} THEN UC.[Email] END {request.OrderByDirectionStr},

                    CASE WHEN @OrderBy = {(int)DeviceOrderByEnum.LastModifiedDateUTC} THEN D.[LastModifiedDateUTC] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceOrderByEnum.LastModifiedByUserId} THEN D.[LastModifiedByUserId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceOrderByEnum.LastModifiedByFullName} THEN UM.[FirstName] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceOrderByEnum.LastModifiedByFullName} THEN UM.[LastName] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceOrderByEnum.LastModifiedByFirstName} THEN UM.[FirstName] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceOrderByEnum.LastModifiedByLastName} THEN UM.[LastName] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceOrderByEnum.LastModifiedByEmail} THEN UM.[Email] END {request.OrderByDirectionStr}


                {UsePagingSql(usePaging)}
            ";

            return sql;
        }

        private string GetSqlWhere(DeviceRequest request)
        {
            var searchParams = new List<string>();

            if (request.DeviceId > 0)
            {
                searchParams.Add("D.[DeviceId] = @DeviceId");
            }
            else
            {
                if (request.ClientId > 0) searchParams.Add("D.[ClientId] = @ClientId");

                if (!string.IsNullOrEmpty(request.Name)) searchParams.Add("D.[Name] LIKE '%' + @Name + '%'");
                if (!string.IsNullOrEmpty(request.ChipId)) searchParams.Add("D.[ChipId] = @ChipId");
                if (!string.IsNullOrEmpty(request.AuthName)) searchParams.Add("D.[AuthName] = @AuthName");
                if (!string.IsNullOrEmpty(request.SetTopic)) searchParams.Add("D.[SetTopic] LIKE '%' + @SetTopic + '%'");
                if (!string.IsNullOrEmpty(request.StateTopic)) searchParams.Add("D.[StateTopic] LIKE '%' + @StateTopic + '%'");

                if (request.CreatedDateUTC > DateTime.MinValue) searchParams.Add("CAST(D.[CreatedDateUTC] As DATE) = CAST(@CreatedDateUTC As DATE)");
                if (request.CreatedByUserId > 0) searchParams.Add("D.[CreatedByUserId] = @CreatedByUserId");
                if (!string.IsNullOrEmpty(request.CreatedByFullName)) searchParams.Add("CONCAT(UC.[FirstName], ' ', UC.[LastName]) LIKE '%' + @CreatedByFullName + '%'");
                if (!string.IsNullOrEmpty(request.CreatedByFirstName)) searchParams.Add("UC.[FirstName] LIKE '%' + @CreatedByFirstName + '%'");
                if (!string.IsNullOrEmpty(request.CreatedByLastName)) searchParams.Add("UC.[LastName] LIKE '%' + @CreatedByLastName + '%'");
                if (!string.IsNullOrEmpty(request.CreatedByEmail)) searchParams.Add("UC.[Email] LIKE '%' + @CreatedByEmail + '%'");


                if (request.LastModifiedDateUTC > DateTime.MinValue) searchParams.Add("CAST(D.[LastModifiedDateUTC] As DATE) = CAST(@LastModifiedDateUTC As DATE)");
                if (request.LastModifiedByUserId > 0) searchParams.Add("D.[CreatedByUserId] = @CreatedByUserId");
                if (!string.IsNullOrEmpty(request.LastModifiedByFullName)) searchParams.Add("CONCAT(UM.[FirstName], ' ', UM.[LastName]) LIKE '%' + @LastModifiedByFullName + '%'");
                if (!string.IsNullOrEmpty(request.LastModifiedByFirstName)) searchParams.Add("UM.[FirstName] LIKE '%' + @LastModifiedByFirstName + '%'");
                if (!string.IsNullOrEmpty(request.LastModifiedByLastName)) searchParams.Add("UM.[LastName] LIKE '%' + @LastModifiedByLastName + '%'");
                if (!string.IsNullOrEmpty(request.LastModifiedByEmail)) searchParams.Add("UM.[Email] LIKE '%' + @LastModifiedByEmail + '%'");

                if (request.DeviceIds != null && request.DeviceIds.Any()) searchParams.Add("D.[DeviceId] IN @DeviceIds");
            }

            return @$"
                {(searchParams.Any() ? " WHERE " + string.Join(" AND ", searchParams) : "")}
            ";
        }

        internal long Count(DeviceRequest request)
        {
            string sql = $@"
                SELECT COUNT(*) FROM [DEVICE].[DEVICE_TAB]
                {GetSqlWhere(request)}
            ";

            return FirstOrDefault<long>(sql, request);
        }


        internal List<LookupItem> GetLookup(DeviceRequest request)
        {
            string sql = $@"
                SELECT [DeviceId] As 'Id', [Name] As 'Description'
                FROM [DEVICE].[DEVICE_TAB]
                {GetSqlWhere(request)}
            ";

            return QueryList<LookupItem>(sql, request);
        }

        internal PagedList<DeviceModel> GetPagedList(DeviceRequest request)
        {
            var items = QueryList<DeviceModel>(GetSql(request, usePaging: true), request);
            return new PagedList<DeviceModel>(items, Count(request), request);
        }
    
        internal List<DeviceModel> GetList(DeviceRequest request)
        {
            return QueryList<DeviceModel>(GetSql(request), request);
        }

        internal DeviceModel FirstOrDefault(DeviceRequest request)
        {
            return FirstOrDefault<DeviceModel>(GetSql(request), request);
        }

        internal DeviceModel GetById(long deviceId)
        {
            var request = new DeviceRequest { DeviceId = deviceId };
            return FirstOrDefault<DeviceModel>(GetSql(request), request);
        }

        internal bool Update(DeviceModel model)
        {
            var sql = @"
                UPDATE [DEVICE].[DEVICE_TAB] SET
                    [Name] = @Name,
                    [ChipId] = @ChipId,
                    [SetTopic] = @SetTopic,
                    [StateTopic] = @StateTopic, 
                    [DataPresetId] = @DataPresetId,
                    [AuthPasswordHash] = @AuthPasswordHash,
                    [LastModifiedDateUTC] = GETUTCDATE(),
                    [LastModifiedByUserId] = @LastModifiedByUserId
                WHERE 
                    [DeviceId] = @DeviceId 
            ";

            return Execute(sql, model);
        }

        internal long Insert(DeviceModel model)
        {
            var sql = @"
                INSERT INTO [DEVICE].[DEVICE_TAB] (
                    [Name], [ChipId], [SetTopic], [StateTopic], [DataPresetId], [AuthName], [AuthPasswordHash], [CreatedDateUTC], 
                    [CreatedByUserId], [ClientId]
                )
                OUTPUT INSERTED.[DeviceId]
                VALUES (
                    @Name, @ChipId, @SetTopic, @StateTopic, @DataPresetId, @AuthName, @AuthPasswordHash, @CreatedDateUTC, 
                    @CreatedByUserId, @ClientId
                )
            ";

            return FirstOrDefault<long>(sql, model);
        }

        internal bool Delete(long deviceId)
        {
            var sql = @"
                DELETE FROM [DEVICE].[DEVICE_TAB] SET
                WHERE [DeviceId] = @deviceId 
            ";

            return Execute(sql, new { deviceId });
        }
    }
}
