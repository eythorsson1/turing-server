﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Turing.Core.Request.Device.Feature.Data;
using Turing.Domain.Domain.Models.Device.Feature.Data;

namespace Turing.Core.Repository.Device.Type.Feature.Data
{

    internal class DeviceFeatureDataRepository : BaseRepository
    {
        public DeviceFeatureDataRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }

        private string GetSql(DeviceFeatureDataRequest request, bool usePaging = false)
        {
            var sql = @$"
                SELECT 
	                DTFD.[DeviceFeatureDataId], DTF.[DeviceFeatureId], DTF.[DeviceId], DTFD.[Value], 
                    DTF.[Type], DTF.[Name]
                FROM [DEVICE].[DEVICE_FEATURE_TAB] DTF
	                LEFT OUTER JOIN [DEVICE].[DEVICE_FEATURE_DATA_TAB] DTFD ON DTF.[DeviceFeatureId] = DTFD.[DeviceFeatureId]
                {GetSqlWhere(request)}

                ORDER BY
                    CASE WHEN @OrderBy = {(int)DeviceFeatureDataOrderByEnum.DeviceFeatureDataId} THEN DTFD.[DeviceFeatureDataId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceFeatureDataOrderByEnum.DeviceFeatureId} THEN DTFD.[DeviceFeatureId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceFeatureDataOrderByEnum.DeviceId} THEN DTFD.[DeviceId] END {request.OrderByDirectionStr}

                {UsePagingSql(usePaging)}
            ";

            return sql;
        }

        private string GetSqlWhere(DeviceFeatureDataRequest request)
        {
            var searchParams = new List<string>();

            if (request.DeviceFeatureDataId > 0)
                searchParams.Add("DTFD.[DeviceFeatureDataId] = @DeviceFeatureDataId");
            else {
                if (request.DeviceId > 0) searchParams.Add("DTFD.[DeviceId] = @DeviceId");
                if (request.DeviceFeatureId > 0) searchParams.Add("DTFD.[DeviceFeatureId] = @DeviceFeatureId");

                if (request.DeviceIds != null && request.DeviceIds.Any()) searchParams.Add("DTFD.[DeviceId] IN @DeviceIds");
            }

            return @$"
                {(searchParams.Any() ? " WHERE " + string.Join(" AND ", searchParams) : "")}
            ";
        }

        internal long Count(DeviceFeatureDataRequest request)
        {
            string sql = @$"
                SELECT COUNT(*) 
                FROM [DEVICE].[DEVICE_FEATURE_TAB] DTF
	                LEFT OUTER JOIN [DEVICE].[DEVICE_FEATURE_DATA_TAB] DTFD ON DTF.[DeviceFeatureId] = DTFD.[DeviceFeatureId]
                {GetSqlWhere(request)}
            ";

            return FirstOrDefault<long>(sql, request);
        }

        internal List<DeviceFeatureDataModel> GetList(DeviceFeatureDataRequest request)
        {
            return QueryList<DeviceFeatureDataModel>(GetSql(request), request);
        }

        internal DeviceFeatureDataModel FirstOrDefault(DeviceFeatureDataRequest request)
        {
            return FirstOrDefault<DeviceFeatureDataModel>(GetSql(request), request);
        }

        internal DeviceFeatureDataModel GetById(long deviceFeatureDataId)
        {
            var request = new DeviceFeatureDataRequest { DeviceFeatureDataId = deviceFeatureDataId };
            return FirstOrDefault<DeviceFeatureDataModel>(GetSql(request), request);
        }

        internal bool Update(DeviceFeatureDataModel model)
        {
            var sql = @"
                UPDATE [DEVICE].[DEVICE_FEATURE_DATA_TAB] SET
                    [Value] = @Value, 
                WHERE 
                    [DeviceFeatureDataId] = @DeviceFeatureDataId 
            ";

            return Execute(sql, model);
        }

        internal long Insert(DeviceFeatureDataModel model)
        {
            var sql = @"
                INSERT INTO [DEVICE].[DEVICE_FEATURE_DATA_TAB] (
                    [DeviceFeatureId], [DeviceId], [Value]
                )
                OUTPUT INSERTED.[DeviceFeatureDataId]
                VALUES (
                    @DeviceFeatureId, @DeviceId, @Value
                )
            ";

            return FirstOrDefault<long>(sql, model);
        }

        internal bool Delete(long deviceFeatureDataId)
        {
            var sql = @"
                DELETE FROM [DEVICE].[DEVICE_FEATURE_DATA_TAB]
                WHERE [DeviceFeatureDataId] = @deviceFeatureDataId 
            ";

            return Execute(sql, new { deviceFeatureDataId });
        }
    }
}
