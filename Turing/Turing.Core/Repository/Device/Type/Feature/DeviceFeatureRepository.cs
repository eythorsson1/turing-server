﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Turing.Core.Request.Device.Feature;
using Turing.Domain.Domain.Models.Device.Feature;
using Turing.Domain.Infrastructure;
using Turing.Domain.Infrastructure.Extensions;

namespace Turing.Core.Repository.Device.Type.Feature
{

    internal class DeviceFeatureRepository : BaseRepository
    {
        public DeviceFeatureRepository(IDbConnection dbConnection, IDbTransaction dbTransaction) : base(dbConnection, dbTransaction)
        {
        }

        private string GetSql(DeviceFeatureRequest request, bool usePaging = false)
        {
            var sql = @$"
                SELECT 
                    DTF.[DeviceFeatureId], DTF.[DeviceId], DTF.[Type], DTF.[Name], DTF.[DimensionId]
                FROM [DEVICE].[DEVICE_FEATURE_TAB] DTF

                {GetSqlWhere(request)}

                ORDER BY
                    CASE WHEN @OrderBy = {(int)DeviceFeatureOrderByEnum.DeviceFeatureId} THEN DTF.[DeviceFeatureId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceFeatureOrderByEnum.DeviceId} THEN DTF.[DeviceId] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceFeatureOrderByEnum.Type} THEN DTF.[Type] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceFeatureOrderByEnum.Name} THEN DTF.[Name] END {request.OrderByDirectionStr},
                    CASE WHEN @OrderBy = {(int)DeviceFeatureOrderByEnum.DimensionId} THEN DTF.[DimensionId] END {request.OrderByDirectionStr}

                {UsePagingSql(usePaging)}
            ";

            return sql;
        }

        private string GetSqlWhere(DeviceFeatureRequest request)
        {
            var searchParams = new List<string>();

            if (request.DeviceFeatureId > 0)
                searchParams.Add("DTF.[DeviceFeatureId] = @DeviceFeatureId");
            else {
                if (request.DeviceId > 0) searchParams.Add("DTF.[DeviceId] = @DeviceId");
                if (request.DeviceIds != null && request.DeviceIds.Any()) searchParams.Add("DTF.[DeviceId] IN @DeviceIds");
                if (request.Type > 0) searchParams.Add("DTF.[Type] = @Type");
                if (request.DimensionId > 0) searchParams.Add("DTF.[DimensionId] = @DimensionId");

                if (!string.IsNullOrEmpty(request.Name)) searchParams.Add("DTF.[Name] LIKE '%' + @Name + '%'");

                if (!request.DeviceFeatureIds.Empty()) searchParams.Add("DTF.[DeviceFeatureId] IN @DeviceFeatureIds");
            }

            return @$"
                {(searchParams.Any() ? " WHERE " + string.Join(" AND ", searchParams) : "")}
            ";
        }

        internal long Count(DeviceFeatureRequest request)
        {
            string sql = @$"
                SELECT COUNT(*) FROM [DEVICE].[DEVICE_FEATURE_TAB]
                {GetSqlWhere(request)}
            ";

            return FirstOrDefault<long>(sql, request);
        }
        internal List<LookupItem> GetLookup(DeviceFeatureRequest request)
        {
            string sql = @$"
                SELECT [DeviceFeatureId] As 'Id', [Name] As 'Description' 
                FROM [DEVICE].[DEVICE_FEATURE_TAB]
                {GetSqlWhere(request)}
            ";

            return QueryList<LookupItem>(sql, request);
        }

        internal PagedList<DeviceFeatureModel> GetPagedList(DeviceFeatureRequest request)
        {
            var items = QueryList<DeviceFeatureModel>(GetSql(request, usePaging: true), request);
            return new PagedList<DeviceFeatureModel>(items, Count(request), request);
        }

        internal List<DeviceFeatureModel> GetList(DeviceFeatureRequest request)
        {
            return QueryList<DeviceFeatureModel>(GetSql(request), request);
        }

        internal DeviceFeatureModel FirstOrDefault(DeviceFeatureRequest request)
        {
            return FirstOrDefault<DeviceFeatureModel>(GetSql(request), request);
        }

        internal DeviceFeatureModel GetById(long deviceId)
        {
            var request = new DeviceFeatureRequest { DeviceFeatureId = deviceId };
            return FirstOrDefault<DeviceFeatureModel>(GetSql(request), request);
        }

        internal bool Update(DeviceFeatureModel model)
        {
            var sql = @"
                UPDATE [DEVICE].[DEVICE_FEATURE_TAB] SET
                    [Name] = @Name, [DimensionId] = @DimensionId
                WHERE 
                    [DeviceFeatureId] = @DeviceFeatureId 
            ";

            return Execute(sql, model);
        }

        internal long Insert(DeviceFeatureModel model)
        {
            var sql = @"
                INSERT INTO [DEVICE].[DEVICE_FEATURE_TAB] (
                    [DeviceId], [Type], [Name], [DimensionId]
                )
                OUTPUT INSERTED.[DeviceFeatureId]
                VALUES (
                    @DeviceId, @Type, @Name, @DimensionId
                )
            ";

            return FirstOrDefault<long>(sql, model);
        }

        internal bool Delete(long deviceId)
        {
            var sql = @"
                DELETE FROM [DEVICE].[DEVICE_FEATURE_TAB] SET
                WHERE [DeviceFeatureId] = @deviceId 
            ";

            return Execute(sql, new { deviceId });
        }
    }
}
