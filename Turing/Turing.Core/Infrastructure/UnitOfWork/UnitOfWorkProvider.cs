﻿
using System.Data.SqlClient;

namespace Turing.Core.Infrastructure.UnitOfWork
{
    public class UnitOfWorkProvider : IUnitOfWorkProvider
    {
        public static string SqlConnectionString { get; set; }
        private static SqlConnection SqlConnection => new SqlConnection(SqlConnectionString);

        public IUnitOfWork GetUnitOfWork(bool useTransaction = false)
        {
            return new UnitOfWork(SqlConnection, useTransaction);
        }
    }
}
