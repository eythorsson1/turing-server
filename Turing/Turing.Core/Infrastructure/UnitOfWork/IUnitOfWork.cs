﻿using System;
using System.Data;
using System.Threading.Tasks;

namespace Turing.Core.Infrastructure.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IDbConnection IDBConnection { get; }
        IDbTransaction IDBTransaction { get; }

        void Commit();
    }
}
