﻿using System.Data;

namespace Turing.Core.Infrastructure.UnitOfWork
{
    internal class UnitOfWork : IUnitOfWork
    {
        public IDbConnection IDBConnection { get; }

        public IDbTransaction IDBTransaction { get; }

        public UnitOfWork(IDbConnection connection, bool useTransaction)
        {
            IDBConnection = connection;
            IDBConnection.Open();

            if (useTransaction)
                IDBTransaction = IDBConnection.BeginTransaction(IsolationLevel.Serializable);
            
        }
        public void Commit()
        {
            if (IDBTransaction != null)
                IDBTransaction.Commit();
        }

        public void Dispose()
        {
            if (IDBTransaction != null)
                IDBTransaction.Dispose();

            if (IDBConnection != null)
                IDBConnection.Dispose();
        }
    }
}
