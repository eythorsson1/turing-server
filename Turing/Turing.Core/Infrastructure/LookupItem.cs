﻿using Turing.Core.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Turing.Domain.Infrastructure
{
    public class LookupItem : LookupItem<long> { }

    public class LookupItem<T>
    {
        public T Id { get; }
        internal string Code { get; }
        internal string Description { get; }
        //public string Value => string.Join(" - ", new string[] { Code, Value }.Where(x => !string.IsNullOrWhiteSpace(x)));
    }
}
