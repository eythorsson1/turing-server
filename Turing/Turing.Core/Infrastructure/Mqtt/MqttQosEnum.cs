﻿namespace Turing.Core.Infrastructure.Mqtt
{
    public enum MqttQosEnum
    {
        AtMostOnce = 0,
        AtLeastOnce = 1,
        ExactlyOnce = 2
    }
}
