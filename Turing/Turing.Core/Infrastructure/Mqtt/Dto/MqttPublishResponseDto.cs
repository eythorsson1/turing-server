﻿using Newtonsoft.Json;
using System.Text;

namespace Turing.Core.Infrastructure.Mqtt.Dto
{
    public class MqttPublishResponseDto
    {
        public long MessageId { get; set; }
        //public bool Success { get; set; }
        public object Data { get; set; }

        public MqttPublishResponseDto(byte[] payloadBytes)
        {
            if (payloadBytes == null) return;
            var payloadStr = Encoding.Default.GetString(payloadBytes);
            var obj = JsonConvert.DeserializeObject<MqttPublishResponseDto>(payloadStr);
            this.MessageId = obj.MessageId;
            this.Data = obj.Data;
        }

        public override string ToString() => JsonConvert.SerializeObject(this);
    }
}
