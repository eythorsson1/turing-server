﻿using Newtonsoft.Json;

namespace Turing.Core.Infrastructure.Mqtt.Dto
{
    public class MqttPublishRequestDto
    {
        public long MessageId { get; }
        public object Data { get; }

        private MqttPublishRequestDto() { }

        public MqttPublishRequestDto(long messageId, object data)
        {
            MessageId = messageId;
            Data = data;
        }

        public override string ToString() => JsonConvert.SerializeObject(this);
    }
}
