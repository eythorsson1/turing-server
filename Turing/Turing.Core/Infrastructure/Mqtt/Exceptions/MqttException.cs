﻿using System;

namespace Turing.Core.Infrastructure.Mqtt.Exceptions
{
    public class MqttException : Exception
    {
        public MqttException()
        {
        }

        public MqttException(string message) : base(message)
        {
        }
    }
}
