﻿using System;
using System.Linq;
using MQTTnet;
using MQTTnet.Server;
using Turing.Core.Infrastructure.Mqtt.Interceptor;
using Turing.Domain.Exceptions;
using Turing.Domain.Infrastructure.Helper;

namespace Turing.Core.Infrastructure.Mqtt
{
    public class MqttServer
    {
        public MqttServerOptionsBuilder Options { get; set; }
        public static IMqttServer Server { get; private set; }

        internal static event EventHandler<MqttApplicationMessage> OnMessageIntercept;

        public MqttServer()
        {
            Options = new MqttServerOptionsBuilder()
                .WithConnectionValidator(e => ConnectionInterceptor.ConnectionValidator(e))
                //.WithDisconnectedInterceptor(e=> DisconnectionInterceptor.HandleIntersection(e))
                .WithSubscriptionInterceptor(e => SubscriptionInterceptor.HandleIntersection(e))
                .WithApplicationMessageInterceptor(e => {
                    MessageInterceptor.HandleIntersection(e);

                    if (OnMessageIntercept != null) {
                        OnMessageIntercept.Invoke(this, e.ApplicationMessage);
                    }
                });

            Server = new MqttFactory().CreateMqttServer();
            Start();
        }

        public async void Start()
        {
            if (Server.IsStarted) return;
            //Server.ClientUnsubscribedTopicHandler += IUnsubscribedHandler;

            await Server.StartAsync(Options.Build());
        }

        public static void ValidateTopic(string topic)
        {
            var allowedCharacters = StringHelper.LegalLetters.ToList();
            allowedCharacters.Add('/');
            allowedCharacters.Add('-');

            if (string.IsNullOrEmpty(topic))
                throw new FeedbackException("You must specify a topic to which the action should publish the message");

            if (!StringHelper.LegalLetters.Contains(topic[0]))
                throw new FeedbackException("The topic may only start with a character or number");

            if (topic.ToCharArray().Any(c => !allowedCharacters.Contains(c)))
                throw new FeedbackException("The topic contains illegal characters");
        }
    }
}
