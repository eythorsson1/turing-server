﻿using MQTTnet;
using MQTTnet.Server;
using System.Threading;
using System.Threading.Tasks;
using Turing.Core.Infrastructure.Mqtt.Dto;

namespace Turing.Core.Infrastructure.Mqtt
{
    public class MqttAction
    {
        /// <summary>
        /// This method does not log the response.
        /// </summary>
        /// <param name="setTopic">Of which to send the request</param>
        /// <param name="payload"></param>
        /// <param name="stateTopic">The topic that the device responds to</param>
        /// <returns>The publish callback or null if not responded before the timeout</returns>
        public static async Task<MqttPublishResponseDto> Publish(string setTopic, MqttPublishRequestDto request, string stateTopic = null, int callbackTimeout = 5000, 
            MqttQosEnum qos = MqttQosEnum.AtLeastOnce, bool retainFlag = true)
        {
            var publishCallback = new TaskCompletionSource<MqttPublishResponseDto>();

            var messageBuilder = new MqttApplicationMessageBuilder()
                    .WithTopic(setTopic)
                    .WithPayload(request.ToString());

            if (retainFlag) messageBuilder.WithRetainFlag();

            if (qos == MqttQosEnum.AtMostOnce) messageBuilder.WithAtMostOnceQoS();
            else if (qos == MqttQosEnum.AtLeastOnce) messageBuilder.WithAtLeastOnceQoS();
            else if (qos == MqttQosEnum.AtMostOnce) messageBuilder.WithAtMostOnceQoS();

            var message = messageBuilder.Build();

            Task publishMessage = new Task(async () => {
                var x = await MqttServer.Server.PublishAsync(message);

                Thread.Sleep(callbackTimeout);

                if (!publishCallback.Task.IsCompleted)
                    publishCallback.SetResult(null);
            });

            void intercept(object sender, MqttApplicationMessage applicationMessage)
            {
                if (stateTopic != null && stateTopic == applicationMessage.Topic) {
                    var payload = new MqttPublishResponseDto(applicationMessage.Payload);

                    if (payload.MessageId == request.MessageId && !publishCallback.Task.IsCompleted)
                        publishCallback.SetResult(payload);
                }
            }

            publishMessage.Start();
            MqttServer.OnMessageIntercept += intercept;
            var response = await publishCallback.Task;
            MqttServer.OnMessageIntercept -= intercept;

            return response;
        }

        /// <summary>
        /// Sends a blind request to the device without verifying that the device responds 
        /// </summary>
        public static void Publish(string topic, string payload)
        {
            var message = new MqttApplicationMessageBuilder()
                .WithTopic(topic)
                .WithPayload(payload)
                .WithExactlyOnceQoS()
                .WithRetainFlag()
                .Build();

            MqttServer.Server.PublishAsync(message);
        }
    }
}
