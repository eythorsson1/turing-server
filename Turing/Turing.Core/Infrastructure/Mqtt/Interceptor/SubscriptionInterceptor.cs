﻿using MQTTnet.Server;

namespace Turing.Core.Infrastructure.Mqtt.Interceptor
{
    internal static class SubscriptionInterceptor
    {
        public static void HandleIntersection(MqttSubscriptionInterceptorContext subscription)
        {

            subscription.AcceptSubscription = true;
        }
    }
}
