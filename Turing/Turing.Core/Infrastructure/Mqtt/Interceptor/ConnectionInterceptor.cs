﻿using MQTTnet.Protocol;
using MQTTnet.Server;
using Turing.Core.Request.Device;
using Turing.Core.Request.User;
using Turing.Domain.Enums.Device;
using Turing.Domain.Infrastructure;

namespace Turing.Core.Infrastructure.Mqtt.Interceptor
{
    internal static class ConnectionInterceptor
    {
        public static void ConnectionValidator(MqttConnectionValidatorContext Client)
        {
            var services = TuringAppContext.Current.Services;
            var userService = services.UserService;
            var deviceService = services.DeviceService;
            var deviceLogService = services.DeviceLogService;

            var device = deviceService.FirstOrDefault(new DeviceRequest { AuthName = Client.Username });
            var user = userService.FirstOrDefault(new UserRequest { Username = Client.Username });

            if (StringHasher.VerifyHash(Client.Password, device?.AuthPasswordHash)) {
                Client.ReasonCode = MqttConnectReasonCode.Success;

                deviceLogService.Insert(device.DeviceId, DeviceLogActionEnum.DeviceConnected,
                    "Device successfully connected to the server", "", device.ClientId);
            }
            else if (StringHasher.VerifyHash(Client.Password, user?.PasswordHash)) {
                Client.ReasonCode = MqttConnectReasonCode.Success;

                //mqttLogService.Insert(DeviceLogActionEnum.DeviceConnected,
                //    "User successfully connected to the server", user.UserId, device.ClientId);
            }
            else
                Client.ReasonCode = MqttConnectReasonCode.BadUserNameOrPassword;
        }
    }
}

