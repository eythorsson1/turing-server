﻿using MQTTnet.Server;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Turing.Core.Request.Automation.Item;
using Turing.Core.Request.Device;
using Turing.Core.Request.User;
using Turing.Domain.Domain.Models.Dimension;
using Turing.Domain.Enums.Automation.AutomationItemType;

namespace Turing.Core.Infrastructure.Mqtt.Interceptor
{
    public static class MessageInterceptor
    {
        //internal static void HandleIntersection(MqttApplicationMessageInterceptorContext msg)
        //{
        //    var services = TuringAppContext.Current?.Services;
        //    if (services != null) {
        //        var deviceService = services.DeviceService;
        //        var dataValueService = services.DataValueService;
        //        var dataPresetService = services.DataPresetService;
        //        var automationService = services.AutomationService;
        //        var automationTriggerService = services.AutomationTriggerService;

        //        throw new NotFiniteNumberException(); // includeDeviceData has been changed from DataPresetData to DeviceTypeFeatureData
        //        var device = deviceService.FirstOrDefault(
        //            new DeviceRequest { StateTopic = msg.ApplicationMessage.Topic },
        //            includeDeviceData: true,
        //            includeDeviceDataPreset: true,
        //            includeDeviceFeatures: true
        //        );

        //        if (device != null) {
        //            string payloadStr = Encoding.Default.GetString(msg.ApplicationMessage.Payload);
        //            object payload = JsonConvert.DeserializeObject(payloadStr);

        //            if (device.DataValueId > 0) {

        //                var dataItemDict = DataValueModel.GetItems(payload).ToDictionary(x => x.Path);
        //                var dataPreset = dataPresetService.GetById(device.DataPresetId);
        //                var featureDict = device.Features.ToDictionary(x => x.DeviceTypeFeatureId);

        //                //// Dictionary< PresetPath, DataPath > -> 3.Red => Color.R
        //                //var presetDataPathDict = dataPreset.Items
        //                //   .Where(x => PresetHelper.PresetRegex.IsMatch(x.Placeholder))
        //                //   .ToDictionary(x => string.Join(".", PresetHelper.PresetRegex.Match(x.Placeholder).Groups[1].Value.Split(".").Skip(2)), x => x.Path);

        //                //// Dictionary< DataPath, PresetPath > -> Color.3 => 3.Red
        //                //var dataPresetPathDict = dataPreset.Items
        //                //    .Where(x => PresetHelper.PresetRegex.IsMatch(x.Placeholder))
        //                //    .ToDictionary(x => x.Path, x => string.Join(".", PresetHelper.PresetRegex.Match(x.Placeholder).Groups[1].Value.Split(".").Skip(2)));

        //                foreach (var presetItem in dataPreset.Items) {

        //                    // If one of the device properties has not been not updated can we ignore it.
        //                    if (!dataItemDict.TryGetValue(presetItem.Path, out var dataItem))
        //                        continue;

        //                    // Make sure the preset is valid ? 
        //                    if (!PresetHelper.PresetRegex.IsMatch(presetItem.Placeholder))
        //                        throw new NotFiniteNumberException();

        //                    DeviceTypeFeatureModel feature = null;
        //                    var match = PresetHelper.PresetRegex.Match(presetItem.Placeholder);
        //                    var parts = match.Groups[1].Value.Split(".");

        //                    if (parts[0] != "Device") throw new NotImplementedException();
        //                    else if (parts[1] != device.DeviceId.ToString()) throw new NotImplementedException();
        //                    else if (!long.TryParse(parts[2], out long deviceFeatureId))
        //                        throw new ArgumentException($"The third item ({parts[2]}) on the devices data path (DataPath: {presetItem.Path}, PresetValue: {presetItem.Placeholder}) is invalid; it must be the devices DeviceFeatureId");
        //                    else if (!featureDict.TryGetValue(deviceFeatureId, out feature))
        //                        throw new ArgumentException($"The DeviceFeatureId ({parts[2]}) on the devices data path (DataPath: {presetItem.Path}, PresetValue: {presetItem.Placeholder}) is not valid; DeviceFeatureId not found for the device");

        //                    switch (feature.Type) {
        //                        case DeviceTypeFeatureEnum.PowerState: {
        //                            if (bool.TryParse(dataItem.Value, out bool value)) device.Data.TrySetPath(dataItem.Path, value.ToString());
        //                            else throw new ArgumentException($"The value {dataItem.Value} (Path: {dataItem.Path}) is not a valid option for the PowerState Feature");
        //                            break;
        //                        }

        //                        case DeviceTypeFeatureEnum.ColorPicker: {
        //                            var dataColor = new DeviceTypeFeature_ColorPicker();

        //                            // Get All the color values saved for the device
        //                            var colorData = device.Data.Items
        //                                .Where(data => device.DataPreset.Items
        //                                    .Where(preset => long.TryParse(preset.Placeholder.Split(".")[2], out long featureId) && featureId == feature.DeviceTypeFeatureId)
        //                                    .Any(preset => preset.Path == data.Path)
        //                                );

        //                            foreach (var data in colorData) {
        //                                string propName = dataPreset.DataPresetPathDict[data.Path].Split(".").Last();
        //                                dataColor.TrySet(propName, data.Value);
        //                            }

        //                            dataColor.TrySet(parts[3], dataItem.Value);

        //                            // Set the device data
        //                            foreach (var data in dataColor.ToDictionary()) {
        //                                if (dataPreset.PresetDataPathDict.TryGetValue($"{feature.DeviceTypeFeatureId}.{data.Key}", out string path))
        //                                    device.Data.TrySetPath(path, data.Value);
        //                            }

        //                            break;
        //                        }

        //                        default: throw new NotFiniteNumberException();
        //                    }
        //                }

        //                // Save the device data
        //                dataValueService.Update(device.Data);
        //            }
        //            else {
        //                throw new NotImplementedException();

        //                //var deviceData = new DataValueModel(device.DataPresetId, payloadStr, device.ClientId);
        //                //dataValueService.Insert(deviceData);

        //                //device.DataValueId = deviceData.DataValueId;
        //                //deviceService.Update(device);
        //            }

        //            var triggers = automationTriggerService.GetList(new AutomationTriggerRequest {
        //                Type = AutomationTriggerEnum.DeviceState,
        //                DataStr = device.DeviceId.ToString()
        //            });

        //            if (triggers != null && triggers.Any()) {
        //                foreach (var trigger in triggers) {

        //                    automationService.TriggerAutomation(trigger.AutomationItemId, payload);
        //                }
        //            }
        //        }
        //    }
        //}

        internal static void HandleIntersection(MqttApplicationMessageInterceptorContext msg)
        {
            var services = TuringAppContext.Current?.Services;
            if (services != null) {
                var userService = services.UserService;
                var deviceService = services.DeviceService;
                var automationService = services.AutomationService;
                var automationTriggerService = services.AutomationTriggerService;

                try {

                    string payloadStr = Encoding.Default.GetString(msg.ApplicationMessage.Payload);


                    if (msg.ApplicationMessage.Topic == "Device/Setup") {
                        var user = userService.FirstOrDefault(new UserRequest { Username = msg.ClientId });
                        var x = JsonConvert.DeserializeObject<List<DimensionRegisterModel>>("[ { Code: 1, Description: \"Solid Color\", Type: 1 }, { Code: 2, Description: \"Running Rabit\", Type: 1 } ]");
                        var request = JsonConvert.DeserializeObject<DeviceSetupRequest>(payloadStr);
                        request.CreatedByUserId = user.UserId;
                        request.ClientId = user.CurrentClientId;

                        deviceService.SetupDevice(request);
                  
                    }
                    else if (msg.ApplicationMessage.Topic == "Device/Setup/Success" || msg.ApplicationMessage.Topic == "Device/Setup/Complete") { 


                    }
                    else {
                        var device = deviceService.FirstOrDefault(
                            new DeviceRequest { StateTopic = msg.ApplicationMessage.Topic }
                        );

                        if (device != null) {
                            object payload = JsonConvert.DeserializeObject(payloadStr);


                            var triggers = automationTriggerService.GetList(new AutomationTriggerRequest {
                                Type = AutomationTriggerEnum.DeviceState,
                                DataStr = device.DeviceId.ToString()
                            });

                            if (triggers != null && triggers.Any()) {
                                foreach (var trigger in triggers) {

                                    automationService.TriggerAutomation(trigger.AutomationItemId, payload);
                                }
                            }
                        }
                    }

                }
                catch (Exception ex) {
                    throw ex;
                }
            }
        }
    }
}
