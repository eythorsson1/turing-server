﻿using Turing.Core.Request;
using System;
using System.Collections.Generic;
using System.Text;
using Turing.Domain.Models;

namespace Turing.Domain.Infrastructure
{
    public class PagedList<T> //: IPagedList<T>
        where T : IModel
    {
        public List<T> Items { get; set; }
        //public IEnumerable<T> IPagedList<T>._items { get { return Items } }

        public long CurrentPage { get; }
        public long TotalNumberOfItems { get; }
        public long ItemsPerPage { get; }
        public long TotalNumberOfPages => (long)Math.Ceiling((decimal)TotalNumberOfItems / ItemsPerPage);

        public PagedList(List<T> items, long count, BasePagedRequest request)
        {
            this.Items = items;
            this.TotalNumberOfItems = count;
            this.CurrentPage = request.CurrentPage;
            this.ItemsPerPage = request.ItemsPerPage;
        }
    }

    //public interface IPagedList<out T>
    //{
    //    //IEnumerable<T> _items { get; }
        
    //    long CurrentPage { get; }
        
    //    long TotalNumberOfItems { get; }
        
    //    long ItemsPerPage { get; }
        
    //    long TotalNumberOfPages => (long)Math.Ceiling((decimal)TotalNumberOfItems / ItemsPerPage);
    //}
}
