﻿using Microsoft.AspNetCore.Http;
using System;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Turing.Domain.Exceptions;

namespace Turing.Core.Infrastructure.Filters
{
    public class HandleExceptions
    {
        private readonly RequestDelegate _next;
        public HandleExceptions(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception error)
            {
                var response = context.Response;
                response.ContentType = "application/json";

                string errorMessage = "";

                switch (error)
                {
                    case FeedbackException e:
                        response.StatusCode = (int)HttpStatusCode.BadRequest;
                        errorMessage = error.Message;
                        break;

                    case UnauthorizedAccessException e:
                        response.StatusCode = (int)HttpStatusCode.Unauthorized;
                        break;

                    default:
                        response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        break;
                }
                
                #if DEBUG
                    errorMessage = error.Message;
                #endif

                var result = JsonSerializer.Serialize(new { message = errorMessage });
                await response.WriteAsync(result);
            }
        }
    }
}
