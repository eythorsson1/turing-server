﻿using Flee.PublicTypes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Turing.Core.Infrastructure.Mqtt;
using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository;
using Turing.Core.Request.Automation;
using Turing.Core.Request.Automation.Item;
using Turing.Core.Request.Device;
using Turing.Core.Service;
using Turing.Core.Services.Automation.Item;
using Turing.Core.Services.Data;
using Turing.Core.Services.Device;
using Turing.Domain.Aggregated.Automation;
using Turing.Domain.Domain.Aggregated.Automation;
using Turing.Domain.Domain.Enums.Device;
using Turing.Domain.Domain.Models.Data.Preset;
using Turing.Domain.Enums.Automation;
using Turing.Domain.Enums.Automation.AutomationItemType;
using Turing.Domain.Infrastructure;
using Turing.Domain.Infrastructure.Extensions;
using Turing.Domain.Infrastructure.Helper;
using Turing.Domain.Models.Automation;
using Turing.Domain.Models.Automation.Item;
using Turing.Domain.Models.Device;

namespace Turing.Core.Services.Automation
{
    public class AutomationService : BaseService
    {
        private readonly AutomationItemLinkService AutomationItemLinkService;

        private readonly AutomationTriggerService AutomationTriggerService;
        private readonly AutomationConditionService AutomationConditionService;
        private readonly AutomationActionService AutomationActionService;

        private readonly DeviceService DeviceService;

        private readonly DataPresetService DataPresetService;

        public AutomationService(IUnitOfWorkProvider uowProvider, RepoFactory repoFactory,
            AutomationItemLinkService automationItemLinkService, AutomationTriggerService automationTriggerService,
            AutomationConditionService automationConditionService, AutomationActionService automationActionService,
            DeviceService deviceService, DataPresetService dataPresetService) : base(uowProvider, repoFactory)
        {
            this.AutomationItemLinkService = automationItemLinkService;

            this.AutomationTriggerService = automationTriggerService;
            this.AutomationConditionService = automationConditionService;
            this.AutomationActionService = automationActionService;

            this.DeviceService = deviceService;

            this.DataPresetService = dataPresetService;
        }

        public PagedList<AutomationModel> GetPagedList(AutomationRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.AutomationRepository(uow);
                return repo.GetPagedList(request);
            }
        }
        public List<AutomationModel> GetList(AutomationRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.AutomationRepository(uow);
                return repo.GetList(request);
            }
        }
        public AutomationModel FirstOrDefault(long automationId)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.AutomationRepository(uow);
                return repo.GetById(automationId);
            }
        }
        public AutomationModel GetById(long automationId)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.AutomationRepository(uow);
                return repo.GetById(automationId);
            }
        }

        public void Update(AutomationModel model)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                Update(uow, model);
            }
        }
        internal void Update(IUnitOfWork uow, AutomationModel model)
        {
            var repo = RepoFactory.AutomationRepository(uow);
            if (!repo.Update(model))
                throw new ArgumentException("Failed to update the model");
        }

        public long Insert(AutomationModel model, List<(long Index, long ParentIndex, AutomationTypeEnum AutomationType, int Type, object Data)> items)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork(useTransaction: true)) {
                long automationId = Insert(uow, model);

                // Map the items.Index value to the AutomationItemId
                var indexIdDict = new Dictionary<long, long>();

                // Insert the automation items
                foreach (var item in items.OrderBy(x => x.Index)) {
                    long itemId;

                    if (item.AutomationType == AutomationTypeEnum.Trigger) {
                        if (!Enum.GetValues(typeof(AutomationTriggerEnum)).Cast<AutomationTriggerEnum>().Any(x => (int)x == item.Type))
                            throw new ArgumentException("The automation trigger does contain a definition of the provided type");

                        var itemModel = new AutomationTriggerModel(automationId, (AutomationTriggerEnum)item.Type, model.ClientId, item.Data);
                        itemId = AutomationTriggerService.Insert(uow, itemModel);
                    }
                    else if (item.AutomationType == AutomationTypeEnum.Condition) {
                        if (!Enum.GetValues(typeof(AutomationConditionEnum)).Cast<AutomationConditionEnum>().Any(x => (int)x == item.Type))
                            throw new ArgumentException("The automation condition does contain a definition of the provided type");

                        var itemModel = new AutomationConditionModel(automationId, (AutomationConditionEnum)item.Type, model.ClientId, item.Data);
                        itemId = AutomationConditionService.Insert(uow, itemModel);
                    }
                    else if (item.AutomationType == AutomationTypeEnum.Action) {
                        if (!Enum.GetValues(typeof(AutomationActionEnum)).Cast<AutomationActionEnum>().Any(x => (int)x == item.Type))
                            throw new ArgumentException("The automation action does contain a definition of the provided type");

                        var itemModel = new AutomationActionModel(automationId, (AutomationActionEnum)item.Type, model.ClientId, item.Data);
                        itemId = AutomationActionService.Insert(uow, itemModel);
                    }
                    else
                        throw new ArgumentException("Invalid AutomationType");

                    indexIdDict.Add(item.Index, itemId);
                }

                // create the links
                foreach (var item in items.Where(x => indexIdDict[x.ParentIndex] != indexIdDict[x.Index])) {
                    var linkModel = new AutomationItemLinkModel(automationId, model.ClientId, indexIdDict[item.ParentIndex], indexIdDict[item.Index]);
                    AutomationItemLinkService.Insert(uow, linkModel);
                }

                uow.Commit();
                return automationId;
            }
        }
        internal long Insert(IUnitOfWork uow, AutomationModel model)
        {
            var repo = RepoFactory.AutomationRepository(uow);
            var automationId = repo.Insert(model);
            if (automationId < 1)
                throw new ArgumentException("Failed to insert the model");

            return automationId;
        }

        public void Delete(long automationId)
        {
            var model = GetById(automationId);
            var links = AutomationItemLinkService.GetList(
                new AutomationItemLinkRequest {
                    AutomationId = automationId,
                    ClientId = model.ClientId,
                });

            var itemTypeDict = links
                .SelectMany(link => new[] {
                    new { ItemId = link.ParentAutomationItemId, AutomationType = link.ParentAutomationType },
                    new { ItemId = link.ChildAutomationItemId, AutomationType = link.ChildAutomationType }
                })
                .Distinct()
                .Where(item => item.ItemId > 0)
                .GroupBy(x => x.AutomationType)
                .ToDictionary(x => x.Key, x => x.SelectList(y => y.ItemId));

            using (IUnitOfWork uow = UowProvider.GetUnitOfWork(useTransaction: true)) {
                // DELETE THE LINKS
                links.ForEach(link => AutomationItemLinkService.Delete(uow, link.AutomationItemLinkId));

                // DELETE ITEMS
                itemTypeDict[AutomationTypeEnum.Trigger].ForEach(id => AutomationTriggerService.Delete(uow, id));
                itemTypeDict[AutomationTypeEnum.Condition].ForEach(id => AutomationConditionService.Delete(uow, id));
                itemTypeDict[AutomationTypeEnum.Action].ForEach(id => AutomationActionService.Delete(uow, id));

                // DELETE MAIN 
                Delete(uow, automationId);
            }
        }

        internal void Delete(IUnitOfWork uow, long automationId)
        {
            var repo = RepoFactory.AutomationRepository(uow);
            if (!repo.Delete(automationId))
                throw new ArgumentException("Failed to delete the automation");
        }


        public void TriggerAutomation(long automationItemId, object data = null)
        {
            var links = AutomationItemLinkService.GetList(new AutomationItemLinkRequest {
                ParentAutomationItemId = automationItemId
            });

            foreach (var item in links) {
                switch (item.ChildAutomationType) {
                    case AutomationTypeEnum.Trigger:
                        TriggerAutomation(item.ChildAutomationItemId, data);
                        break;

                    case AutomationTypeEnum.Condition:
                        HandleCondition(item.ChildAutomationItemId, data);
                        break;

                    case AutomationTypeEnum.Action:
                        PerformAction(item.ChildAutomationItemId, data);
                        break;

                    default:
                        throw new NotImplementedException();
                };
            }
        }

        private void HandleCondition(long automationItemId, object data)
        {
            var condition = AutomationConditionService.GetById(automationItemId);
            //var conditionValue = FormatPresetData(condition.DataStr);
            bool isValid = false;


            switch (condition.Type) {
                case AutomationConditionEnum.ConditionString:
                    string conditionStr = DataPresetService.Parse(condition.DataStr);

                    ExpressionContext context = new ExpressionContext();
                    IGenericExpression<bool> e = context.CompileGeneric<bool>(conditionStr);
                    isValid = e.Evaluate();

                    break;

                //case AutomationConditionEnum.IsEqual:
                //    isValid = conditionValue.Equals(DateTime.Parse(condition.DataStr));
                //    break;

                //case AutomationConditionEnum.IsGreaterThan:
                //    if (conditionValue.GetType().IsAssignableFrom(typeof(float)))
                //        isValid = float.Parse(conditionValue as string) > float.Parse(data as string);
                //    else if (conditionValue is DateTime)
                //        isValid = conditionValue as DateTime? > DateTime.Now;
                //    else
                //        throw new NotImplementedException();
                //    break;

                //case AutomationConditionEnum.IsLessThan:
                //    if (conditionValue.GetType().IsAssignableFrom(typeof(float)))
                //        isValid = float.Parse(conditionValue as string) < float.Parse(data as string);
                //    else if (conditionValue is DateTime)
                //        isValid = conditionValue as DateTime? < DateTime.Now;
                //    else
                //        throw new NotImplementedException();
                //    break;

                default:
                    throw new ArgumentException("The provided condition type is not implemented");
            };

            if (isValid) {
                TriggerAutomation(automationItemId, data);
            }
        }

        private void PerformAction(long automationItemId, object data)
        {
            DataPresetModel dataPreset = null;
            var action = AutomationActionService.GetById(automationItemId);

            if (action.DataPresetId > 0) {
                dataPreset = DataPresetService.GetById(action.DataPresetId);
            }

            switch (action.Type) {
                case AutomationActionEnum.Device:
                    PerformDeviceAction(long.Parse(action.DataStr), dataPreset);
                    break;
                case AutomationActionEnum.Mqtt:
                    throw new NotImplementedException();
                    // PerformMqttAction(data as AutomationAction_Mqtt);
                    // break;

                default:
                    throw new NotImplementedException("Action type is not implemented");
            }
        }

        private async void PerformDeviceAction(long deviceId, DataPresetModel dataPreset)
        {
            var device = DeviceService.GetById(deviceId);

            AutomationTriggerEnum triggerType;
            IAutomationItemData data;

            try {
                var payload = DataPresetService.Parse(dataPreset.DataPresetStr);
                var response = await DeviceService.PublishPayloadToDevice(device, payload);

                if (response == null) triggerType = AutomationTriggerEnum.DeviceCallbackEmpty;
                //if (response.Success) triggerType = AutomationTriggerEnum.DeviceCallbackSuccess;
                //else triggerType = AutomationTriggerEnum.DeviceCallbackFailiour;
                else triggerType = AutomationTriggerEnum.DeviceCallbackSuccess;

                data = response?.Data as IAutomationItemData;
            }
            catch (TimeoutException) {
                triggerType = AutomationTriggerEnum.DeviceTimeout;
                data = null;
            }

            var triggers = AutomationTriggerService
                .GetList(new AutomationTriggerRequest {
                    Type = triggerType,
                    ClientId = device.ClientId
                });

            foreach (var trigger in triggers) {
                TriggerAutomation(trigger.AutomationItemId, data);
            }
        }

        private void PerformMqttAction(AutomationAction_Mqtt request)
        {
            string payload = JsonConvert.SerializeObject(request.Payload);
            MqttAction.Publish(request.Topic, payload);
        }

        //public object FormatPresetData(string presetDataStr)
        //{
        //    var presetTemplateRegex = new Regex(@"{{(.*?)}}");

        //    if (presetTemplateRegex.IsMatch(presetDataStr)) {
        //        string data;
        //        //if (presetDataStr.StartsWith("{{Device.")) return FormatDevicePresetData(presetDataStr);
        //        //if (presetDataStr.StartsWith("{{Device.")) return GetDataPresetValue(presetDataStr);

        //        //else throw new NotImplementedException();

        //        return GetDataPresetValue(presetDataStr);
        //    }
            
        //    else if (int.TryParse(presetDataStr, out var intVal)) return intVal;
        //    else if (long.TryParse(presetDataStr, out var longVal)) return longVal;
        //    else if (float.TryParse(presetDataStr, out var floatVal)) return floatVal;
        //    else if (DateTime.TryParse(presetDataStr, out var dateVal)) return dateVal;
        //    else return presetDataStr;
        //}

        //public object FormatDevicePresetData(string presetDataStr)
        //{
        //    var parts = presetDataStr.Split(".");
        //    var path = string.Join(".", parts.Skip(2));

        //    var device = DeviceService.GetById(
        //        long.Parse(parts[1]), 
        //        includeDeviceData: true, 
        //        includeDeviceFeatures: true
        //    );

        //    var deviceFeatureDict = device.Features.ToDictionary(x => x.DeviceTypeFeatureId);

        //    object format(string presetDataFeatureStr)
        //    {
        //        var parts = presetDataFeatureStr.Split(".");
        //        if (long.TryParse(parts[2], out long deviceFeatureId)) {
        //            var deviceFeature = deviceFeatureDict[deviceFeatureId];

        //            if (device.Data.TryGetPath(presetDataStr, out string value)) {
        //                if (parts.Length > 3) return FormatPresetData(value);
        //                else {
        //                    throw new NotFiniteNumberException();
        //                    //if(deviceFeature.Type == DeviceTypeFeatureEnum.ColorPicker) return Color 
        //                }
        //            }
        //        }

        //        return null;
        //    }

        //    if (device.Data.Items.SelectList(item => item.Path).Contains(path)) {
        //        // The presetDataStr is in the path format: Device.< DeviceId >.< DataPath >

        //        DataPresetModel dataPreset = device.DataPresetId > 0
        //            ? DataPresetService.GetById(device.DataPresetId)
        //            : null;

        //        var preset = dataPreset?.Items.FirstOrDefault(x => x.Path == path);

        //        if (preset.HasValue && long.TryParse(preset.Value.Placeholder.Split(".")[2], out long deviceFeatureId))
        //            return format(preset.Value.Placeholder);
        //    }
        //    else
        //        return format(presetDataStr);

        //    return null;
        //}

        //public object GetDataPresetValue(string presetDataStr)
        //{
        //    var parts = presetDataStr.Replace("{{", "").Replace("}}", "").Split(".");
        //    string value;

        //    if (parts[0] == "Device") {
        //        DeviceModel device = DeviceService.GetById(long.Parse(parts[1]), includeDeviceData: true);
        //        DataPresetModel dataPreset = device.DataPresetId > 0 ? DataPresetService.GetById(device.DataPresetId) : null;

        //        value = GetDeviceDataPresetValue(presetDataStr, device, dataPreset);
        //    }
        //    else
        //        return presetDataStr;

        //    return FormatPresetData(value);
        //}

        //public string GetDeviceDataPresetValue(string presetDataStr, DeviceModel device, DataPresetModel dataPreset = null)
        //{
        //    // Supported presetDataStr formats:
        //    // Device.< DeviceId >.< DataPath > 
        //    // Device.< DeviceId >.< DeviceFeatureId  > 

        //    var parts = presetDataStr.Replace("{{", "").Replace("}}", "").Split(".");
        //    string value = null;

        //    if (!device.Data.TryGetPath(string.Join(".", parts.Skip(2)), out value) &&
        //        device.DataPresetId > 0) {

        //        if (dataPreset == null) dataPreset = DataPresetService.GetById(device.DataPresetId);

        //        var presetItem = dataPreset.Items
        //            .FirstOrDefault(item => item.Placeholder == string.Join(".", parts));

        //        device.Data.TryGetPath(presetItem.Path, out value);
        //    }

        //    return value;
        //}
    }
}
