﻿using System;
using System.Collections.Generic;
using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository;
using Turing.Core.Request.Automation.Item;
using Turing.Core.Service;
using Turing.Domain.Infrastructure;
using Turing.Domain.Models.Automation.Item;

namespace Turing.Core.Services.Automation.Item
{
    abstract public class BaseAutomationItemService<AutomationItemTypeEnum, TRequest, TModel> : BaseService
        where AutomationItemTypeEnum : Enum
        where TRequest : BaseAutomationItemRequest<AutomationItemTypeEnum>, new()
        where TModel : BaseAutomationItemModel<AutomationItemTypeEnum>
    {

        protected readonly AutomationItemLinkService AutomationItemLinkService;
        public BaseAutomationItemService(IUnitOfWorkProvider uowProvider, RepoFactory repoFactory, AutomationItemLinkService automationItemLinkService) : base(uowProvider, repoFactory)
        {
            AutomationItemLinkService = automationItemLinkService;
        }

        public PagedList<TModel> GetPagedList(TRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.AutomationItemRepository(uow);
                return repo.GetPagedList<AutomationItemTypeEnum, TModel>(request);
            }
        }

        public List<TModel> GetList(TRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.AutomationItemRepository(uow);
                return repo.GetList<AutomationItemTypeEnum, TModel>(request);
            }
        }

        public BaseAutomationItemModel<AutomationItemTypeEnum> FirstOrDefault(TRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.AutomationItemRepository(uow);
                return repo.FirstOrDefault<AutomationItemTypeEnum, TModel>(request);
            }
        }

        public BaseAutomationItemModel<AutomationItemTypeEnum> GetById(long automationItemId)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var request = new TRequest { AutomationItemId = automationItemId };
                var repo = RepoFactory.AutomationItemRepository(uow);
                return repo.FirstOrDefault<AutomationItemTypeEnum, TModel>(request);
            }
        }

        internal void Update(IUnitOfWork uow, TModel model)
        {
            Validate(model);

            var repo = RepoFactory.AutomationItemRepository(uow);
            if (!repo.Update<AutomationItemTypeEnum, TModel>(model))
                throw new ArgumentException("Failed to update the model");
        }

        internal long Insert(IUnitOfWork uow, TModel model)
        {
            Validate(model);

            var repo = RepoFactory.AutomationItemRepository(uow);
            var automationId = repo.Insert<AutomationItemTypeEnum, TModel>(model);
            if (automationId < 1)
                throw new ArgumentException("Failed to insert the model");

            return automationId;
        }

        internal bool Delete(IUnitOfWork uow, long automationItemId)
        {
            var repo = RepoFactory.AutomationItemRepository(uow);
            var automationType = new TRequest().AutomationType;
            return repo.Delete(automationItemId, automationType);
        }

        protected virtual void Validate(TModel model)
        {
            if (model.ClientId < 1) throw new ArgumentException("ClientId is not set");
            if (model.AutomationId < 1) throw new ArgumentException("AutomationId is not set");
        }
    }
}
