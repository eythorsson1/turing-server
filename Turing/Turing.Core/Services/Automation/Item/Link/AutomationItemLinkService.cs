﻿using System;
using System.Collections.Generic;
using System.Text;
using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository;
using Turing.Core.Request.Automation.Item;
using Turing.Core.Service;
using Turing.Domain.Models.Automation.Item;
using Turing.Domain.Infrastructure;

namespace Turing.Core.Services.Automation.Item
{
    public class AutomationItemLinkService : BaseService
    {
        public AutomationItemLinkService(IUnitOfWorkProvider uowProvider, RepoFactory repoFactory) : base(uowProvider, repoFactory)
        {
        }

        public List<AutomationItemLinkModel> GetList(AutomationItemLinkRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.AutomationItemLinkRepository(uow);
                return repo.GetList(request);
            }
        }
        public AutomationItemLinkModel FirstOrDefault(long automationItemLinkId)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.AutomationItemLinkRepository(uow);
                return repo.GetById(automationItemLinkId);
            }
        }
        public AutomationItemLinkModel GetById(long automationItemLinkId)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.AutomationItemLinkRepository(uow);
                return repo.GetById(automationItemLinkId);
            }
        }

        public long Insert(AutomationItemLinkModel model)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                return Insert(uow, model);
            }
        }
        public long Insert(IUnitOfWork uow, AutomationItemLinkModel model)
        {
            var repo = RepoFactory.AutomationItemLinkRepository(uow);
            var automationItemLinkId = repo.Insert(model);
            if (automationItemLinkId < 1)
                throw new ArgumentException("Failed to insert the model");

            return automationItemLinkId;
        }

        public bool Delete(long automationItemLinkId)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                return Delete(uow, automationItemLinkId);
            }
        }
        public bool Delete(IUnitOfWork uow, long automationItemLinkId)
        {
            var repo = RepoFactory.AutomationItemLinkRepository(uow);
            return repo.Delete(automationItemLinkId);
        }
    }
}
