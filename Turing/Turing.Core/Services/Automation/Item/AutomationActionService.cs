﻿using System;
using System.Collections.Generic;
using System.Linq;
using Turing.Core.Infrastructure.Mqtt;
using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository;
using Turing.Core.Request.Automation.Item;
using Turing.Domain.Aggregated.Automation;
using Turing.Domain.Enums.Automation.AutomationItemType;
using Turing.Domain.Exceptions;
using Turing.Domain.Infrastructure.Helper;
using Turing.Domain.Models.Automation.Item;

namespace Turing.Core.Services.Automation.Item
{
    public class AutomationActionService : BaseAutomationItemService<AutomationActionEnum, AutomationActionRequest, AutomationActionModel>
    {

        public AutomationActionService(IUnitOfWorkProvider uowProvider, RepoFactory repoFactory, AutomationItemLinkService automationItemLinkService)
            : base(uowProvider, repoFactory, automationItemLinkService)
        {
        }

        protected override void Validate(AutomationActionModel model)
        {
            base.Validate(model);

            if (model.Type == AutomationActionEnum.Device) {
                var data = model.GetData<AutomationAction_Device>();
                if (data.DeviceId < 1) throw new FeedbackException("You must specify a device for the selected action");
                if (data.DataPreset == null) throw new FeedbackException("The device action requires a payload");
            }
            else if (model.Type == AutomationActionEnum.Mqtt) {
                var data = model.GetData<AutomationAction_Mqtt>();
                
                if (data.Payload == null) throw new FeedbackException("The device action requires a payload");
                MqttServer.ValidateTopic(data.Topic);
            }

        }
    }
}
