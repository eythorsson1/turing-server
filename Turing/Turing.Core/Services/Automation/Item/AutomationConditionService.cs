﻿using System;
using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository;
using Turing.Core.Request.Automation.Item;
using Turing.Domain.Domain.Aggregated.Automation;
using Turing.Domain.Enums.Automation.AutomationItemType;
using Turing.Domain.Exceptions;
using Turing.Domain.Models.Automation.Item;

namespace Turing.Core.Services.Automation.Item
{
    public class AutomationConditionService : BaseAutomationItemService<AutomationConditionEnum, AutomationConditionRequest, AutomationConditionModel>
    {
        public AutomationConditionService(IUnitOfWorkProvider uowProvider, RepoFactory repoFactory, AutomationItemLinkService automationItemLinkService)
            : base(uowProvider, repoFactory, automationItemLinkService)
        { }

        protected override void Validate(AutomationConditionModel model)
        {
            throw new NotImplementedException();

            //base.Validate(model);

            //if (model.Type == AutomationConditionEnum.IsEqual) {
            //    var data = model.GetData<AutomationCondition_IsEqual>();
            //    if (data == null || data.Value == null) throw new FeedbackException("The IsEqual condition requires data to compare it to");
            //}
            //else {
            //    throw new NotImplementedException();
            //}
        }
    }
}
