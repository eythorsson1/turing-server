﻿using NCrontab;
using System;
using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository;
using Turing.Core.Request.Automation.Item;
using Turing.Domain.Aggregated.Automation;
using Turing.Domain.Enums.Automation.AutomationItemType;
using Turing.Domain.Exceptions;
using Turing.Domain.Models.Automation;

namespace Turing.Core.Services.Automation.Item
{
    public class AutomationTriggerService : BaseAutomationItemService<AutomationTriggerEnum, AutomationTriggerRequest, AutomationTriggerModel>
    {
        public AutomationTriggerService(IUnitOfWorkProvider uowProvider, RepoFactory repoFactory,
            AutomationItemLinkService automationItemLinkService)
            : base(uowProvider, repoFactory, automationItemLinkService)
        {
        }

        protected override void Validate(AutomationTriggerModel model)
        {
            base.Validate(model);

            if (model.Type == AutomationTriggerEnum.Schedule) {
                var data = model.GetData<AutomationTrigger_Schedule>();
                if (CrontabSchedule.TryParse(data.CronExpression) == null) throw new ArgumentException("The provided CronExpression was invalid");
            }
            else if (
                model.Type == AutomationTriggerEnum.DeviceState || model.Type == AutomationTriggerEnum.DeviceTimeout || 
                model.Type == AutomationTriggerEnum.DeviceCallbackSuccess || model.Type == AutomationTriggerEnum.DeviceCallbackFailiour ||
                model.Type == AutomationTriggerEnum.DeviceCallbackEmpty) {

                var data = model.GetData<AutomationTrigger_Device>() ;
                if (data.DeviceId < 1) throw new FeedbackException("The device trigger must include the device that should trigger it");
            }
            else {
                throw new NotImplementedException();
            }
        }
    }
}
