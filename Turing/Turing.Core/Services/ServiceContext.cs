﻿using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository;
using Turing.Core.Services.Automation;
using Turing.Core.Services.Automation.Item;
using Turing.Core.Services.Client;
using Turing.Core.Services.ClientUserLink.UserLink;
using Turing.Core.Services.Data;
using Turing.Core.Services.Data.Value;
using Turing.Core.Services.Device;
using Turing.Core.Services.Device.Feature;
using Turing.Core.Services.Device.Log;
using Turing.Core.Services.Device.Type.Feature.Data;
using Turing.Core.Services.Dimension;
using Turing.Core.Services.User;
using Turing.Core.Services.User.Preference;

namespace Turing.Core.Service
{
    public class ServiceContext
    {
        public UserService UserService { get; internal set; }
        public UserTablePreferenceService UserTablePreferenceService { get; internal set; }
        public ClientService ClientService { get; internal set; }
        public ClientUserLinkService ClientUserLinkService { get; internal set; }


        // Dimension
        public DimensionRegisterLinkService DimensionRegisterLinkService { get; internal set; }
        public DimensionRegisterService DimensionRegisterService { get; internal set; }
        public DimensionService DimensionService { get; internal set; }


        // Device
        public DeviceLogService DeviceLogService { get; internal set; }
        public DeviceFeatureService DeviceFeatureService { get; internal set; }
        public DeviceFeatureDataService DeviceFeatureDataService { get; internal set; }
        public DeviceService DeviceService { get; internal set; }

        // Data
        public DataPresetService DataPresetService { get; internal set; }
        public DataValueService DataValueService { get; internal set; }

        // Automation
        public AutomationItemLinkService AutomationItemLinkService { get; internal set; }
        public AutomationTriggerService AutomationTriggerService { get; internal set; }
        public AutomationConditionService AutomationConditionService { get; internal set; }
        public AutomationActionService AutomationActionService { get; internal set; }
        public AutomationService AutomationService { get; internal set; }

        public ServiceContext(string sqlConnectionString)
        {
            UnitOfWorkProvider.SqlConnectionString = sqlConnectionString;
            UnitOfWorkProvider uowProvider = new UnitOfWorkProvider();
            RepoFactory repoFactory = new RepoFactory();


            this.UserService = new UserService(uowProvider, repoFactory);
            this.UserTablePreferenceService = new UserTablePreferenceService(uowProvider, repoFactory);
            this.ClientUserLinkService = new ClientUserLinkService(uowProvider, repoFactory);
            this.ClientService = new ClientService(uowProvider, repoFactory, ClientUserLinkService, UserService);

            this.DimensionRegisterLinkService = new DimensionRegisterLinkService(uowProvider, repoFactory);
            this.DimensionRegisterService = new DimensionRegisterService(uowProvider, repoFactory);
            this.DimensionService = new DimensionService(uowProvider, repoFactory, DimensionRegisterLinkService, DimensionRegisterService);


            this.DataPresetService = new DataPresetService(uowProvider, repoFactory);
            this.DataValueService = new DataValueService(uowProvider, repoFactory, DataPresetService); // Not in use


            this.DeviceLogService = new DeviceLogService(uowProvider, repoFactory);
            this.DeviceFeatureService = new DeviceFeatureService(uowProvider, repoFactory);
            this.DeviceFeatureDataService = new DeviceFeatureDataService(uowProvider, repoFactory, DeviceFeatureService, DimensionService);
            this.DeviceService = new DeviceService(uowProvider, repoFactory, DeviceFeatureDataService, DeviceFeatureService, DeviceLogService, DataPresetService, DimensionService, UserService);


            this.AutomationItemLinkService = new AutomationItemLinkService(uowProvider, repoFactory);
            this.AutomationTriggerService = new AutomationTriggerService(uowProvider, repoFactory, AutomationItemLinkService);
            this.AutomationConditionService = new AutomationConditionService(uowProvider, repoFactory, AutomationItemLinkService);
            this.AutomationActionService = new AutomationActionService(uowProvider, repoFactory, AutomationItemLinkService);
            this.AutomationService = new AutomationService(uowProvider, repoFactory, AutomationItemLinkService, AutomationTriggerService, AutomationConditionService, AutomationActionService, DeviceService, DataPresetService);
        }
    }
}
