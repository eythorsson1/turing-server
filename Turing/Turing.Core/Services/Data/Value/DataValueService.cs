﻿using System;
using System.Collections.Generic;
using System.Text;
using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository;
using Turing.Core.Request.Data.Value;
using Turing.Core.Service;
using Turing.Domain.Domain.Models.Data.Value;

namespace Turing.Core.Services.Data.Value
{

    public class DataValueService : BaseService
    {
        private readonly DataPresetService DataPresetService;

        public DataValueService(IUnitOfWorkProvider uowProvider, RepoFactory repoFactory, 
            DataPresetService dataPresetService) : base(uowProvider, repoFactory)
        {
            DataPresetService = dataPresetService;
        }

        public List<DataValueModel> GetList(DataValueRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DataValueRepository(uow);
                return repo.GetList(request);
            }
        }

        public DataValueModel FirstOrDefault(DataValueRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DataValueRepository(uow);
                return repo.FirstOrDefault(request);
            }
        }

        public DataValueModel GetById(long dataValueId)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DataValueRepository(uow);
                return repo.GetById(dataValueId);
            }
        }

        public void Update(DataValueModel model)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                Update(uow, model);
            }
        }
        internal void Update(IUnitOfWork uow, DataValueModel model)
        {
            Validate(model);

            var repo = RepoFactory.DataValueRepository(uow);
            if (!repo.Update(model)) throw new ArgumentException("Failed to insert the User model");
        }
        public long Insert(DataValueModel model)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                return Insert(uow, model);
            }
        }

        internal long Insert(IUnitOfWork uow, DataValueModel model)
        {
            Validate(model);

            var repo = RepoFactory.DataValueRepository(uow);
            var dataValueId = repo.Insert(model);
            if (dataValueId < 1) throw new ArgumentException("Failed to insert the User model");
            model.GetType().GetProperty(nameof(model.DataValueId)).SetValue(model, dataValueId);
            return dataValueId;
        }

        private void Validate(DataValueModel model)
        {
            if (model.ClientId < 0) throw new ArgumentException("ClientId not set");
            //if (model.DataPresetId < 0) throw new ArgumentException("DataPresetId not set");
            if (string.IsNullOrWhiteSpace(model.DataValueStr)) throw new ArgumentException("DataValueStr not set");
        }
    }
}
