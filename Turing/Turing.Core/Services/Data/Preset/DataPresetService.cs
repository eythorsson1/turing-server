﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository;
using Turing.Core.Request.Data;
using Turing.Core.Request.Device.Feature.Data;
using Turing.Core.Service;
using Turing.Domain.Domain.Aggregated.DeviceFeature;
using Turing.Domain.Domain.Enums.Device;
using Turing.Domain.Domain.Models.Data.Preset;
using Turing.Domain.Domain.Models.Device.Feature.Data;
using Turing.Domain.Infrastructure.Extensions;

namespace Turing.Core.Services.Data
{

    public class DataPresetService : BaseService
    {
        public DataPresetService(IUnitOfWorkProvider uowProvider, RepoFactory repoFactory) : base(uowProvider, repoFactory)
        {

        }

        public List<DataPresetModel> GetList(DataPresetRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DataPresetRepository(uow);
                return repo.GetList(request);
            }
        }

        public DataPresetModel FirstOrDefault(DataPresetRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DataPresetRepository(uow);
                return repo.FirstOrDefault(request);
            }
        }

        public DataPresetModel GetById(long dataId)
        {
            if (dataId < 1) throw new ArgumentException("DataId is not set");

            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DataPresetRepository(uow);
                return repo.GetById(dataId);
            }
        }

        public void Update(DataPresetModel model)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                Update(uow, model);
            }
        }
        internal void Update(IUnitOfWork uow, DataPresetModel model)
        {
            Validate(model);

            var repo = RepoFactory.DataPresetRepository(uow);
            if (!repo.Update(model)) throw new ArgumentException("Failed to insert the User model");
        }

        internal long Insert(IUnitOfWork uow, DataPresetModel model)
        {
            Validate(model);

            var repo = RepoFactory.DataPresetRepository(uow);
            var dataId = repo.Insert(model);
            if (dataId < 1) throw new ArgumentException("Failed to insert the User model");
            model.GetType().GetProperty(nameof(model.DataPresetId)).SetValue(model, dataId);
            return dataId;
        }

        private void Validate(DataPresetModel model)
        {
            if (model.ClientId < 0) throw new ArgumentException("ClientId not set");
            if (string.IsNullOrWhiteSpace(model.DataPresetStr)) throw new ArgumentException("DataPresetStr not set");
        }

        internal string Parse(string dataPresetStr)
        {
            throw new NotImplementedException();
            //List <DeviceModel> deviceList;
            //{
            //    var deviceIds = new Regex(@"{{Device\.(.*?)\.").Matches(dataPresetStr)
            //            .SelectList(x => long.Parse(x.Groups[1].Value));

            //    using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
            //        var deviceRepo = RepoFactory.DeviceRepository(uow);
            //        var dataValueRepo = RepoFactory.DataValueRepository(uow);
            //        var dataPresetRepo = RepoFactory.DataPresetRepository(uow);

            //        deviceList = deviceRepo.GetList(new DeviceRequest { DeviceIds = deviceIds });

            //        if (deviceList.SelectList(x => x.DataValueId).Any(x => x > 0)) {
            //            var deviceDataDict = dataValueRepo.GetList(new DataValueRequest { DataValueIds = deviceList.SelectList(x => x.DataValueId) })
            //                .ToDictionary(x => x.DataValueId);

            //            deviceList.ForEach(x => x.GetType().GetProperty(nameof(x.Data)).SetValue(x, deviceDataDict[x.DataValueId]));
            //        }

            //        if (deviceList.SelectList(x => x.DataPresetId).Any(x => x > 0)) {
            //            var deviceDataDict = dataPresetRepo.GetList(new DataPresetRequest { DataPresetIds = deviceList.SelectList(x => x.DataPresetId) })
            //                .ToDictionary(x => x.DataPresetId);

            //            deviceList.ForEach(x => x.GetType().GetProperty(nameof(x.DataPreset)).SetValue(x, deviceDataDict[x.DataPresetId]));
            //        }
            //    }
            //} 

            //var deviceDict = deviceList.ToDictionary(x => x.DeviceId);

            //return PresetHelper.PresetRegex.Replace(dataPresetStr, match => {
            //    string presetStr = match.Groups[1].Value;
            //    var parts = presetStr.Split(".");
            //    string value;

            //    switch (parts[0]) {
            //        case "Device": {
            //            var device = deviceDict[long.Parse(parts[1])];
            //            var path = device.DataPreset.PresetDataPathDict[string.Join(".", parts.Skip(2))];
            //            device.Data.TryGetPath(path, out value);

            //            break;
            //        }
            //        default: throw new NotImplementedException();
            //    }

            //    return value;
            //});
        }

        internal string ParsePreset(string dataPresetStr, List<DeviceFeatureDataModel> dd_models = null)
        {
            string response = "";

            response = ParseDeviceData(dataPresetStr, dd_models: dd_models);
            //response = ParseWebRequestData(dataPresetStr, wd_models: wd_models);

            return response;
        }

        private string ParseDeviceData(string dataPresetStr, List<DeviceFeatureDataModel> dd_models = null)
        {
            if (dd_models == null) dd_models = new List<DeviceFeatureDataModel>();

            // Get all the items not included as a parameter
            var dd_request = new Regex(@"{{Device\.([0-9]+)\.([0-9]+).*?}}").Matches(dataPresetStr)
                .Select(x => new DeviceFeatureDataRequest {
                    DeviceId = long.Parse(x.Groups[1].Value),
                    DeviceFeatureId = long.Parse(x.Groups[2].Value)
                })
                .WhereList(r => !dd_models.Any(m => m.DeviceId == r.DeviceId && m.DeviceFeatureId == r.DeviceFeatureId));


            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var deviceFeatureDataRepo = RepoFactory.DeviceFeatureDataRepository(uow);

                dd_models.AddRange(dd_request.Select(request => deviceFeatureDataRepo.FirstOrDefault(request)));
            }


            foreach (var model in dd_models) {
                string presetStr = $"Device.{model.DeviceId}.{model.DeviceFeatureId}";

                if (model.Type == DeviceFeatureEnum.ColorPicker) {
                    var colorPicker = new DeviceFeature_ColorPicker();
                    colorPicker.Hex = model.Value;

                    dataPresetStr = dataPresetStr.Replace("{{" + presetStr + ".Hex}}", colorPicker.Hex.ToString());

                    dataPresetStr = dataPresetStr.Replace("{{" + presetStr + ".Red}}", colorPicker.Red.ToString());
                    dataPresetStr = dataPresetStr.Replace("{{" + presetStr + ".Green}}", colorPicker.Green.ToString());
                    dataPresetStr = dataPresetStr.Replace("{{" + presetStr + ".Blue}}", colorPicker.Blue.ToString());

                    dataPresetStr = dataPresetStr.Replace("{{" + presetStr + ".Hue}}", colorPicker.Hue.ToString());
                    dataPresetStr = dataPresetStr.Replace("{{" + presetStr + ".Saturation}}", colorPicker.Saturation.ToString());
                    dataPresetStr = dataPresetStr.Replace("{{" + presetStr + ".Value}}", colorPicker.Value.ToString());
                }
                else {
                    dataPresetStr = dataPresetStr.Replace("{{" + presetStr + "}}", model.Value);
                }
            }

            return dataPresetStr;
        }
    }
}
