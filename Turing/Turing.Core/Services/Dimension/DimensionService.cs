﻿using System;
using System.Collections.Generic;
using System.Linq;
using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository;
using Turing.Core.Request.Dimension;
using Turing.Core.Service;
using Turing.Domain.Domain.Models.Dimension;
using Turing.Domain.Infrastructure;
using Turing.Domain.Infrastructure.Extensions;

namespace Turing.Core.Services.Dimension
{

    public class DimensionService : BaseService
    {
        private DimensionRegisterLinkService DimensionRegisterLinkService;
        private DimensionRegisterService DimensionRegisterService;

        public DimensionService(IUnitOfWorkProvider uowProvider, RepoFactory repoFactory, DimensionRegisterLinkService dimensionRegisterLinkService, DimensionRegisterService dimensionRegisterService) : base(uowProvider, repoFactory)
        {
            DimensionRegisterLinkService = dimensionRegisterLinkService;
            DimensionRegisterService = dimensionRegisterService;
        }

        public PagedList<DimensionModel> GetPagedList(DimensionRequest request, bool includeRegister = false)
        {
            PagedList<DimensionModel> pagedList;
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DimensionRepository(uow);
                pagedList = repo.GetPagedList(request);
            }

            if (includeRegister) {
                var links = DimensionRegisterLinkService.GetList(new DimensionRegisterLinkRequest { DimensionIds = pagedList.Items.SelectList(x => x.DimensionId) });
                var linkDimensionDict = links.GroupBy(x => x.DimensionId).ToDictionary(x => x.Key, x => x.ToList());

                if (links.Any()) {
                    var registerDict = DimensionRegisterService.GetList(new DimensionRegisterRequest { DimensionRegisterIds = links.SelectList(x => x.DimensionRegisterId) })
                        .ToDictionary(x => x.DimensionRegisterId);

                    foreach (var model in pagedList.Items) {
                        var r_models = linkDimensionDict[model.DimensionId].SelectList(x => registerDict[x.DimensionRegisterId]);
                        model.GetType().GetProperty(nameof(model.Register)).SetValue(model, r_models);
                    }
                }
            }

            return pagedList;
        }

        public List<DimensionModel> GetList(DimensionRequest request, bool includeRegister = false)
        {
            List<DimensionModel> list;
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DimensionRepository(uow);
                list = repo.GetList(request);
            }

            if (includeRegister) {
                var links = DimensionRegisterLinkService.GetList(new DimensionRegisterLinkRequest { DimensionIds = list.SelectList(x=> x.DimensionId) });
                var linkDimensionDict = links.GroupBy(x=> x.DimensionId).ToDictionary(x => x.Key, x=> x.ToList());

                if (links.Any()) {
                    var registerDict = DimensionRegisterService.GetList(new DimensionRegisterRequest { DimensionRegisterIds = links.SelectList(x => x.DimensionRegisterId) })
                        .ToDictionary(x=> x.DimensionRegisterId);
             
                    foreach(var model in list) {
                        var r_models = linkDimensionDict[model.DimensionId].SelectList(x => registerDict[x.DimensionRegisterId]);
                        model.GetType().GetProperty(nameof(model.Register)).SetValue(model, r_models);
                    }
                }
            }

            return list;
        }

        public DimensionModel FirstOrDefault(DimensionRequest request, bool includeRegister = false)
        {
            DimensionModel model;
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DimensionRepository(uow);
                model = repo.FirstOrDefault(request);
            }

            if (includeRegister) {
                var links = DimensionRegisterLinkService.GetList(new DimensionRegisterLinkRequest { DimensionId = model.DimensionId });

                if (links.Any()) {
                    var r_models = DimensionRegisterService.GetList(new DimensionRegisterRequest { DimensionRegisterIds = links.SelectList(x => x.DimensionRegisterId) });
                    model.GetType().GetProperty(nameof(model.Register)).SetValue(model, r_models);
                }
            }

            return model;
        }

        public DimensionModel GetById(long dimensionId, bool includeRegister = false)
        {
            return FirstOrDefault(new DimensionRequest { DimensionId = dimensionId }, includeRegister: includeRegister);
        }


        internal long Insert(IUnitOfWork uow, DimensionModel model, List<DimensionRegisterModel> dimensionRegister = null)
        {
            if (uow.IDBTransaction == null) throw new ArgumentException("The insert statement needs a transaction");

            Validate(model);

            var repo = RepoFactory.DimensionRepository(uow);
            var dataId = repo.Insert(model);
            if (dataId < 1) throw new ArgumentException("Failed to insert the User model");
            model.GetType().GetProperty(nameof(model.DimensionId)).SetValue(model, dataId);

            if (!dimensionRegister.Empty()) {
                foreach(var dr_model in dimensionRegister) {
                    DimensionRegisterService.Insert(uow, dr_model);

                    var link = new DimensionRegisterLinkModel(dr_model.DimensionRegisterId, model.DimensionId);
                    DimensionRegisterLinkService.Insert(uow, link);
                }
            }

            return dataId;
        }

        private void Validate(DimensionModel model)
        {
            if (model.Type < 0) throw new ArgumentException("Type not set");
            if (model.ClientId < 0) throw new ArgumentException("ClientId not set");
        }
    }
}
