﻿using System;
using System.Collections.Generic;
using System.Text;
using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository;
using Turing.Core.Request.Dimension;
using Turing.Core.Service;
using Turing.Domain.Domain.Models.Dimension;
using Turing.Domain.Exceptions;
using Turing.Domain.Infrastructure;

namespace Turing.Core.Services.Dimension
{

    public class DimensionRegisterService : BaseService
    {
        public DimensionRegisterService(IUnitOfWorkProvider uowProvider, RepoFactory repoFactory) : base(uowProvider, repoFactory)
        {
        }

        public PagedList<DimensionRegisterModel> GetPagedList(DimensionRegisterRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DimensionRegisterRepository(uow);
                return repo.GetPagedList(request);
            }
        }

        public List<DimensionRegisterModel> GetList(DimensionRegisterRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DimensionRegisterRepository(uow);
                return repo.GetList(request);
            }
        }

        public DimensionRegisterModel FirstOrDefault(DimensionRegisterRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DimensionRegisterRepository(uow);
                return repo.FirstOrDefault(request);
            }
        }

        public DimensionRegisterModel GetById(long dimensionRegisterId)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DimensionRegisterRepository(uow);
                return repo.GetById(dimensionRegisterId);
            }
        }

        public void Update(DimensionRegisterModel model)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                Update(uow, model);
            }
        }
        internal void Update(IUnitOfWork uow, DimensionRegisterModel model)
        {
            Validate(model);

            var repo = RepoFactory.DimensionRegisterRepository(uow);
            if (!repo.Update(model)) throw new ArgumentException("Failed to insert the DimensionRegister model");
        }

        internal long Insert(IUnitOfWork uow, DimensionRegisterModel model)
        {
            Validate(model);

            var repo = RepoFactory.DimensionRegisterRepository(uow);
            var dimensionRegisterId = repo.Insert(model);
            if (dimensionRegisterId < 1) throw new ArgumentException("Failed to insert the DimensionRegister model");
            model.GetType().GetProperty(nameof(model.DimensionRegisterId)).SetValue(model, dimensionRegisterId);
            return dimensionRegisterId;
        }

        private void Validate(DimensionRegisterModel model)
        {
            if (model.Type == 0) throw new ArgumentException("Type not set");

            //if (string.IsNullOrWhiteSpace(model.Code)) throw new FeedbackException("Code not set");
            if (string.IsNullOrWhiteSpace(model.Description)) throw new FeedbackException("Description not set");
        }
    }
}
