﻿using System;
using System.Collections.Generic;
using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository;
using Turing.Core.Request.Dimension;
using Turing.Core.Service;
using Turing.Domain.Domain.Models.Dimension;

namespace Turing.Core.Services.Dimension
{

    public class DimensionRegisterLinkService : BaseService
    {
        public DimensionRegisterLinkService(IUnitOfWorkProvider uowProvider, RepoFactory repoFactory) : base(uowProvider, repoFactory)
        {
        }

        public List<DimensionRegisterLinkModel> GetList(DimensionRegisterLinkRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DimensionRegisterLinkRepository(uow);
                return repo.GetList(request);
            }
        }

        public DimensionRegisterLinkModel FirstOrDefault(DimensionRegisterLinkRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DimensionRegisterLinkRepository(uow);
                return repo.FirstOrDefault(request);
            }
        }

        public DimensionRegisterLinkModel GetById(long dimensionRegisterLinkId)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DimensionRegisterLinkRepository(uow);
                return repo.GetById(dimensionRegisterLinkId);
            }
        }

        internal long Insert(IUnitOfWork uow, DimensionRegisterLinkModel model)
        {
            Validate(model);

            var repo = RepoFactory.DimensionRegisterLinkRepository(uow);
            var dimensionRegisterLinkId = repo.Insert(model);
            if (dimensionRegisterLinkId < 1) throw new ArgumentException("Failed to insert the User model");
            model.GetType().GetProperty(nameof(model.DimensionRegisterLinkId)).SetValue(model, dimensionRegisterLinkId);
            return dimensionRegisterLinkId;
        }

        internal bool Delete(IUnitOfWork uow, long dimensionRegisterLinkId)
        {
            var repo = RepoFactory.DimensionRegisterLinkRepository(uow);
            return repo.Delete(dimensionRegisterLinkId);
        }

        private void Validate(DimensionRegisterLinkModel model)
        {
            if (model.DimensionId < 0) throw new ArgumentException("DimensionId not set");
            if (model.DimensionRegisterId < 0) throw new ArgumentException("DimensionRegisterId not set");
        }
    }
}
