﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository;
using Turing.Core.Request.Device.Feature;
using Turing.Core.Request.Device.Feature.Data;
using Turing.Core.Request.Dimension;
using Turing.Core.Service;
using Turing.Core.Services.Device.Feature;
using Turing.Core.Services.Dimension;
using Turing.Domain.Domain.Enums.Device;
using Turing.Domain.Domain.Models.Device.Feature;
using Turing.Domain.Domain.Models.Device.Feature.Data;
using Turing.Domain.Domain.Models.Dimension;
using Turing.Domain.Infrastructure.Extensions;

namespace Turing.Core.Services.Device.Type.Feature.Data
{

    public class DeviceFeatureDataService : BaseService
    {
        private readonly DeviceFeatureService DeviceFeatureService;
        private DimensionService DimensionService;

        public DeviceFeatureDataService(IUnitOfWorkProvider uowProvider, RepoFactory repoFactory,
            DeviceFeatureService deviceFeatureService, DimensionService dimensionService
            ) : base(uowProvider, repoFactory)
        {
            DeviceFeatureService = deviceFeatureService;
            DimensionService = dimensionService;
        }

        public List<DeviceFeatureDataModel> GetList(DeviceFeatureDataRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DeviceFeatureDataRepository(uow);
                return repo.GetList(request);
            }
        }

        public DeviceFeatureDataModel FirstOrDefault(DeviceFeatureDataRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DeviceFeatureDataRepository(uow);
                return repo.FirstOrDefault(request);
            }
        }

        public DeviceFeatureDataModel GetById(long deviceFeatureDataId)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DeviceFeatureDataRepository(uow);
                return repo.GetById(deviceFeatureDataId);
            }
        }


        internal void Update(List<DeviceFeatureDataModel> models, long currentUserId)
        {
            if (models.Empty()) throw new ArgumentException("Models is not set");

            var features = DeviceFeatureService.GetList(new DeviceFeatureRequest { DeviceFeatureIds = models.SelectList(x => x.DeviceFeatureId) });

            var dimensions = new List<DimensionModel>();
            if (features.Any(x => x.DeviceId > 0))
                dimensions = DimensionService.GetList(new DimensionRequest { DimensionIds = features.SelectList(x => x.DimensionId) });

            var featureDict = features.ToDictionary(x => x.DeviceFeatureId);
            var dimensionDict = dimensions.ToDictionary(x => x.DimensionId);

            using (IUnitOfWork uow = UowProvider.GetUnitOfWork(useTransaction: true)) {
                foreach (var model in models) {
                    var feature = featureDict[model.DeviceFeatureId];
                    dimensionDict.TryGetValue(feature.DimensionId, out var dimension);

                    Validate(model, feature, dimension);

                    Delete(uow, model.DeviceFeatureDataId);
                    var repo = RepoFactory.DeviceFeatureDataRepository(uow);
                    if (!repo.Update(model)) throw new ArgumentException("Failed to insert the User model");
                }
            }
        }

        internal long Insert(IUnitOfWork uow, DeviceFeatureDataModel model, bool validateModel = false)
        {
            if (validateModel) throw new NotFiniteNumberException();

            var repo = RepoFactory.DeviceFeatureDataRepository(uow);
            var deviceFeatureDataId = repo.Insert(model);
            if (deviceFeatureDataId < 1) throw new ArgumentException("Failed to insert the User model");
            model.GetType().GetProperty(nameof(model.DeviceFeatureDataId)).SetValue(model, deviceFeatureDataId);
            return deviceFeatureDataId;
        }

        internal void Delete(IUnitOfWork uow, long deviceFeatureDataId)
        {
            if (deviceFeatureDataId < 1) throw new ArgumentException("Failed to insert the User model");
            var repo = RepoFactory.DeviceFeatureDataRepository(uow);
            if (!repo.Delete(deviceFeatureDataId))
                throw new ArgumentException("Failed to delete the DeviceFeatureData model");
        }


        /// <summary>
        /// Validates all the properties of the model, and that the value property is correct based on the feature
        /// </summary>
        /// <param name="d_model">Must include the DimensionRegisterList</param>
        internal void Validate(DeviceFeatureDataModel model, DeviceFeatureModel f_model, DimensionModel d_model = null)
        {
            if (model.DeviceId < 0) throw new ArgumentException("DeviceId not set");
            if (model.DeviceFeatureId < 0) throw new ArgumentException("DeviceFeatureId not set");
            if (string.IsNullOrWhiteSpace(model.Value)) throw new ArgumentException("Value not set");

            if (f_model.DimensionId > 0 && (d_model == null || d_model.Register == null))
                throw new ArgumentException("The dimension model or dimension model register is not included in the request");

            if (f_model.Type == DeviceFeatureEnum.ColorPicker) {
                if (!new Regex("^#[0-9A-Fa-f]{6}$").IsMatch(model.Value))
                    throw new ArgumentException("The provided hex color is not valid");
            }
            else if (f_model.Type == DeviceFeatureEnum.PowerState) {
                if (!bool.TryParse(model.Value, out bool val))
                    throw new ArgumentException("The provided PowerState value is not valid");
            }
            else if (f_model.Type == DeviceFeatureEnum.Toggle) {
                if (!bool.TryParse(model.Value, out bool val))
                    throw new ArgumentException("The provided Toggle value is not valid");
            }
            else if (f_model.Type == DeviceFeatureEnum.Slider) {
                if (!int.TryParse(model.Value, out int value) || 0 > value || value > 100)
                    throw new ArgumentException("The provided Slider value is not valid");
            }
            else if (f_model.Type == DeviceFeatureEnum.Brightness) {
                if (!int.TryParse(model.Value, out int value) || 0 > value || value > 100)
                    throw new ArgumentException("The provided Slider value is not valid");
            }
            else if (f_model.Type == DeviceFeatureEnum.Select) {
                if (!d_model.Register.Any(x => x.Code == model.Value)) {
                    throw new ArgumentException("The provided Select value is not valid");
                }
            }
            else if (f_model.Type == DeviceFeatureEnum.ColorShema)
                throw new NotImplementedException();
        }
    }
}
