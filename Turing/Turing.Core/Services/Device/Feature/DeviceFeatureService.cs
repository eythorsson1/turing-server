﻿using System;
using System.Collections.Generic;
using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository;
using Turing.Core.Request.Device.Feature;
using Turing.Core.Service;
using Turing.Domain.Domain.Models.Device.Feature;
using Turing.Domain.Exceptions;
using Turing.Domain.Infrastructure;

namespace Turing.Core.Services.Device.Feature
{

    public class DeviceFeatureService : BaseService
    {
        public DeviceFeatureService(IUnitOfWorkProvider uowProvider, RepoFactory repoFactory) : base(uowProvider, repoFactory)
        {
        }

        public List<LookupItem> GetLookup(DeviceFeatureRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DeviceFeatureRepository(uow);
                return repo.GetLookup(request);
            }
        }

        public PagedList<DeviceFeatureModel> GetPagedList(DeviceFeatureRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DeviceFeatureRepository(uow);
                return repo.GetPagedList(request);
            }
        }

        public List<DeviceFeatureModel> GetList(DeviceFeatureRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                return GetList(uow, request);
            }
        }
        internal List<DeviceFeatureModel> GetList(IUnitOfWork uow, DeviceFeatureRequest request)
        {
            var repo = RepoFactory.DeviceFeatureRepository(uow);
            return repo.GetList(request);
        }

        public DeviceFeatureModel FirstOrDefault(DeviceFeatureRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DeviceFeatureRepository(uow);
                return repo.FirstOrDefault(request);
            }
        }

        public DeviceFeatureModel GetById(long deviceFeatureId)
        {
            return FirstOrDefault(new DeviceFeatureRequest { DeviceFeatureId = deviceFeatureId });
        }
        internal DeviceFeatureModel GetById(IUnitOfWork uow, long deviceFeatureId)
        {
            var repo = RepoFactory.DeviceFeatureRepository(uow);
            return repo.GetById(deviceFeatureId);
        }

        public void Update(DeviceFeatureModel model)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                Update(uow, model);
            }
        }
        internal void Update(IUnitOfWork uow, DeviceFeatureModel model)
        {
            Validate(model);

            var repo = RepoFactory.DeviceFeatureRepository(uow);
            if (!repo.Update(model)) throw new ArgumentException("Failed to insert the User model");
        }

        internal long Insert(IUnitOfWork uow, DeviceFeatureModel model)
        {
            Validate(model);

            var repo = RepoFactory.DeviceFeatureRepository(uow);
            var deviceFeatureId = repo.Insert(model);
            if (deviceFeatureId < 1) throw new ArgumentException("Failed to insert the User model");
            model.GetType().GetProperty(nameof(model.DeviceFeatureId)).SetValue(model, deviceFeatureId);
            return deviceFeatureId;
        }

        private void Validate(DeviceFeatureModel model)
        {
            if (model.DeviceId < 0) throw new ArgumentException("DeviceId not set");
            if (model.Type < 0) throw new ArgumentException("Type not set");
            if (string.IsNullOrWhiteSpace(model.Name)) throw new FeedbackException("Name not set");
        }
    }
}
