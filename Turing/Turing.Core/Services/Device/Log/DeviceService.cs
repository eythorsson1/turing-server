﻿using System.Collections.Generic;
using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository;
using Turing.Core.Request.Device;
using Turing.Core.Request.Device.Log;
using Turing.Core.Service;
using Turing.Domain.Enums.Device;
using Turing.Domain.Models.Device.Log;
using Turing.Domain.Infrastructure;

namespace Turing.Core.Services.Device.Log
{
    public class DeviceLogService : BaseService
    {
        public DeviceLogService(IUnitOfWorkProvider uowProvider, RepoFactory repoFactory) : base(uowProvider, repoFactory)
        {
        }


        public PagedList<DeviceLogModel> GetPagedList(DeviceLogFilterRequest request)
        {
            using (var uow = UowProvider.GetUnitOfWork())
            {
                return GetPagedList(uow, request);
            }
        }

        private PagedList<DeviceLogModel> GetPagedList(IUnitOfWork uow, DeviceLogFilterRequest request)
        {
            var repo = RepoFactory.DeviceLogRepository(uow);
            return repo.GetPagedList(request);
        }

        public List<DeviceLogModel> GetList(DeviceLogFilterRequest request)
        {
            using (var uow = UowProvider.GetUnitOfWork())
            {
                return GetList(uow, request);
            }
        }

        private List<DeviceLogModel> GetList(IUnitOfWork uow, DeviceLogFilterRequest request)
        {
            var repo = RepoFactory.DeviceLogRepository(uow);
            return repo.GetList(request);
        }

        public DeviceLogModel GetById(long deviceId)
        {
            using (var uow = UowProvider.GetUnitOfWork())
            {
                return GetById(uow, deviceId);
            }
        }

        private DeviceLogModel GetById(IUnitOfWork uow, long deviceId)
        {
            var repo = RepoFactory.DeviceLogRepository(uow);
            return repo.GetById(deviceId);
        }

        public long Insert(long deviceId, DeviceLogActionEnum action, string description, string technicalDescription, long clientId,
            long actionByUserId = 0, string actionByEmail = "", string actionByFirstName = "System",
            string actionByLastName = "")
        {

            if (actionByUserId == 0) {
                actionByEmail = "";
                actionByFirstName = "System";
                actionByLastName = "";
            }

            var model = new DeviceLogModel(deviceId, action, description, technicalDescription, clientId, actionByUserId: actionByUserId,
                actionByEmail: actionByEmail, actionByFirstName: actionByFirstName, actionByLastName: actionByLastName);

            using (var uow = UowProvider.GetUnitOfWork()) {
                return Insert(uow, model);
            }
        }

        internal long Insert(IUnitOfWork uow, long deviceId, DeviceLogActionEnum action, string description, string technicalDescription, long clientId,
            long actionByUserId = 0, string actionByEmail = "", string actionByFirstName = "System",
            string actionByLastName = "")
        {

            if (actionByUserId == 0) {
                actionByEmail = "";
                actionByFirstName = "System";
                actionByLastName = "";
            }

            var model = new DeviceLogModel(deviceId, action, description, technicalDescription, clientId, actionByUserId: actionByUserId,
                actionByEmail: actionByEmail, actionByFirstName: actionByFirstName, actionByLastName: actionByLastName);

            return Insert(uow, model);
        }

        private long Insert(IUnitOfWork uow, DeviceLogModel model)
        {
            var repo = RepoFactory.DeviceLogRepository(uow);
            return repo.Insert(model);
        }
    }
}
