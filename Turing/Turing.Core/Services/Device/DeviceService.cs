﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Turing.Core.Infrastructure.Mqtt;
using Turing.Core.Infrastructure.Mqtt.Dto;
using Turing.Core.Infrastructure.Mqtt.Exceptions;
using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository;
using Turing.Core.Request.Device;
using Turing.Core.Request.Device.Feature;
using Turing.Core.Request.Device.Feature.Data;
using Turing.Core.Request.Dimension;
using Turing.Core.Service;
using Turing.Core.Services.Data;
using Turing.Core.Services.Device.Feature;
using Turing.Core.Services.Device.Log;
using Turing.Core.Services.Device.Type.Feature.Data;
using Turing.Core.Services.Dimension;
using Turing.Core.Services.User;
using Turing.Domain.Domain.Enums.Device;
using Turing.Domain.Domain.Enums.Dimension;
using Turing.Domain.Domain.Models.Data.Preset;
using Turing.Domain.Domain.Models.Device.Feature;
using Turing.Domain.Domain.Models.Device.Feature.Data;
using Turing.Domain.Domain.Models.Dimension;
using Turing.Domain.Enums.Device;
using Turing.Domain.Exceptions;
using Turing.Domain.Infrastructure;
using Turing.Domain.Infrastructure.Extensions;
using Turing.Domain.Infrastructure.Helper;
using Turing.Domain.Models.Device;

namespace Turing.Core.Services.Device
{
    public class DeviceService : BaseService
    {
        private readonly DeviceFeatureDataService DeviceFeatureDataService;
        private readonly DeviceFeatureService DeviceFeatureService;
        private readonly DeviceLogService DeviceLogService;
        private readonly DataPresetService DataPresetService;
        private readonly DimensionService DimensionService;
        private readonly UserService UserService;


        public DeviceService(IUnitOfWorkProvider uowProvider, RepoFactory repoFactory,
            DeviceFeatureDataService deviceFeatureDataService,
            DeviceFeatureService deviceFeatureService,
            DeviceLogService deviceLogService, DataPresetService dataPresetService,
            DimensionService dimensionService, UserService userService
            ) : base(uowProvider, repoFactory)
        {
            DeviceFeatureDataService = deviceFeatureDataService;
            DeviceFeatureService = deviceFeatureService;
            DeviceLogService = deviceLogService;
            DataPresetService = dataPresetService;
            DimensionService = dimensionService;
            UserService = userService;
        }

        public List<LookupItem> GetLookup(DeviceRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DeviceRepository(uow);
                return repo.GetLookup(request);
            }
        }


        public PagedList<DeviceModel> GetPagedList(DeviceRequest request, bool includeDeviceData = false, bool includeDeviceFeatures = false)
        {
            using (var uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DeviceRepository(uow);
                var pagedList = repo.GetPagedList(request);

                throw new NotImplementedException();
                // //if (includeDeviceData) {
                // //    var deviceDataDict = DeviceDataService.GetList(new DeviceDataRequest { DeviceIds = pagedList.Items.SelectList(x => x.DeviceId) })
                // //        .GroupBy(x => x.DeviceId)
                // //        .ToDictionary(x => x.Key, x => x.ToDictionary(x => x.DataTypeFieldName, x => x.Value));

                // //    pagedList.Items.ForEach(x => x.GetType().GetProperty(nameof(x.Data)).SetValue(x.Data, deviceDataDict[x.DeviceId]));
                // //}

                // if (includeDeviceData) {
                //     //var deviceDataDict = DataValueService.GetList(new DataValueRequest { DataValueIds = pagedList.Items.SelectList(x => x.DataValueId) })
                //     //    .ToDictionary(x => x.DataValueId);

                //     //pagedList.Items.ForEach(x => x.Data.SetPropertyValue(deviceDataDict[x.DeviceId]));
                // }

                // if (includeDeviceFeatures) {
                //     var features = DeviceFeatureService.GetList(new DeviceFeatureRequest { DeviceIds = pagedList.Items.SelectList(x => x.DeviceId) })
                //         .ToDictionary(x => x.DeviceId);

                //     pagedList.Items.ForEach(x => x.GetType().GetProperty(nameof(x.Features)).SetValue(x.Features, features[x.DeviceId]));
                // }

                // return pagedList;
            }
        }

        public List<DeviceModel> GetList(DeviceRequest request)
        {
            using (var uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DeviceRepository(uow);
                return repo.GetList(request);
            }
        }

        public DeviceModel FirstOrDefault(DeviceRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.DeviceRepository(uow);
                return repo.FirstOrDefault(request);
            }
        }

        public DeviceModel GetById(long deviceId)
        {
            var request = new DeviceRequest { DeviceId = deviceId };
            return FirstOrDefault(request);
        }

        public void Update(DeviceModel model)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                Update(uow, model);
            }
        }

        internal void Update(IUnitOfWork uow, DeviceModel model)
        {
            var repo = RepoFactory.DeviceRepository(uow);
            if (!repo.Update(model))
                throw new ArgumentException("Failed to update the device");
        }

        internal long Insert(IUnitOfWork uow, DeviceModel model)
        {
            Validate(model);
            var repo = RepoFactory.DeviceRepository(uow);
            var deviceId = repo.Insert(model);
            model.GetType().GetProperty(nameof(model.DeviceId)).SetValue(model, deviceId);

            if (deviceId < 1)
                throw new ArgumentException("Failed to insert the device");

            return deviceId;
        }


        public async Task SetState(long deviceId, Dictionary<long, string> data, long currentUserId)
        {
            if (data == null) throw new ArgumentException("data is not set");
            if (deviceId < 1) throw new ArgumentException("DeviceId is not set");
            if (currentUserId < 1) throw new ArgumentException("currentUserId is not set");

            var device = GetById(deviceId);
            var features = DeviceFeatureService.GetList(new DeviceFeatureRequest { DeviceId = device.DeviceId });
            var featureData = DeviceFeatureDataService.GetList(new DeviceFeatureDataRequest { DeviceId = deviceId });

            var dimensions = new List<DimensionModel>();

            if (features.Any(x => x.DimensionId > 0))
                dimensions = DimensionService.GetList(new DimensionRequest { DimensionIds = features.SelectList(x => x.DimensionId) }, includeRegister: true);


            var featureDict = features.ToDictionary(x => x.DeviceFeatureId);
            var featureDataDict = featureData.ToDictionary(x => x.DeviceFeatureId);
            var dimensionDict = dimensions.ToDictionary(x => x.DimensionId);

            /// Create the dataList
            var dataList = new List<DeviceFeatureDataModel>();
            foreach (var item in data) {
                if (!featureDict.TryGetValue(item.Key, out var feature))
                    throw new FeedbackException("One of the dataIds are not valid for the device");

                DeviceFeatureDataModel dataItem;
                if (featureDataDict.TryGetValue(item.Key, out dataItem)) {
                    dataItem.Value = item.Value;
                }
                else {
                    dataItem = new DeviceFeatureDataModel(deviceId, item.Key, item.Value);
                }

                dimensionDict.TryGetValue(feature.DimensionId, out var dimension);

                DeviceFeatureDataService.Validate(dataItem, feature, dimension);

                dataList.Add(dataItem);
            }

            // Make sure the dataList contains all the data so that we don't loose data
            // when we delete all the data that is stored on the specific device later on
            dataList.AddRange(featureData.Where(oldItem => !dataList.Any(newItem => newItem.DeviceFeatureId == oldItem.DeviceFeatureId)));

            /// Set the device state
            var dataPreset = DataPresetService.GetById(device.DataPresetId);

            var requestStr = DataPresetService.ParsePreset(dataPreset.DataPresetStr, dd_models: dataList);
            var request = JsonConvert.DeserializeObject(requestStr);

            var deviceResponse = await PublishPayloadToDevice(device, request, currentUserId);

            /// Save the data
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork(useTransaction: true)) {
                foreach (var item in dataList) {
                    if (item.DeviceFeatureDataId > 0)
                        DeviceFeatureDataService.Delete(uow, item.DeviceFeatureDataId);

                    DeviceFeatureDataService.Insert(uow, item);
                }

                uow.Commit();
            }
        }

        internal async Task SetupDevice(DeviceSetupRequest request)
        {

            using (IUnitOfWork uow = UowProvider.GetUnitOfWork(useTransaction: true)) {
                bool updateDeviceModel = false;

                string authName = StringHelper.GenerateRandomString(6);
                string authPass = StringHelper.GenerateRandomString(8);

                var model = new DeviceModel(request.DeviceName, authName, authPass, request.ClientId, request.CreatedByUserId, chipId: request.ChipId);

                // Create the device
                Insert(uow, model);


                // Create the provided dimensions
                var dimensions = new List<DimensionModel>();
                if (request.Dimensions.NotEmpty()) {
                    foreach (var item in request.Dimensions) {
                        var dimension = new DimensionModel(item.Name, item.Type, request.ClientId);
                        var register = item.Register.SelectList(x => new DimensionRegisterModel(x.Type, x.Code, x.Description));
                        DimensionService.Insert(uow, dimension, register);

                        // Add the dimension register to the list
                        dimension.GetType().GetProperty(nameof(dimension.Register)).SetValue(dimension, register);
                        dimensions.Add(dimension);
                    }
                }

                var dimensionNameDict = dimensions.ToDictionary(x => x.Name);
                //var dimensionDict = dimensions.ToDictionary(x => x.DimensionId);

                // Create the features and add the default value
                var features = new List<DeviceFeatureModel>();

                if (request.Features.NotEmpty()) {
                    var defaultData = new List<DeviceFeatureDataModel>();

                    foreach (var item in request.Features) {
                        var feature = new DeviceFeatureModel(model.DeviceId, item.Type, item.Name);
                        if (dimensionNameDict.TryGetValue(item.Name, out var dimension)) {
                            feature.DimensionId = dimension.DimensionId;
                        }

                        DeviceFeatureService.Insert(uow, feature);
                        features.Add(feature);

                        string value = "";

                        switch (feature.Type) {
                            case DeviceFeatureEnum.PowerState:
                            case DeviceFeatureEnum.Toggle:
                                value = "true";
                                break;
                            case DeviceFeatureEnum.Brightness:
                            case DeviceFeatureEnum.Slider:
                                value = "75";
                                break;
                            case DeviceFeatureEnum.ColorPicker:
                                value = "#EBD314";
                                break;
                            case DeviceFeatureEnum.Select:
                                value = dimension.Register.First().Code;
                                break;
                            default:
                                throw new NotImplementedException();

                        }
                        
                        defaultData.Add(new DeviceFeatureDataModel(model.DeviceId, feature.DeviceFeatureId, value));
                    }

                    foreach(var dfd_model in defaultData) {
                        DeviceFeatureDataService.Insert(uow, dfd_model);
                    }
                }


                // Create the DataPresetStr
                if (!string.IsNullOrEmpty(request.DataPresetStr)) {
                    var featureDict = features.ToDictionary(x => x.Name);

                    // Generate the dataPresetStr
                    string dataPresetStr = PresetHelper.PresetRegex.Replace(request.DataPresetStr, (match) => {
                        var str = match.Groups[1].Value;
                        var parts = str.Split(".");

                        if (featureDict.TryGetValue(str.Split(".")[0], out var feature)) {
                            var presetStr = $@"{{{{Device.{model.DeviceId}.{feature.DeviceFeatureId}{(parts.Length > 1 ? "." + parts.Skip(1).Join(".") : "")}}}}}";
                            PresetHelper.ValidateDataPreset(presetStr, features);
                            return presetStr;
                        }
                        else {
                            // TODO: Tell the user that the device has a error in the device Type feature ?
                            return match.Value;
                        }
                    });

                    var dp_model = new DataPresetModel(dataPresetStr, request.ClientId);
                    model.DataPresetId = DataPresetService.Insert(uow, dp_model);

                    updateDeviceModel = true;
                }

                if (updateDeviceModel)
                    Update(uow, model);


                // Send the DeviceDetails to the device
                var mqttRequest = new MqttPublishRequestDto(model.DeviceId, new {
                    ChipId = model.ChipId,
                    DeviceId = model.DeviceId,
                    AuthName = authName,
                    AuthPass = authPass,
                    MqttState = model.StateTopic,
                    MqttSet = model.SetTopic
                });

                var mqttResponse = await MqttAction.Publish("Device/Setup/Complete", mqttRequest, stateTopic: "Device/Setup/Success", 
                    callbackTimeout: 10000, qos: MqttQosEnum.ExactlyOnce, retainFlag: false);

                if (mqttResponse != null) {
                    bool.TryParse(mqttResponse.Data.ToString(), out bool success);

                    if (success) {
                        uow.Commit();
                    }
                }
            }
        }


        /// <summary>
        /// Publishes the payload message to the provided deviceId and waits for the a callback from the device confining its state.<br />
        /// The method throws an TimeoutException if the device does not respond within the timeout.
        /// </summary>
        /// <param name="deviceId">The DeviceId of the device to which the payload should be published</param>
        /// <param name="payload">The payload to send to the device</param>
        /// <param name="sendByUserId">If not set will the system log the sent by user as the sustem</param>
        /// <param name="topic">Will override the set topic that the payload is sent to</param>
        /// <returns>
        /// true: The Message was successfully send and the callback recived<br />
        /// false: The Message was successfully send and callback recived but not successfull.
        /// </returns>
        public async Task<MqttPublishResponseDto> PublishPayloadToDevice(DeviceModel device, object payload, long sendByUserId = 0, string requestTopic = null, string responseTopic = null)
        {
            // LOG THE REQUEST
            var actionUser = sendByUserId > 0 ? UserService.GetById(sendByUserId) : null;

            var deviceLogId = DeviceLogService.Insert(device.DeviceId, DeviceLogActionEnum.PublishMessage, "Message Sent", payload.ToString(),
                    device.ClientId, sendByUserId, actionUser?.Email, actionUser?.FirstName, actionUser?.LastName);

            // PERFORM THE REQUEST
            var request = new MqttPublishRequestDto(deviceLogId, payload);
            var response = await MqttAction.Publish((requestTopic ?? device.SetTopic), request, stateTopic: (responseTopic ?? device.StateTopic));

            // LOG THE RESPONSE
            string logMessage;
            if (response == null) logMessage = "Request failed. The device failed to respond to the request";
            //else if (!response.Success) logMessage = "Request failed. The device responded with success = false";
            else logMessage = "Success";

            DeviceLogService.Insert(device.DeviceId, DeviceLogActionEnum.PublishMessageResponse, logMessage,
                JsonConvert.SerializeObject(response != null ? response.Data ?? "" : ""), device.ClientId,
                sendByUserId, actionUser?.Email, actionUser?.FirstName, actionUser?.LastName);

            // RETURN RESPONSE
            if (response == null)
                throw new MqttException("Request Failed. The Device Failed to respond to the request");

            return response;
        }


        public async Task<MqttPublishResponseDto> PublishPayloadToDevice(IUnitOfWork uow, DeviceModel device, object payload, long sendByUserId = 0, string requestTopic = null, string responseTopic = null)
        {
            // LOG THE REQUEST
            var actionUser = sendByUserId > 0 ? UserService.GetById(sendByUserId) : null;

            var deviceLogId = DeviceLogService.Insert(uow, device.DeviceId, DeviceLogActionEnum.PublishMessage, "Message Sent", payload.ToString(),
                    device.ClientId, sendByUserId, actionUser?.Email, actionUser?.FirstName, actionUser?.LastName);

            // PERFORM THE REQUEST
            var request = new MqttPublishRequestDto(deviceLogId, payload);
            var response = await MqttAction.Publish((requestTopic ?? device.SetTopic), request, stateTopic: (responseTopic ?? device.StateTopic));

            // LOG THE RESPONSE
            string logMessage;
            if (response == null) logMessage = "Request failed. The device failed to respond to the request";
            //else if (!response.Success) logMessage = "Request failed. The device responded with success = false";
            else logMessage = "Success";

            DeviceLogService.Insert(uow, device.DeviceId, DeviceLogActionEnum.PublishMessageResponse, logMessage,
                JsonConvert.SerializeObject(response != null ? response.Data ?? "" : ""), device.ClientId,
                sendByUserId, actionUser?.Email, actionUser?.FirstName, actionUser?.LastName);

            // RETURN RESPONSE
            if (response == null)
                throw new MqttException("Request Failed. The Device Failed to respond to the request");

            return response;
        }


        private void Validate(DeviceModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Name)) throw new FeedbackException("The device must have a valid name");

            if (model.CreatedDateUTC == DateTime.MinValue) throw new ArgumentException("CreatedDateUTC is not set");
            if (model.CreatedByUserId < 1) throw new ArgumentException("ClientId is not set");
            if (model.ClientId < 1) throw new ArgumentException("ClientId is not set");
            if (string.IsNullOrWhiteSpace(model.SetTopic)) throw new ArgumentException("SetTopic is not set");
            if (string.IsNullOrWhiteSpace(model.StateTopic)) throw new ArgumentException("StateTopic is not set");

            if (string.IsNullOrWhiteSpace(model.AuthName)) throw new ArgumentNullException("AuthName is not set");
            if (model.AuthPasswordHash == null || model.AuthPasswordHash.Length == 0) throw new ArgumentNullException("AuthPasswordHash is not set");
        }



        /// <summary>
        /// Gets the device data based on the preset string
        /// </summary>
        /// <param name="presetTemplate">Should be in the following format; <DEVICE_NAME>.<FEATURE_NAME>.[<FEATURE_DATA>]</param>
        /// <returns>True if the presetTemplate string is valid string</returns>
        //public bool TryGetDeviceData(long clientId, string presetTemplate, out object value)
        //{
        //    value = null;
        //    string[] parts = presetTemplate.Split("."); // 1: DeviceName, 2: FeatureName, 3: FeatureValue

        //    // Find the device
        //    var device = FirstOrDefault(new DeviceRequest { ClientId = clientId, AuthName = parts[0] });
        //    if (device == null) return false;

        //    // Find the feature
        //    var feature_req = new DeviceTypeFeatureRequest { DeviceTypeId = device.DeviceTypeId };
        //    var featureDict = DeviceTypeFeatureService.GetList(feature_req).ToDictionary(x => x.Name);
        //    if (!featureDict.TryGetValue(parts[1], out var feature)) return false;

        //    var data_req = new DeviceDataRequest { DeviceId = device.DeviceId, DeviceTypeFeatureId = feature.DeviceTypeFeatureId };
        //    var data = DeviceDataService.FirstOrDefault(data_req);

        //    // Parse the Data
        //    if (feature.Type == DeviceTypeFeatureEnum.PowerState) {
        //        if(bool.TryParse(data.ValueStr, out bool val)) value = val;
        //    }
        //    else if (feature.Type == DeviceTypeFeatureEnum.ColorPicker) {
        //        var val = data.Value as DeviceTypeFeature_ColorPicker;
        //        var prop = val.GetType().GetProperty(parts[2], BindingFlags.IgnoreCase);
        //        if (prop != null) value = prop.GetValue(val);
        //    }
        //    else if (feature.Type == DeviceTypeFeatureEnum.ColorShema) {
        //        if (int.TryParse(data.ValueStr, out int val)) value = val;
        //    }
        //    else if (feature.Type == DeviceTypeFeatureEnum.Brightness) {
        //        if (int.TryParse(data.ValueStr, out int val)) value = val;
        //    }
        //    else if (feature.Type == DeviceTypeFeatureEnum.Select) {
        //        if (int.TryParse(data.ValueStr, out int val)) value = val;
        //    }
        //    else if (feature.Type == DeviceTypeFeatureEnum.Slider) {
        //        if (int.TryParse(data.ValueStr, out int val)) value = val;
        //    }
        //    else
        //        throw new NotImplementedException($"The {presetTemplate} feature is not implement");

        //    return true;
        //}


        /// <summary>
        /// Finds the correct device and device feature and creates a new DeviceDataModel
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="presetTemplate"></param>
        /// <param name="data"></param>
        /// <returns>True if the string and value is valid</returns>
        //public bool TryCreateDeviceData(long clientId, string presetTemplate, out DeviceDataModel data, object value)
        //{
        //    data = null;
        //    string[] parts = presetTemplate.Split("."); // 1: DeviceName, 2: FeatureName, 3: FeatureValue

        //    // Find the device
        //    var device = FirstOrDefault(new DeviceRequest { ClientId = clientId, AuthName = parts[0] });
        //    if (device == null) return false;

        //    // Find the feature
        //    var feature = DeviceTypeFeatureService.FirstOrDefault(new DeviceTypeFeatureRequest {
        //        DeviceTypeId = device.DeviceTypeId,
        //        Name = parts[1]
        //    });

        //    if (feature == null) return false;

        //    // Validate the data
        //    if (feature.Type == DeviceTypeFeatureEnum.PowerState) {
        //        if (!bool.TryParse(value.ToString(), out bool val)) return false;
        //        data = new DeviceDataModel(device.DeviceId, feature.DeviceTypeFeatureId, value.ToString());
        //    }
        //    else if (feature.Type == DeviceTypeFeatureEnum.ColorPicker) {




        //    }
        //    else if (feature.Type == DeviceTypeFeatureEnum.ColorShema) {
        //        if (!int.TryParse(data.ValueStr, out int val)) data return ;
        //    }
        //    else if (feature.Type == DeviceTypeFeatureEnum.Brightness) {
        //        if (int.TryParse(data.ValueStr, out int val)) data = val;
        //    }
        //    else if (feature.Type == DeviceTypeFeatureEnum.Select) {
        //        if (int.TryParse(data.ValueStr, out int val)) data = val;
        //    }
        //    else if (feature.Type == DeviceTypeFeatureEnum.Slider) {
        //        if (int.TryParse(data.ValueStr, out int val)) data = val;
        //    }
        //    else
        //        throw new NotImplementedException($"The {presetTemplate} feature is not implement");

        //    return true;
        //}





        //public List<DeviceDataModel> ParseRequest(DeviceModel device, Dictionary<string, object> data)
        //{
        //    var features = DeviceTypeFeatureService.GetList(new DeviceTypeFeatureRequest {
        //        DeviceTypeId = device.DeviceTypeId
        //    });

        //    var fields = DeviceTypeFieldService.GetList(new DeviceTypeFieldRequest {
        //        DeviceTypeId = device.DeviceTypeId
        //    });

        //    var featureDict = features.ToDictionary(x => x.Name);
        //    var fieldDict = fields.ToDictionary(x => x.Type);

        //    var response = new List<DeviceDataModel>();
        //    foreach (var item in data) {
        //        if (!featureDict.TryGetValue(item.Key, out DeviceTypeFeatureModel feature))
        //            throw new FeedbackException($"The key \"{item.Key}\" is not a valid device feature");

        //        if (feature.Type == DeviceTypeFeatureEnum.ColorPicker) {
        //            var value = item.Value as DeviceTypeFeature_ColorPicker;
        //            response.Add(new DeviceDataModel(deivceTypeFieldId, ))

        //        }
        //    }

        //    return response;
        //}

        //public List<DeviceDataModel> ParseModelData(long deviceId, long deviceTypeId, Dictionary<string, string> data, bool allowMissingFields = false, bool allowUnspecifiedFields = false)
        //{
        //    var deviceData = new List<DeviceDataModel>();
        //    var fields = DeviceTypeFieldService.GetList(new DeviceTypeFieldRequest {
        //        DeviceTypeId = deviceTypeId,
        //    });

        //    if (fields != null) {
        //        var missingFields = new List<string>();
        //        var invalidFields = new List<string>();

        //        foreach (var field in fields) {
        //            if (!data.TryGetValue(field.Name, out string value)) {
        //                missingFields.Add(field.Name);
        //            }
        //            else if (field.ValidationRegex != null && !Regex.Match(value, field.ValidationRegex).Success) {
        //                invalidFields.Add(field.Name);
        //            }
        //            else if (!string.IsNullOrEmpty(value)){
        //                deviceData.Add(new DeviceDataModel(deviceId, field.DeviceTypeFieldId, value));
        //            }
        //        }

        //        {
        //            string message = "";
        //            if (missingFields.Any() && !allowUnspecifiedFields) 
        //                message += "The following fields are missing: " + string.Join("\n", missingFields);

        //            if (invalidFields.Any()) 
        //                message += (message != null ? "\n" : "") + "The following fields have an invalid value: " + string.Join("\n", missingFields);

        //            if (!string.IsNullOrEmpty(message))
        //                throw new FeedbackException(message);
        //        }
        //    }

        //    return deviceData;
        //}


        ///// <summary>
        ///// Creates the DeviceTypeFieldModels and inserts them into the database
        ///// </summary>
        ///// <param name="uow">Requires a transaction</param>
        //internal List<DeviceTypeFieldModel> ParseFields(IUnitOfWork uow, long deviceTypeId, Dictionary<string, DeviceTypeFeatureEnum> features, object presetData, long parentFieldId = 0)
        //{
        //    if (uow.IDBTransaction == null) 
        //        throw new ArgumentException("ParseFields requires a transaction");

        //    var fields = new List<DeviceTypeFieldModel>();
        //    foreach (var preset in presetData as JObject) {
        //        if (preset.Value.Type == JTokenType.Object) {
        //            var model = new DeviceTypeFieldModel(deviceTypeId, preset.Key, DeviceTypeFieldTypeEnum.Object, parentDeviceTypeFieldId: parentFieldId);
        //            var fieldId = DeviceTypeFieldService.Insert(uow, model);
        //            model.GetType().GetProperty(nameof(model.DeviceTypeFieldId)).SetValue(model, fieldId);

        //            fields.Add(model);
        //            fields.AddRange(ParseFields(uow, deviceTypeId, features, preset.Value, parentFieldId: fieldId));
        //        }
        //        else {
        //            var match = Regex.Match(preset.Value.ToString(), "^\\{\\{([A-Za-z0-9_\\.]*)\\}\\}$");

        //            DeviceTypeFieldTypeEnum type;
        //            if (!match.Success) {
        //                type = DeviceTypeFieldTypeEnum.StaticValue;
        //            }
        //            else {
        //                var parts = match.Groups[1].Value.Split(".");

        //                if (!features.TryGetValue(parts[0], out DeviceTypeFeatureEnum featureType))
        //                    throw new ArgumentException($"The provided key {preset.Key} is not valid.");

        //                if (featureType == DeviceTypeFeatureEnum.PowerState) {
        //                    type = DeviceTypeFieldTypeEnum.PowerState;
        //                }
        //                else if (featureType == DeviceTypeFeatureEnum.ColorPicker) {
        //                    if (parts[1] == "Red") type = DeviceTypeFieldTypeEnum.ColorRed;
        //                    else if (parts[1] == "Green") type = DeviceTypeFieldTypeEnum.ColorGreen;
        //                    else if (parts[1] == "Blue") type = DeviceTypeFieldTypeEnum.ColorBlue;
        //                    else if (parts[1] == "Hue") type = DeviceTypeFieldTypeEnum.ColorHue;
        //                    else if (parts[1] == "Saturation") type = DeviceTypeFieldTypeEnum.ColorSaturation;
        //                    else if (parts[1] == "Value") type = DeviceTypeFieldTypeEnum.ColorValue;
        //                    else throw new ArgumentException($"The value \"{parts[1]}\" is not valid a member of the ColorPicker feature");
        //                }
        //                else if (featureType == DeviceTypeFeatureEnum.ColorShema) {
        //                    type = DeviceTypeFieldTypeEnum.ColorScheme;
        //                }
        //                else if (featureType == DeviceTypeFeatureEnum.Brightness) {
        //                    type = DeviceTypeFieldTypeEnum.Brightness;
        //                }
        //                else if (featureType == DeviceTypeFeatureEnum.Select) {
        //                    type = DeviceTypeFieldTypeEnum.Select;
        //                }
        //                else if (featureType == DeviceTypeFeatureEnum.Slider) {
        //                    type = DeviceTypeFieldTypeEnum.Slider;
        //                }
        //                else
        //                    throw new NotImplementedException($"The {preset.Value} feature is not implement");
        //            }

        //            var model = new DeviceTypeFieldModel(deviceTypeId, preset.Key, type, parentDeviceTypeFieldId: parentFieldId);
        //            var fieldId = DeviceTypeFieldService.Insert(uow, model);
        //            model.GetType().GetProperty(nameof(model.DeviceTypeFieldId)).SetValue(model, fieldId);
        //            fields.Add(model);
        //        }
        //    }

        //    return fields;
        //}

        ///// <summary>
        ///// Parses the JsonRequest for a given device
        ///// </summary>
        //public List<DeviceDataModel> ParseData(long deviceId, object request)
        //{
        //    var device = GetById(deviceId);

        //    var fields = DeviceTypeFieldService.GetList(new DeviceTypeFieldRequest {
        //        DeviceTypeId = device.DeviceTypeId,
        //    });


        //    var fieldNameDict = fields.ToDictionary(x => x.DeviceTypeFieldId, x => x.Name);
        //    var fieldChildDict = fields.ToDictionary(x => x.DeviceTypeFieldId,
        //            x => fields.Where(y => y.ParentDeviceTypeFieldId == x.DeviceTypeFieldId)
        //                    .SelectList(x => x.DeviceTypeFieldId));

        //    List<DeviceDataModel> ParseData(Dictionary<long, List<long>> _fieldChildDict, object request)
        //    {
        //        var response = new List<DeviceDataModel>();

        //        foreach (var item in _fieldChildDict) {
        //            DeviceDataModel model;

        //            if (item.Value.Any()) {
        //                var childDict = item.Value.Select(x => new { ParentId = x, Children = fieldChildDict[x] })
        //                    .ToDictionary(x => x.ParentId, x => x.Children);

        //                response.AddRange(ParseData(childDict, (request as JObject)[fieldNameDict[item.Key]]));
        //            }
        //            else {
        //                var value = (request as JToken)[fieldNameDict[item.Key]];
        //                if (value != null) {
        //                    model = new DeviceDataModel(deviceId, item.Key, value.ToString());
        //                    response.Add(model);
        //                }
        //            }

        //            fieldChildDict.Remove(item.Key);
        //        }

        //        return response;
        //    }

        //    return ParseData(fieldChildDict, request);
        //}

        ///// <summary>
        ///// Creates the device JSON object from the field preset
        ///// </summary>
        //public object GenerateDeviceData(List<DeviceTypeFieldModel> fields, List<DeviceDataModel> data)
        //{
        //    var dataDict = data.ToDictionary(x => x.DeivceTypeFieldId, x => x.Value);
        //    var fieldNameDict = fields.ToDictionary(x => x.DeviceTypeFieldId, x => x.Name);
        //    var fieldChildDict = fields.ToDictionary(x => x.DeviceTypeFieldId,
        //            x => fields.Where(y => y.ParentDeviceTypeFieldId == x.DeviceTypeFieldId)
        //                    .SelectList(x => x.DeviceTypeFieldId));


        //    JObject GenerateResponse(Dictionary<long, List<long>> _fieldChildDict)
        //    {
        //        var response = new JObject();

        //        foreach (var item in _fieldChildDict) {
        //            if (item.Value.Any()) {
        //                var childDict = item.Value.Select(x => new { ParentId = x, Children = fieldChildDict[x] })
        //                    .ToDictionary(x => x.ParentId, x => x.Children);

        //                response[fieldNameDict[item.Key]] = GenerateResponse(childDict);
        //            }
        //            else if (dataDict.TryGetValue(item.Key, out string value)) {
        //                response[fieldNameDict[item.Key]] = value;
        //            }

        //            fieldChildDict.Remove(item.Key);
        //        }

        //        return response;
        //    }

        //    return GenerateResponse(fieldChildDict);
        //}

    }
}
