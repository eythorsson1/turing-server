﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository;
using Turing.Core.Service;
using Turing.Domain.Domain.Enums.User;
using Turing.Domain.Domain.Models.User.Preference;
using Turing.Domain.Infrastructure.Extensions;

namespace Turing.Core.Services.User.Preference
{
    public class UserTablePreferenceService : BaseService
    {
        public UserTablePreferenceService(IUnitOfWorkProvider uowProvider, RepoFactory repoFactory) : base(uowProvider, repoFactory)
        {
        }

        public UserTablePreferenceModel<TableColumnOrderByEnum> GetById<TableColumnOrderByEnum>(long userId, UserTablePreferenceEnum tableId)
            where TableColumnOrderByEnum : Enum
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.UserTablePreferenceRepository(uow);
                bool isNew = false;

                var model = repo.GetById<TableColumnOrderByEnum>(userId, tableId);

                if (model == null) {
                    var list = new List<TableColumnOrderModel<TableColumnOrderByEnum>>();
                    model = new UserTablePreferenceModel<TableColumnOrderByEnum>(userId, tableId, list);
                    isNew = true;
                }
                else if (model.UserId != userId) {
                    model = new UserTablePreferenceModel<TableColumnOrderByEnum>(userId, tableId, model.ColumnOrder);
                    isNew = true;
                }

                if (isNew) Insert(uow, model);

                return model;
            }
        }

        internal void Insert<TableColumnOrderByEnum>(IUnitOfWork uow, UserTablePreferenceModel<TableColumnOrderByEnum> model)
            where TableColumnOrderByEnum : Enum
        {
            var repo = RepoFactory.UserTablePreferenceRepository(uow);
            AddMissingValues(ref model);

            Validate(model);
            if (!repo.Insert(model))
                throw new ArgumentException("Failed to insert the model");
        }


        public void Update<TableColumnOrderByEnum>(UserTablePreferenceModel<TableColumnOrderByEnum> model)
            where TableColumnOrderByEnum : Enum
        {

            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.UserTablePreferenceRepository(uow);
                AddMissingValues(ref model);

                Validate(model);
                if (!repo.Update(model))
                    throw new ArgumentException("Failed to insert the model");
            }
        }

        public void Delete(long userId, UserTablePreferenceEnum userTablePreferenceTableId)
        {

            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.UserTablePreferenceRepository(uow);
                repo.Delete(userId, userTablePreferenceTableId);
            }
        }


        private void Validate<TableColumnOrderByEnum>(UserTablePreferenceModel<TableColumnOrderByEnum> model)
            where TableColumnOrderByEnum : Enum
        {
            if (model.UserId < 1) throw new ArgumentException("UserId is not set");
            if (model.TableId == 0) throw new ArgumentException("UserTablePreferenceTableId is not set");
            if (model.ColumnOrder.Empty()) throw new ArgumentException("TablePreference is not set");
        }
        private void AddMissingValues<TableColumnOrderByEnum>(ref UserTablePreferenceModel<TableColumnOrderByEnum> model)
            where TableColumnOrderByEnum : Enum
        {
            if (model == null) throw new ArgumentException("Model is not set");
            if (model.ColumnOrder.Empty()) model.ColumnOrder = new List<TableColumnOrderModel<TableColumnOrderByEnum>>();

            model.ColumnOrder.RemoveAll(x => x.ColumnId == null || int.Parse(x.ColumnId.ToString()) < 1);

            var existingValues = model.ColumnOrder.Select(x => x.ColumnId).Cast<int>().ToList();
            var possableValues = System.Enum.GetValues(typeof(TableColumnOrderByEnum)).Cast<int>().ToList();
            var missingValues = possableValues.Where(x => !existingValues.Any(y => y != x)).ToList();

            model.ColumnOrder.AddRange(
                missingValues.Cast<TableColumnOrderByEnum>() 
                .Select(columnId => new TableColumnOrderModel<TableColumnOrderByEnum>(columnId))
            );
        }
    }
}
