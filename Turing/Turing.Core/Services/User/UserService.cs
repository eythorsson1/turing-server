﻿using System;
using System.Collections.Generic;
using System.Text;
using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository;
using Turing.Core.Request.Device;
using Turing.Core.Request.User;
using Turing.Core.Service;
using Turing.Domain.Models.User;
using Turing.Domain.Infrastructure;
using Turing.Domain.Exceptions;
using System.Reflection;

namespace Turing.Core.Services.User
{
    public class UserService : BaseService
    {
        public UserService(IUnitOfWorkProvider uowProvider, RepoFactory repoFactory) : base(uowProvider, repoFactory)
        {
        }

        public List<LookupItem> GetLookup(UserRequest request)
        {
            using (var uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.UserRepository(uow);
                return repo.GetLookup(request);
            }
        }

        public PagedList<UserModel> GetPagedList(UserRequest request)
        {
            using (var uow = UowProvider.GetUnitOfWork())
            {
                var repo = RepoFactory.UserRepository(uow);
                return repo.GetPagedList(request);
            }
        }

        public List<UserModel> GetList(UserRequest request)
        {
            using (var uow = UowProvider.GetUnitOfWork())
            {
                var repo = RepoFactory.UserRepository(uow);
                return repo.GetList(request);
            }
        }

        public UserModel FirstOrDefault(UserRequest request)
        {
            using (var uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.UserRepository(uow);
                return repo.FirstOrDefault(request);
            }
        }

        public UserModel GetById(long UserId)
        {
            using (var uow = UowProvider.GetUnitOfWork())
            {
                var repo = RepoFactory.UserRepository(uow);
                return repo.GetById(UserId);
            }
        }

        public void Update(UserModel model)
        {
            using(IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                Update(uow, model);
            }
        }
        internal void Update(IUnitOfWork uow, UserModel model)
        {
            Validate(model);

            var repo = RepoFactory.UserRepository(uow);
            if (!repo.Update(model)) throw new ArgumentException("Failed to insert the user model");
        }

        internal long Insert(IUnitOfWork uow, UserModel model)
        {
            Validate(model);

            var repo = RepoFactory.UserRepository(uow);
            var userId = repo.Insert(model);
            if (userId < 1) throw new ArgumentException("Failed to insert the user model");
            model.GetType().GetProperty(nameof(model.UserId)).SetValue(model, userId);
            return userId;
        }

        private void Validate(UserModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Email)) throw new FeedbackException("Email not set");
            if (string.IsNullOrWhiteSpace(model.Username)) throw new FeedbackException("Username not set");
            if (string.IsNullOrWhiteSpace(model.FirstName)) throw new FeedbackException("FirstName not set");
            if (string.IsNullOrWhiteSpace(model.LastName)) throw new FeedbackException("LastName not set");
            if (model.PasswordHash == null || model.PasswordHash.Length == 0) throw new ArgumentException("PasswordHash not set");

            //if (model.CurrentClientId < 1) throw new ArgumentException("CurrentClientId not set");
        }
    }
}
