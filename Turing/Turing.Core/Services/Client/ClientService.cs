﻿using System;
using System.Collections.Generic;
using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository;
using Turing.Core.Request.Client;
using Turing.Core.Service;
using Turing.Core.Services.ClientUserLink.UserLink;
using Turing.Core.Services.User;
using Turing.Domain.Domain.Models.Client.ClientUserLink;
using Turing.Domain.Exceptions;
using Turing.Domain.Infrastructure;
using Turing.Domain.Models.Client;
using Turing.Domain.Models.User;

namespace Turing.Core.Services.Client
{
    public class ClientService : BaseService
    {
        private readonly ClientUserLinkService ClientUserLinkService;
        private readonly UserService UserService;

        public ClientService(IUnitOfWorkProvider uowProvider, RepoFactory repoFactory,
            ClientUserLinkService clientUserLinkService, UserService userService
            ) : base(uowProvider, repoFactory)
        {
            ClientUserLinkService = clientUserLinkService;
            UserService = userService;
        }


        public PagedList<ClientModel> GetPagedList(ClientRequest request)
        {
            using (var uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.ClientRepository(uow);
                return repo.GetPagedList(request);
            }
        }

        public List<ClientModel> GetList(ClientRequest request)
        {
            using (var uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.ClientRepository(uow);
                return repo.GetList(request);
            }
        }

        public ClientModel GetById(long ClientId)
        {
            using (var uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.ClientRepository(uow);
                return repo.GetById(ClientId);
            }
        }

        public ClientModel FirstOrDefault(ClientRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.ClientRepository(uow);
                return repo.FirstOrDefault(request);
            }
        }

        public void Update(ClientModel model)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                Update(uow, model);
            }
        }
        public void Update(IUnitOfWork uow, ClientModel model)
        {
            var repo = RepoFactory.ClientRepository(uow);
            if (!repo.Update(model))
                throw new ArgumentException("Failed to update the client model");
        }

        public long Insert(ClientModel model)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                return Insert(uow, model);
            }
        }
        public long Insert(IUnitOfWork uow, ClientModel model)
        {
            Validate(model);
            var repo = RepoFactory.ClientRepository(uow);
            var clientId = repo.Insert(model);

            if (clientId < 1)
                throw new ArgumentException("Failed to insert the client model");

            model.GetType().GetProperty(nameof(model.ClientId)).SetValue(model, clientId);
            return clientId;
        }

        public void AddUser(string clientUniqueIdentifier, UserModel user)
        {
            ClientModel client;

            if (string.IsNullOrWhiteSpace(clientUniqueIdentifier))
                client = new ClientModel(user.FullName, user.UserId);
            else {
                client = FirstOrDefault(new ClientRequest { UniqueIdentifier = clientUniqueIdentifier });
                if (client == null) throw new FeedbackException("Invalid Client Identifier");
            }



            using (IUnitOfWork uow = UowProvider.GetUnitOfWork(useTransaction: true)) {
                if (client.ClientId == 0) {
                    if (user.UserId == 0) {
                        UserService.Insert(uow, user);

                        // Set the readOnly CreatedByUserId since we just created the user.
                        client.GetType().GetProperty(nameof(client.CreatedByUserId)).SetValue(client, user.UserId);
                    }

                    Insert(uow, client);
                }

                AddUser(uow, client.ClientId, user);
                uow.Commit();
            }
        }

        internal void AddUser(IUnitOfWork uow, long clientId, UserModel user)
        {
            long userId = user.UserId;

            if (userId == 0) {
                user.CurrentClientId = clientId;
                UserService.Insert(uow, user);
            }

            ClientUserLinkService.Insert(uow, new ClientUserLinkModel(clientId, userId));
        }
        private void Validate(ClientModel model)
        {
            if (string.IsNullOrWhiteSpace(model.UniqueIdentifier)) throw new ArgumentException("UniqueIdentifier is not set");
            if (string.IsNullOrWhiteSpace(model.Name)) throw new ArgumentException("Name is not set");
            if (model.CreatedByUserId < 1) throw new ArgumentException("CreatedByUserId is not set");
        }
    }
}
