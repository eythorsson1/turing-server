﻿using System;
using System.Collections.Generic;
using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository;
using Turing.Core.Request.Client.UserLink;
using Turing.Core.Service;
using Turing.Domain.Domain.Models.Client.ClientUserLink;

namespace Turing.Core.Services.ClientUserLink.UserLink
{
    public class ClientUserLinkService : BaseService
    {
        public ClientUserLinkService(IUnitOfWorkProvider uowProvider, RepoFactory repoFactory) : base(uowProvider, repoFactory)
        {
        }

        public List<ClientUserLinkModel> GetList(ClientUserLinkRequest request)
        {
            using (var uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.ClientUserLinkRepository(uow);
                return repo.GetList(request);
            }
        }

        public ClientUserLinkModel GetById(long ClientUserLinkId)
        {
            using (var uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.ClientUserLinkRepository(uow);
                return repo.GetById(ClientUserLinkId);
            }
        }

        public ClientUserLinkModel FirstOrDefault(ClientUserLinkRequest request)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                var repo = RepoFactory.ClientUserLinkRepository(uow);
                return repo.FirstOrDefault(request);
            }
        }

        public long Insert(ClientUserLinkModel model)
        {
            using (IUnitOfWork uow = UowProvider.GetUnitOfWork()) {
                return Insert(uow, model);
            }
        }
        public long Insert(IUnitOfWork uow, ClientUserLinkModel model)
        {
            Validate(model);

            var repo = RepoFactory.ClientUserLinkRepository(uow);
            var ClientUserLinkId = repo.Insert(model);

            if (ClientUserLinkId < 1)
                throw new ArgumentException("Failed to insert the ClientUserLink model");

            return ClientUserLinkId;
        }

        private void Validate(ClientUserLinkModel model)
        {
            if (model.ClientId < 1) throw new ArgumentException("ClientId is not set");
            if (model.UserId < 1) throw new ArgumentException("UserId is not set");
        }
    }
}
