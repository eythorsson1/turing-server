﻿using Turing.Core.Infrastructure.UnitOfWork;
using Turing.Core.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Turing.Core.Service
{
    public abstract class BaseService
    {
        protected readonly IUnitOfWorkProvider UowProvider;
        protected readonly RepoFactory RepoFactory;

        public BaseService(IUnitOfWorkProvider uowProvider, RepoFactory repoFactory)
        {
            this.UowProvider = uowProvider;
            this.RepoFactory = repoFactory;
        }
    }
}
