﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Turing.Core.Request
{
    public abstract class BaseRequest { }

    public abstract class BaseRequest<TableColumns>
        where TableColumns : Enum
    {
        public TableColumns OrderBy { get; set; }
        public bool OrderByDesc { get; set; }

        internal string OrderByDirectionStr => OrderByDesc ? "DESC" : "ASC";
    }

    public abstract class BasePagedRequest
    {
        public long CurrentPage { get; set; }
        public long ItemsPerPage { get; set; }
    }

    public abstract class BasePagedRequest<TableColumns> : BasePagedRequest
        where TableColumns : Enum
    {
        public TableColumns OrderBy { get; set; }
        public bool OrderByDesc { get; set; }

        internal string OrderByDirectionStr => OrderByDesc ? "DESC" : "ASC";
    }
}
