﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Turing.Core.Request.Data.Value
{
    public class DataValueRequest : BaseRequest<DataValueOrderByEnum>
    {
        public long DataValueId { get; set; }
        public List<long> DataValueIds { get; set; }
        public long DataPresetId { get; set; }
        public string DataValueStr { get; set; }
        public long ClientId { get; set; }
    }

    public enum DataValueOrderByEnum
    {
        DataValueId = 1,
        DataPresetId = 2,
        ClientId = 3
    }
}
