﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Turing.Core.Request.Data
{
    public class DataPresetRequest : BaseRequest<DataPresetOrderByEnum>
    {
        public long DataPresetId { get; set; }
        public List<long> DataPresetIds { get; set; }
        public string DataPresetStr { get; set; }
        public long ClientId { get; set;  }
    }

    public enum DataPresetOrderByEnum
    {
        DataPresetId = 1,
        ClientId = 2
    }
}
