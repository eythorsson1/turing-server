﻿using System.Collections.Generic;
using Turing.Domain.Domain.Enums.Dimension;

namespace Turing.Core.Request.Dimension
{
    public class DimensionRequest : BasePagedRequest<DimensionOrderByEnum>
    {
        public List<long> DimensionIds { get; set; }
        public long DimensionId { get; set; }
        public DimensionTypeEnum Type { get; set; }
        public long ClientId { get; set; }
    }

    public enum DimensionOrderByEnum
    {
        DimensionId = 1,
        Type = 2,
        ClientId = 3
    }
}
