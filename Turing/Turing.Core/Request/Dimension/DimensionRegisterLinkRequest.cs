﻿using System.Collections.Generic;

namespace Turing.Core.Request.Dimension
{
    public class DimensionRegisterLinkRequest : BaseRequest<DimensionRegisterLinkOrderByEnum>
    {
        public long DimensionRegisterLinkId { get; set; }
        public long DimensionRegisterId { get; set; }
        public long DimensionId { get; set; }
        public List<long> DimensionIds { get; set; }
    }

    public enum DimensionRegisterLinkOrderByEnum
    {
        DimensionRegisterLinkId = 1,
        DimensionRegisterId = 2,
        DimensionId = 3,
    }
}
