﻿using System.Collections.Generic;
using Turing.Domain.Domain.Enums.Device;
using Turing.Domain.Domain.Enums.Dimension;

namespace Turing.Core.Request.Dimension
{
    public class DimensionRegisterRequest : BasePagedRequest<DimensionRegisterOrderByEnum>
    {
        public List<long> DimensionRegisterIds { get; set; }
        public long DimensionRegisterId { get; set; }
        public DimensionTypeEnum Type { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public enum DimensionRegisterOrderByEnum
    {
        DimensionRegisterId = 1,
        Type = 2,
        Code = 3,
        Description = 4
    }
}
