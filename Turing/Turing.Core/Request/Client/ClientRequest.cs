﻿using System;
using Turing.Domain.Models.Client;

namespace Turing.Core.Request.Client
{
    public class ClientRequest : BasePagedRequest<ClientOrderByEnum>
    {
        public long ClientId { get; set; }
        public string Name { get; set; }
        public string UniqueIdentifier { get; set; }

        public DateTime CreatedDateUTC { get; set; }
        public int CreatedByUserId { get; set; }
        public string CreatedByFullName { get; set; }
        public string CreatedByFirstName { get; set; }
        public string CreatedByLastName { get; set; }
        public string CreatedByEmail { get; set; }
    }
}
