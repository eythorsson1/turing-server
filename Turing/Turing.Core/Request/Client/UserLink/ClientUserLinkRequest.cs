﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Turing.Core.Request.Client.UserLink
{
    public class ClientUserLinkRequest : BaseRequest<ClientUserLinkOrderByEnum>
    {
        public long ClientUserLinkId { get; set; }
        public long ClientId { get; set; }
        public long UserId { get; set; }
    }

    public enum ClientUserLinkOrderByEnum
    {
        ClientUserLinkId = 1,
        ClientId = 2,
        UserId = 3
    }
}
