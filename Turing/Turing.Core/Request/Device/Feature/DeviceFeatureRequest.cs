﻿using System.Collections.Generic;
using Turing.Domain.Domain.Enums.Device;

namespace Turing.Core.Request.Device.Feature
{
    public class DeviceFeatureRequest : BasePagedRequest<DeviceFeatureOrderByEnum>
    {
        public List<long> DeviceFeatureIds { get; set; }
        public long DeviceFeatureId { get; set; }
        public long DeviceId { get; set; }
        public List<long> DeviceIds { get; set; }
        public DeviceFeatureEnum Type { get; set; }
        public string Name { get; set; }
        public long DimensionId { get; set; }
    }

    public enum DeviceFeatureOrderByEnum
    {
        DeviceFeatureId = 1,
        DeviceId = 2,
        Type = 3,
        Name = 4,
        DimensionId = 5
    }
}
