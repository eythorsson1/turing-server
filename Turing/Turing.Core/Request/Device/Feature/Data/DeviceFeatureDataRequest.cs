﻿using System.Collections.Generic;

namespace Turing.Core.Request.Device.Feature.Data
{
    public class DeviceFeatureDataRequest : BaseRequest<DeviceFeatureDataOrderByEnum>
    {
        public long DeviceFeatureDataId { get; set; }
        public long DeviceFeatureId { get; set; }
        public long DeviceId { get; set; }
        public List<long> DeviceIds { get; set; }
    }

    public enum DeviceFeatureDataOrderByEnum
    {
        DeviceFeatureDataId = 1,
        DeviceFeatureId = 2,
        DeviceId = 3
    }
}
