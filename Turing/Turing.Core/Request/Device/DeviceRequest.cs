﻿using System;
using System.Collections.Generic;
using Turing.Domain.Models.Device;

namespace Turing.Core.Request.Device
{
    public class DeviceRequest : BasePagedRequest<DeviceOrderByEnum>
    {
        public long DeviceId { get; set; }
        public List<long> DeviceIds { get; set; }
        public string Name { get; set; }
        public string ChipId { get;  set; }

        public string SetTopic { get; set; }
        public string StateTopic { get; set; }

        public string AuthName { get; set; }

        public long ClientId { get; set; }

        public DateTime CreatedDateUTC { get; set; }
        public int CreatedByUserId { get; set; }
        public string CreatedByFullName { get; set; }
        public string CreatedByFirstName { get; set; }
        public string CreatedByLastName { get; set; }
        public string CreatedByEmail { get; set; }

        public DateTime LastModifiedDateUTC { get;  set; }
        public int LastModifiedByUserId { get;  set; }
        public string LastModifiedByFullName { get;  set; }
        public string LastModifiedByFirstName { get;  set; }
        public string LastModifiedByLastName { get;  set; }
        public string LastModifiedByEmail { get;  set; }
    }
}
