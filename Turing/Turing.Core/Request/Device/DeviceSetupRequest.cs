﻿using System.Collections.Generic;
using Turing.Domain.Domain.Enums.Device;
using Turing.Domain.Domain.Enums.Dimension;
using Turing.Domain.Domain.Models.Device.Feature;
using Turing.Domain.Domain.Models.Dimension;

namespace Turing.Core.Request.Device
{
    public class DeviceSetupRequest
    {
        public string ChipId { get; set; }
        public string DeviceName { get; set; }
        public string DataPresetStr { get; set; }

        public List<DeviceSetupFeatureModel> Features { get; set; }
        public List<DeviceSetupDimensionModel> Dimensions { get; set; }


        public long CreatedByUserId { get; set; }
        public long ClientId { get; set; }
    }

    public class DeviceSetupFeatureModel
    {
        public DeviceFeatureEnum Type { get; set; }
        public string Name { get; set; }
    }

    public class DeviceSetupDimensionModel 
    {
        public string Name { get; set; }
        public DimensionTypeEnum Type { get; set;  }

        public List<DeviceSetupDimensionRegisterModel> Register { get; set; }
    }
    public class DeviceSetupDimensionRegisterModel
    {
        public DimensionTypeEnum Type { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
