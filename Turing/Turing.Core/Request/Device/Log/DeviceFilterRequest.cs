﻿using System;
using Turing.Domain.Enums.Device;
using Turing.Domain.Models.Device.Log;

namespace Turing.Core.Request.Device.Log
{
    public class DeviceLogFilterRequest : BasePagedRequest<DeviceLogOrderByEnum>
    {
        public long DeviceLogId { get; set; }
        public long DeviceId { get; set; }

        public DeviceLogActionEnum Action { get; set; }
        public DateTime ActionDateUTC { get; set; }
        public long ActionByUserId { get; set; }
        public string ActionByEmail { get; set; }
        public string ActionByFirstName { get; set; }
        public string ActionByLastName { get; set; }

        public string Description { get; set; }
        public string TechnicalDescription { get; set; }

        public long ClientId { get; set; }

        // Aggregated
        public string DeviceName { get; set; }
    }
}
