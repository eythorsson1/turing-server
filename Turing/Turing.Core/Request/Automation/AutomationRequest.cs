﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Turing.Core.Request.Automation
{
    public class AutomationRequest : BasePagedRequest<AutomationOrderByEnum>
    {
        public long AutomationId { get; set; }
        public string Name { get; set; }

        public DateTime CreatedDateUTC { get; set; }
        public long CreatedByUserId { get; set; }
        public string CreatedByFirstName { get; set; }
        public string CreatedByLastName { get; set; }

        public DateTime LastModifiedDateUTC { get; set; }
        public long LastModifiedByUserId { get; set; }
        public string LastModifiedByFirstName { get; set; }
        public string LastModifiedByLastName { get; set; }

        public long ClientId { get; set; }
    }

    public enum AutomationOrderByEnum
    {
        Name = 1,
        
        CreatedDateUTC = 2,
        CreatedByFullName = 3,
        CreatedByFirstName = 4,
        CreatedByLastName = 5,

        LastModifiedDateUTC = 6,
        LastModifiedByFullName = 7,
        LastModifiedByFirstName = 8,
        LastModifiedByLastName = 9
    }
}
