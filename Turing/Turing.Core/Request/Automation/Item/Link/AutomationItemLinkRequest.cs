﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Turing.Core.Request.Automation.Item
{
    public class AutomationItemLinkRequest
    {
        public long AutomationItemLinkId { get; set; }
        public long ParentAutomationItemId { get; set; }
        public long ChildAutomationItemId { get; set; }
        public long AutomationId { get; set; }
        public long ClientId { get; set; }
    }
}
