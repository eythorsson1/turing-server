﻿using System;
using System.Collections.Generic;
using System.Text;
using Turing.Domain.Enums.Automation;

namespace Turing.Core.Request.Automation.Item
{
    abstract public class BaseAutomationItemRequest<AutomationItemTypeEnum> : BasePagedRequest<AutomationItemOrderByEnum>
        where AutomationItemTypeEnum : Enum
    {
        public long AutomationItemId { get; set; }
        public long AutomationId { get; set; }
        public AutomationTypeEnum AutomationType { get; }
        public AutomationItemTypeEnum Type { get; set; }
        public string DataStr { get; set; }

        // Aggregated
        public long ClientId { get; set; }

        public BaseAutomationItemRequest(AutomationTypeEnum automationType)
        {
            AutomationType = automationType;
        }
    }

    public enum AutomationItemOrderByEnum
    {
        AutomationItemId = 0,
    }
}
