﻿using System;
using System.Collections.Generic;
using System.Text;
using Turing.Domain.Enums.Automation;
using Turing.Domain.Enums.Automation.AutomationItemType;

namespace Turing.Core.Request.Automation.Item
{
    public class AutomationActionRequest : BaseAutomationItemRequest<AutomationActionEnum>
    {
        public AutomationActionRequest() : base(AutomationTypeEnum.Action)
        {

        }
    }
}
