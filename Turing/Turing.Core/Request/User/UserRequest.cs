﻿using Turing.Domain.Models.User;

namespace Turing.Core.Request.User
{
    public class UserRequest : BasePagedRequest<UserOrderByEnum>
    {
        public long UserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long ClientId { get; set; }
    }
}
