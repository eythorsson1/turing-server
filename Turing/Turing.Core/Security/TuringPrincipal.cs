﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;
using Turing.Domain.Models.User;

namespace Turing.Core.Security
{
    public class TuringPrincipal : IPrincipal
    {
        public IIdentity Identity { get; private set; }

        public TuringPrincipal(TuringIdentity identity)
        {
            Identity = identity;
        }
        public TuringPrincipal(UserModel user)
        {
            Identity = new TuringIdentity(user);
        }
        public bool IsInRole(string role)
        {
            throw new NotImplementedException();
        }
    }
}
