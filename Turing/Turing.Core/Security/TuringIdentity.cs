﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;
using Turing.Domain.Models.User;

namespace Turing.Core.Security
{
    public class TuringIdentity : IIdentity
    {
        public long UserId { get; private set; }
        public string Username { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }

        public string Name => Username;

        public long ClientId { get; private set; }

        public TuringIdentity(UserModel user)
        {
            this.UserId = user.UserId;
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
            this.ClientId = user.CurrentClientId;
        }

        public TuringIdentity()
        {
        }

        public bool IsAuthenticated => UserId > 0;
        public string AuthenticationType => "Custom Authentication";
    }
}
