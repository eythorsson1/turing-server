﻿using Turing.Core.Service;
using System;
using Turing.Core.Infrastructure.Mqtt;

namespace Turing.Core
{
    public class TuringAppContext
    {
        public static TuringAppContext Current;
        public ServiceContext Services { get; }

        public MqttServer MqttServer { get; }

        public TuringAppContext(ServiceContext serviceContext)
        {
            if (Current != null)
                throw new InvalidOperationException("AppContext is already initialized");

            Services = serviceContext;
            MqttServer = new MqttServer();
        }
    }
}
