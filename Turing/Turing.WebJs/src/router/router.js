import { store } from '../stores/storeContext';

import { createWebHistory, createRouter }  from 'vue-router';

import masterLayoutRoutes from './masterLayoutRoutes';

const Login = () => import('../pages/Login.vue');
//const Register = () => import('../pages/Register.vue');

const router = createRouter({
    //base: process.env.BASE_URL,
    history: createWebHistory(),
    routes: [
        masterLayoutRoutes,
        {
            path: '/Login',
            name: 'Login',
            component: Login,
            meta: {
                AllowAnonymous: true,
                PreventVerifyed: true,
            }
        },
        //{
        //    path: '/Register',
        //    name: 'Register',
        //    component: Register,
        //    meta: {
        //        AllowAnonymous: true,
        //        PreventVerifyed: true,
        //    }
        //},
    ],
});

// Gards
router.beforeEach((to, from, next) => {
    var isLoggedIn = store.getters['Auth/isAuthorized'];

    if (to.meta.PreventVerifyed && isLoggedIn) {
        return next({ path: "/" });
    }
    if (to.meta.AllowAnonymous || isLoggedIn) {
        next();
    }
    else {
        return next({ path: "/Login" });
    }
});

export default router;