const MasterLayout = () => import('../pages/MasterLayout.vue');

const DashboardView = () => import('../views/Dashboard.vue');

export default {
    path: '/',
    name: '',
    component: MasterLayout,
    children: [
        {
            path: '',
            name: 'Dashboard',
            component: DashboardView,
            meta: {
                AllowAnonymous: false,
                RequiredPremissions: [1]
            },
            //children: [ { path, name, component }]
        },
        {
            path: '/Device',
            name: 'DeviceList',
            component: () => import('../views/Device/DeviceList.vue'),
            meta: {
                AllowAnonymous: false,
                RequiredPremissions: [1]
            },
            //children: [ { path, name, component }]
        },
    ]
}