import apiService from './apiService';

// function base(baseUrl){
//     return {
//         GetLookup: (filter) => apiService.Get(baseUrl +"/lookup", filter),
//         GetPagedList: (filter) => apiService.Get(baseUrl, filter),
//         GetList: (filter) => apiService.Get(baseUrl + "/all", filter),
//         GetById: (id) => apiService.Get(baseUrl + "/" + id),
    
//         Update: (model) => apiService.Post(baseUrl + "/update", model),
//         Create: (model) => apiService.Post(baseUrl, model),
//         Delete: (id) => apiService.Post(baseUrl + "/" + id + "/delete"),
//     }
// }

const device = { 
    GetLookup: (filter) => apiService.Get("device/lookup", filter),
    GetPagedList: (filter) => apiService.Get("device", filter),
    GetList: (filter) => apiService.Get("device/all", filter),
    // Set: (request) => apiService.Post("device/set", request),
}

const automation = { 
    Trigger: (id) => apiService.Post("automation/" + id + "/trigger"),
}

delete automation.GetLookup;
delete automation.GetPagedList;
delete automation.GetList;
delete automation.GetById;
delete automation.Update;
delete automation.Create;
delete automation.Delete;
delete automation.Trigger;



export default {
    Device: device,
    Automation: automation,
};