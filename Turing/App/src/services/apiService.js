import axios from 'axios'

const errorHandler = require('../handlers/errorHandler').default

const http = axios.create({
    baseURL: '/api/',
    //timeout: 5000,
    withCredentials: true,
})


var GetRequest = function (throwError, url, data = {}) {
    return new Promise((resolve, reject) => {
        http.get(url, {
            params: data
        }).then(function (data) {
            resolve(data.data);
        }).catch(err => {
            if (throwError)
                errorHandler.DisplayError(err);

            reject(err);
        })
    });
};

const PostRequest = function (throwError, url, data = {}) {
    return new Promise((resolve, reject) => {
        http.post(url, data)
            .then(function (data) {
                resolve(data.data);
            })
            .catch(err => {
                if (throwError)
                    errorHandler.DisplayError(err)

                reject(err);
            })
    })
}

export default {
    Get: (url, data) => GetRequest(true, url, data),
    Post: (url, data) => PostRequest(true, url, data),

    BlindGet: (url, data) => GetRequest(false, url, data),
    BlindPost: (url, data) => PostRequest(false, url, data),

    defaults: http.defaults
}