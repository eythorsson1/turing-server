import apiService from './apiService';


const automation = { 
    Trigger: (id) => apiService.Post("automation/" + id + "/trigger"),
}

const device = (function () {
    var base = function (deviceId) {
        return {
            Set: (request) => apiService.Post("device/" + deviceId + "/set", request),
        };
    };

    base.GetLookup = (filter) => apiService.Get("device/lookup", filter);
    base.GetPagedList = (filter) => apiService.Get("device", filter);
    base.GetList = (filter) => apiService.Get("device/all", filter);

    return base;
})();

const dimension = (function(){
    //var base = (function (dimensionId) {
    //    return {
    //        Update: (filter) => apiService.Get("dimension/" + dimensionId, filter),
            
    //    }
    //});

    var base = {};
    //base.GetLookup = (filter) => apiService.Get("dimension/lookup", filter);
    base.GetPagedList = (filter) => apiService.Get("dimension", filter);
    base.GetList = (filter) => apiService.Get("dimension/all", filter);

    //base.Insert = (filter) => apiService.Get("dimension", filter);

    return base;
})();



export default {
    Automation: automation,
    Device: device,
    Dimension: dimension,
};