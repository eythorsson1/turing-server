export default {
    beforeMount: function (el, binding, vnode) {
        if (typeof binding.value !== 'function') {
            const compName = vnode.context.name
            let warn = `[Vue-on-submit:] provided expression '${binding.expression}' is not a function, but has to be`
            if (compName) { warn += `Found in component '${compName}'` }

            console.warn(warn)
            return;
        }

        // DEFINE HANDLER AND CACHE IT ON THE ELEMENT
        const handler = (e) => {
            var items = Array.from(el.querySelectorAll("input[required], select[required], textarea[required]"));

            var isValid = !items.map(item => {
                var isValid = item.checkValidity();

                // IF ITEM IS INSIDE A FORM CONTROL GROUP SHOUD THE ERROR CLASS GO IN IT IN STEAD
                // OF THE ELEMENT ITSELF.
                var rootElement = item;
                if (Array.from(item.parentElement.classList).indexOf("form-control-group") > -1)
                    rootElement = item.parentElement;

                if (!isValid)
                    rootElement.classList.add("input-error");
                else
                    rootElement.classList.remove("input-error");

                return isValid;
            }).some(x => !x);

            if (isValid)
                binding.value(e)
        }

        const button = el.querySelector("button[type=submit]");

        el.__vueOnSubmit__ = {
            button: button,
            handler: handler
        }


        button.addEventListener("click", handler);
    },

    unmounted: function (el) {
        const cache = el.__vueOnSubmit__;
        cache.button.removeEventListener('mouseup', cache.handler);
        el.__vueOnSubmit__ = null;
    }
}