const displayError = function (error) {
    alert(error);
}
const displayCustomError = function (title, error) {
    alert(title, error);
}

export default {
    DisplayError: displayError,
    DisplayCustomError: displayCustomError
}