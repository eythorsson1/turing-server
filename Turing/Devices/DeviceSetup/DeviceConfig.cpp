#include "Arduino.h"
#include "DeviceConfig.h"

bool DeviceConfig::TryGet(){
   if (!SPIFFS.begin ()) {
      Serial.println ("An Error has occurred while mounting SPIFFS");
      return false;
   }

   File file = SPIFFS.open("/config.json", "r");
   if(!file){
      Serial.println("Failed to open file for reading");
      return false;
   }

   String configJsonStr = "";
   while(file.available()){
      configJsonStr += (char) file.read();
   }
   file.close();

   // PARSE JSON 
   // PARSE JSON 
   DynamicJsonDocument doc(512);
   DeserializationError err = deserializeJson(doc, configJsonStr);

   if (err) {
      Serial.print(F("deserializeJson() failed with code "));
      Serial.println(err.c_str());
      return false;
   }
   else {
      // serializeJsonPretty(doc, Serial);
      ParseJson(doc);
      
      return true;
   }
}
void DeviceConfig::ParseJson(DynamicJsonDocument doc){
   DeviceId = doc["DeviceId"].as<long>();
   
   WiFiName = (char*)doc["WiFiName"].as<char*>();
   WiFiPass = (char*)doc["WiFiPass"].as<char*>();

   AuthName = (char*)doc["AuthName"].as<char*>();
   AuthPass = (char*)doc["AuthPass"].as<char*>();

   MqttIP = (char*)doc["MqttIP"].as<char*>();
   MqttPort = doc["MqttPort"].as<uint16_t>();
   MqttSet = (char*)doc["MqttSet"].as<char*>();
   MqttState = (char*)doc["MqttState"].as<char*>();
}


void DeviceConfig::Save(){
   if (!SPIFFS.begin ()) {
      Serial.println ("An Error has occurred while mounting SPIFFS");
      return;
   }

   File file = SPIFFS.open("/config.json", "w");
   if(!file){
      Serial.println("Failed to open file for reading");
      return;
   }

   DynamicJsonDocument doc(512);

   doc["DeviceId"] = DeviceId;
      
   doc["WiFiName"] = WiFiName;
   doc["WiFiPass"] = WiFiPass;

   doc["AuthName"] = AuthName;
   doc["AuthPass"] = AuthPass;

   doc["MqttIP"] = MqttIP;
   doc["MqttPort"] = MqttPort;
   doc["MqttSet"] = MqttSet;
   doc["MqttState"] = MqttState;
   
   serializeJson(doc, file);
   file.close();
}

void DeviceConfig::Print(){
   Serial.println("---------- DEVICE CONFIG ----------");
   Serial.print("DeviceId:\t");
   Serial.println(DeviceId);
  
   Serial.print("WiFiName:\t");
   Serial.println(WiFiName);
   Serial.print("WiFiPass:\t");
   Serial.println(WiFiPass);
  
   Serial.print("AuthName:\t");
   Serial.println(AuthName);
   Serial.print("AuthPass:\t");
   Serial.println(AuthPass);
   

   Serial.print("MqttIP:\t\t");
   Serial.println(MqttIP);
   Serial.print("MqttPort:\t");
   Serial.println(MqttPort);
   Serial.print("MqttSet: \t");
   Serial.println(MqttSet);
   Serial.print("MqttState:\t");
   Serial.println(MqttState);
}