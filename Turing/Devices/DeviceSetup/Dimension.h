#ifndef Dimension_h
#define Dimension_h

#include "Arduino.h"
#include "ArduinoJson.h"

// enum DimensionRegisterTypeEnum{
//   DIMENSION_REGISTER_OPTION = 1
// };

// class DimensionRegisterModel {
//   public:
//     DimensionRegisterModel(String code, String description, DimensionRegisterTypeEnum type);
//     String Code;
//     String Description;
//     DimensionRegisterTypeEnum Type;
// };

////////////////////////////////////////////////////////////////////////////////////////////////////////////

// enum DimensionTypeEnum{
//   DIMENSION_SELECT = 1
// };

// class DimensionModel{
//   public:
//     DimensionModel(String name, DimensionTypeEnum type, DimensionRegisterModel *dimensionRegister);
//     String Name;
//     DimensionTypeEnum Type;
//     DimensionRegisterModel (&Register)[];
// };

enum DimensionTypeEnum{
  DIMENSION_SELECT = 1
};

class DimensionModel{
  public:
    DimensionModel(String name, DimensionTypeEnum type, String registerJsonArrStr);
    String Name;
    DimensionTypeEnum Type;
    // DimensionRegisterModel Register[];
    String RegisterJsonArrStr;
};
#endif