// ALTERNATIVE TO PubSubClient: https://github.com/marvinroger/async-mqtt-client
#include <ArduinoJson.h>
#include <FastLED.h>

#include "DeviceConfig.h"
#include "DeviceFeature.h"
#include "Dimension.h"

/// Turing Config
PubSubClient _mqttClient;
WiFiClient _wifiClient;
DeviceConfig config;

const char* DeviceName = "WS2812";
const char* DataPresetStr = "{ \"Color\": { \"Red\": {{Color.Red}}, \"Green\": {{Color.Green}}, \"Blue\": {{Color.Blue}} }, \"EffectId\": {{Effect}} }";
const DeviceFeatureModel Features[] = {
   DeviceFeatureModel("Color", FEATURE_COLOR_PICKER),
   DeviceFeatureModel("Effect", FEATURE_SELECT)
};

// DimensionRegisterModel EffectDimReg[] = {
//    DimensionRegisterModel("1", "Solid Color", DIMENSION_REGISTER_OPTION),
//    DimensionRegisterModel("2", "Running Rabit", DIMENSION_REGISTER_OPTION)
// };

// DimensionModel DeviceDimensions[] = {
//    DimensionModel("Effect", DIMENSION_SELECT, EffectDimReg)
// };
DimensionModel DeviceDimensions[] = {
   DimensionModel("Effect", DIMENSION_SELECT, "[ { Code: 1, Description: \"Solid Color\", Type: 1 }, { Code: 2, Description: \"Running Rabit\", Type: 1 } ]")
};

// Custom
#define NUM_LEDS 10
#define DATA_PIN 2
CRGB leds[NUM_LEDS];

int effectId = 0;       // 0: blank, 1: SolidColor, 2: Running Rabit
int _step = 0;           // Used in some of the effects;
int _delay = 50;        // The real delay in ms;
int _delayCounter = 0;  // used to keep track of the delay;


void setup(){
   // TURING CONFIG
   Serial.begin(115200);
   Serial.println();
   config.TryGet();
   config.Print();


   // CUSTOM
   FastLED.addLeds<WS2812, DATA_PIN, GRB>(leds, NUM_LEDS);
   SetColor(184, 245, 0);
}

void loop(){
   if (ConnectToWiFi((char*)config.WiFiName.c_str(), (char*)config.WiFiPass.c_str(), 5)){
      _mqttConnect();
   }

   PreformEffect();
   delay(1);
}

// GET THE DEVICE STATE
// GET THE DEVICE STATE
JsonObject GetDeviceState(){
   Serial.println("Getting Device State");
   DynamicJsonDocument doc(512);

   // Create an object
   JsonObject object = doc.to<JsonObject>();
   object["EffectId"] = effectId;

   if (effectId == 1){
      object["Color"]["Red"] = leds[0].r;
      object["Color"]["Green"] = leds[0].g;
      object["Color"]["Blue"] = leds[0].b;
   }


   // String response;
   // serializeJson(object, response);
   return object;
}

// SET THE DEVICE STATE
// SET THE DEVICE STATE
void SetDeviceState(const JsonObject &data){
   Serial.println("Setting Device State");
   
   serializeJsonPretty(data, Serial);
   Serial.println();

   effectId = data["EffectId"].as<int>();
   Serial.println(effectId);

   switch (effectId)
   {
      case 0:
         SetColor(0,0,0);
      break;

      case 1:
         SetColor(
            data["Color"]["Red"].as<int>(), 
            data["Color"]["Green"].as<int>(), 
            data["Color"]["Blue"].as<int>()
         );
      break;
   
      default:
         break;
   }
}


void PreformEffect(){
   if (_delay <= _delayCounter){
      switch (effectId)
      {
         case 2:
            PreformEffect_RunningRabit();
         break;
      
         default:
            break;
      }
      _delayCounter = 0;
   }
   _delayCounter++;
}

void SetColor(int red, int green, int blue){
   Serial.print("Setting Color To: \t(");
   Serial.print(red);
   Serial.print(", ");
   Serial.print(green);
   Serial.print(", ");
   Serial.print(blue);
   Serial.print(")\n");

   fill_solid(leds, NUM_LEDS, CRGB(red, green, blue));
   FastLED.show();
}

void PreformEffect_RunningRabit(){
   if (NUM_LEDS <= _step) _step = 0;
   else _step++;

   for(int i = 0; i < NUM_LEDS; i++){
      if (i == _step) leds[i] = CRGB(100,50,50);
      else leds[i] = CRGB::Black;
   }

   FastLED.show();
}



/// INTERNAL 
/// INTERNAL 
/// INTERNAL 
// WiFi
bool ConnectToWiFi(char* ssid, char* pass, int attempts)
{
  if (WiFi.status() == WL_CONNECTED) {
      return strcmp(WiFi.SSID().c_str(), ssid) == 0;
  }

  Serial.print("Connecting to WiFi:\t\t");
  Serial.print(ssid);
  Serial.print("@");
  Serial.print(pass);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);


  while(WiFi.waitForConnectResult() != WL_CONNECTED) {
    if (attempts <= 0) {
      Serial.print(" [Failed] (Status: ");
      Serial.print(WiFi.status());
      Serial.println(")");
      return false;
    }
    delay(1000);
    attempts--;
    Serial.print(".");
  }

  Serial.println("\t[Success]");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  return true;
}

// MQTT
bool _mqttConnect(){
   if (WiFi.status() != WL_CONNECTED) return false;

   if(!_mqttClient.connected()){
      Serial.print("\nConnecting to MQTT:\t\t");

      _mqttClient.setClient(_wifiClient);
      _mqttClient.setServer((char*)config.MqttIP.c_str(), config.MqttPort);
      _mqttClient.setCallback(_mqttCallback);

      if(!_mqttClient.connect((char*)config.AuthName.c_str(), (char*)config.AuthName.c_str(), (char*)config.AuthPass.c_str())){
         Serial.println("[Failed]");
         return false;
      }
   
      if (config.DeviceId > 0){
         // Device Is Setup...
         Serial.print("[Success]\nSubscribing To MQTT:(");
         Serial.print((char*)config.MqttSet.c_str());
         Serial.print(")\t");
         if(!_mqttClient.subscribe((char*)config.MqttSet.c_str())){
            Serial.println("[Failed]");
            return false;
         }
            
         Serial.println("[Success]");
      }
      else {
         // Device is not setup...
         Serial.print("[Success]\nSetting Up Device...\nSubscribing To MQTT: (Device/Setup/Complete)\t");
         if(!_mqttClient.subscribe("Device/Setup/Complete")){
            Serial.println("[Failed]");
            return false;
         }
      }
   }

   if (config.DeviceId < 1){
      // Device is not setup...
      Serial.print("[Success]\nSending DevicePreset:\t\t");

      DynamicJsonDocument doc(512);
      doc["ChipId"] = String(ESP.getChipId(), HEX);

      doc["DeviceName"] = DeviceName;
      doc["DataPresetStr"] = DataPresetStr;

      JsonArray _features = doc.createNestedArray("Features");
      for(int f = 0; f < (int)sizeof(Features)/(int)sizeof(DeviceFeatureModel); f++){
         DeviceFeatureModel data = Features[f];
         JsonObject obj = _features.createNestedObject();
         obj["Name"] = data.Name;
         obj["Type"] = data.Type;
      }

      JsonArray _dimensions = doc.createNestedArray("Dimensions");
      for(int d = 0; d < (int)sizeof(DeviceDimensions)/(int)sizeof(DimensionModel); d++){
         DimensionModel data = DeviceDimensions[d];
         JsonObject obj = _dimensions.createNestedObject();
         obj["Name"] = data.Name;
         obj["Type"] = data.Type;

         DynamicJsonDocument regDoc(512);
         deserializeJson(regDoc, data.RegisterJsonArrStr);
         obj["Register"] = regDoc.as<JsonArray>();


         // JsonArray regArr = obj.createNestedArray("Register");
         // for(int r = 0; (int)sizeof(data.Register)/(int)sizeof(DimensionRegisterModel); r++){
         //    DimensionRegisterModel regData = data.Register[r];
         //    JsonObject regObj = regArr.createNestedObject();
         //    regObj["Type"] = regData.Type;
         //    regObj["Code"] = regData.Code;
         //    regObj["Description"] = regData.Description;
         // }
      }


      String payload;      
      serializeJson(doc, payload);

      bool success =_mqttClient.publish("Device/Setup", (char *)payload.c_str());
      if (!success){
         Serial.println("[Failed]");
         return false;
      }
      else {
         Serial.println("[Success]");
      }
      

      serializeJsonPretty(doc, payload);

      Serial.print("Waiting for the device details");
      for(int attempts = 15; attempts > 0; attempts--){
         if (config.DeviceId > 0) attempts = 0;
         _mqttClient.loop();
         delay(1000);
         Serial.print(".");
      }
      
      Serial.println(config.DeviceId > 0 ? "[Success]" : "[Failed]");

   }

   return _mqttClient.loop();
}
void _mqttCallback(char *topic, byte* payload, unsigned int length){
   Serial.print("Message recived from: ");
   Serial.println(topic);
   
   payload[length] = '\0';
   char* requestStr = (char*)payload;
   
   Serial.println(requestStr);

   // PARSE JSON 
   // PARSE JSON 
   DynamicJsonDocument doc(512);
   DeserializationError err = deserializeJson(doc, requestStr);
   if (err) {
      Serial.print(F("deserializeJson() failed with code "));
      Serial.println(err.c_str());
      return;
   }

   // If the callback is for the set topic should the device respond to the request
   // on the state topic (with the messageId as a referance key) to validate that 
   // device recived and completed the request.
   if(strcmp(topic, (char *)config.MqttSet.c_str()) == 0){
      
      if (SetDeviceState){
         SetDeviceState(doc["Data"]);
      }

      doc["Data"] = GetDeviceState();

      String payload;
      serializeJson(doc, payload);
      Serial.println(payload);

      _mqttClient.publish((char *)config.MqttState.c_str(), payload.c_str());
   }
   else if (strcmp(topic, "Device/Setup/Complete") == 0){
      // Save details
      Serial.println((char*)doc["Data"]["ChipId"].as<char*>());
      if (strcmp(String(ESP.getChipId(), HEX).c_str(), (char*)doc["Data"]["ChipId"].as<char*>()) != 0) {
         return;
      }

      Serial.println("Parsing Json Details");

      // Make sure the we do not override thise when we parse the json document...
      doc["Data"]["WiFiName"] = config.WiFiName;
      doc["Data"]["WiFiPass"] = config.WiFiPass;
      doc["Data"]["MqttIP"] = config.MqttIP;
      doc["Data"]["MqttPort"] = config.MqttPort;

      config.ParseJson(doc["Data"]);
      
      config.Print();
      config.Save();

      doc["Data"] = true;
      
      String response;
      serializeJson(doc, response);

      Serial.println(response);
      _mqttClient.publish("Device/Setup/Success", (char*)response.c_str());

      delay(2000);
      // The WiFi disconnect essentially resets the device so that we can 
      // setup the device again..
      WiFi.disconnect(); 
   }
}
