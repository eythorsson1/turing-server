#ifndef DeviceFeature_h
#define DeviceFeature_h

#include "Arduino.h"

enum DeviceFeatureTypeEnum{
  FEATURE_POWER_STATE = 1,

  FEATURE_COLOR_PICKER = 2,
  // ColorShema = 3,
  FEATURE_BRIGHTNESS = 4,

  FEATURE_SELECT = 5,
  FEATURE_SLIDER = 6,
  FEATURE_TOGGLE = 7
};

class DeviceFeatureModel{
  public:
    DeviceFeatureModel(String name, DeviceFeatureTypeEnum type);
    String Name;
    DeviceFeatureTypeEnum Type;
};
#endif