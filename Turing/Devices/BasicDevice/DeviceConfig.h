#ifndef DeviceConfig_h
#define DeviceConfig_h

#include "Arduino.h"
#include <ArduinoJson.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>

class DeviceConfig{
  public:
    // DeviceConfig();
    bool SetupComplete;
    long DeviceId;

    String AuthName;
    String AuthPass;
    
    String WiFiName;
    String WiFiPass;

    String MqttIP;
    uint16_t MqttPort;
    String MqttSet;
    String MqttState;

    bool TryGet();
    void Print();
};

#endif