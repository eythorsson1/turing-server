#include <ArduinoJson.h>
#include <FastLED.h>

#include "DeviceConfig.h"

PubSubClient _mqttClient;
WiFiClient _wifiClient;
DeviceConfig config;


#define NUM_LEDS 10
#define DATA_PIN 2
CRGB leds[NUM_LEDS];

int effectId = 0;       // 0: blank, 1: SolidColor, 2: Running Rabit
int _step = 0;           // Used in some of the effects;
int _delay = 50;        // The real delay in ms;
int _delayCounter = 0;  // used to keep track of the delay;

void setup(){
   Serial.begin(115200);
   Serial.println();
   config.TryGet();
   config.Print();


   FastLED.addLeds<WS2812, DATA_PIN, GRB>(leds, NUM_LEDS);
   SetColor(184, 245, 0);
}

void loop(){
   if (ConnectToWiFi((char*)config.WiFiName.c_str(), (char*)config.WiFiPass.c_str(), 5)){
      _mqttConnect();
   }

   PreformEffect();
   delay(1);
}

// GET THE DEVICE STATE
// GET THE DEVICE STATE
JsonObject GetDeviceState(){
   Serial.println("Getting Device State");
   DynamicJsonDocument doc(512);

   // Create an object
   JsonObject object = doc.to<JsonObject>();
   object["EffectId"] = effectId;

   if (effectId == 1){
      object["Color"]["Red"] = leds[0].r;
      object["Color"]["Green"] = leds[0].g;
      object["Color"]["Blue"] = leds[0].b;
   }


   // String response;
   // serializeJson(object, response);
   return object;
}

// SET THE DEVICE STATE
// SET THE DEVICE STATE
void SetDeviceState(const JsonObject &data){
   Serial.println("Setting Device State");
   
   serializeJsonPretty(data, Serial);
   Serial.println();

   effectId = data["EffectId"].as<int>();
   Serial.println(effectId);

   switch (effectId)
   {
      case 0:
         SetColor(0,0,0);
      break;

      case 1:
         SetColor(
            data["Color"]["Red"].as<int>(), 
            data["Color"]["Green"].as<int>(), 
            data["Color"]["Blue"].as<int>()
         );
      break;
   
      default:
         break;
   }
}


void PreformEffect(){
   if (_delay <= _delayCounter){
      switch (effectId)
      {
         case 2:
            PreformEffect_RunningRabit();
         break;
      
         default:
            break;
      }
      _delayCounter = 0;
   }
   _delayCounter++;
}

void SetColor(int red, int green, int blue){
   Serial.print("Setting Color To: \t(");
   Serial.print(red);
   Serial.print(", ");
   Serial.print(green);
   Serial.print(", ");
   Serial.print(blue);
   Serial.print(")\n");

   fill_solid(leds, NUM_LEDS, CRGB(red, green, blue));
   FastLED.show();
}

void PreformEffect_RunningRabit(){
   if (NUM_LEDS <= _step) _step = 0;
   else _step++;

   for(int i = 0; i < NUM_LEDS; i++){
      if (i == _step) leds[i] = CRGB(100,50,50);
      else leds[i] = CRGB::Black;
   }

   FastLED.show();
}




/// INTERNAL 
/// INTERNAL 
/// INTERNAL 
// WiFi
bool ConnectToWiFi(char* ssid, char* pass, int attempts)
{
  if (WiFi.status() == WL_CONNECTED) {
      return strcmp(WiFi.SSID().c_str(), ssid) == 0;
  }

  Serial.print("Connecting to WiFi:\t\t");
  Serial.print(ssid);
  Serial.print("@");
  Serial.print(pass);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);


  while(WiFi.waitForConnectResult() != WL_CONNECTED) {
    if (attempts <= 0) {
      Serial.print(" [Failed] (Status: ");
      Serial.print(WiFi.status());
      Serial.println(")");
      return false;
    }
    delay(1000);
    attempts--;
    Serial.print(".");
  }

  Serial.println("\t[Success]");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  return true;
}

// MQTT
bool _mqttConnect(){
   if (WiFi.status() != WL_CONNECTED) return false;

   if(!_mqttClient.connected()){
      Serial.print("\nConnecting to MQTT:\t\t");

      _mqttClient.setClient(_wifiClient);
      _mqttClient.setServer((char*)config.MqttIP.c_str(), config.MqttPort);
      _mqttClient.setCallback(_mqttCallback);

      // if(!_mqttClient.connect(_authName, _authName, _authPass, "test/will", 0, false, "Hello World")){
      if(!_mqttClient.connect((char*)config.AuthName.c_str(), (char*)config.AuthName.c_str(), (char*)config.AuthPass.c_str())){
         Serial.println("[Failed]");
         return false;
      }
   
      // if (sizeof (config.MqttSet) > 0){
      Serial.print("[Success]\nSubscribing To MQTT:(");
      Serial.print((char*)config.MqttSet.c_str());
      Serial.print(")\t");
      if(!_mqttClient.subscribe((char*)config.MqttSet.c_str())){
         Serial.println("[Failed]");
         return false;
      }
         
      Serial.println("[Success]");
   }

   return _mqttClient.loop();
}
void _mqttCallback(char *topic, byte* payload, unsigned int length){
   Serial.print("Message recived from: ");
   Serial.println(topic);
   
   payload[length] = '\0';
   char* requestStr = (char*)payload;
   
   Serial.println(requestStr);

   // PARSE JSON 
   // PARSE JSON 
   DynamicJsonDocument doc(512);
   DeserializationError err = deserializeJson(doc, requestStr);
   if (err) {
      Serial.print(F("deserializeJson() failed with code "));
      Serial.println(err.c_str());
      return;
   }

   // If the callback is for the set topic should the device respond to the request
   // on the state topic (with the messageId as a referance key) to validate that 
   // device recived and completed the request.
   if(strcmp(topic, (char *)config.MqttSet.c_str()) == 0){
      
      if (SetDeviceState){
         SetDeviceState(doc["Data"]);
      }

      // doc["MessageId"] = doc["MessageId"];
      doc["Data"] = GetDeviceState();

      String payload;
      serializeJson(doc, payload);
      Serial.println(payload);

      _mqttClient.publish((char *)config.MqttState.c_str(), payload.c_str());
   }
}
