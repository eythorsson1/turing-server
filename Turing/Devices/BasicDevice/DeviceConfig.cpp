#include "Arduino.h"
#include "DeviceConfig.h"


bool DeviceConfig::TryGet(){
   if (!SPIFFS.begin ()) {
      Serial.println ("An Error has occurred while mounting SPIFFS");
      return false;
   }

   File file = SPIFFS.open("/config.json", "r");
   if(!file){
      Serial.println("Failed to open file for reading");
      return false;
   }

   String configJsonStr = "";
   while(file.available()){
      configJsonStr += (char) file.read();
   }
   file.close();

   // PARSE JSON 
   // PARSE JSON 
   DynamicJsonDocument doc(512);
   DeserializationError err = deserializeJson(doc, configJsonStr);

   if (err) {
      Serial.print(F("deserializeJson() failed with code "));
      Serial.println(err.c_str());
      return false;
   }
   else {
      // serializeJsonPretty(doc, Serial);
      SetupComplete = doc["SetupComplete"].as<bool>();
      DeviceId = doc["DeviceId"].as<long>();
      
      WiFiName = (char*)doc["WiFiName"].as<char*>();
      WiFiPass = (char*)doc["WiFiPass"].as<char*>();

      AuthName = (char*)doc["AuthName"].as<char*>();
      AuthPass = (char*)doc["AuthPass"].as<char*>();

      MqttIP = (char*)doc["MqttIP"].as<char*>();
      MqttPort = doc["MqttPort"].as<uint16_t>();
      MqttSet = (char*)doc["MqttSet"].as<char*>();
      MqttState = (char*)doc["MqttState"].as<char*>();
      
      return true;
   }
}

void DeviceConfig::Print(){
   Serial.println("---------- DEVICE CONFIG ----------");
   Serial.print("SetupComplete:\t");
   Serial.println(SetupComplete);
   Serial.print("DeviceId:\t");
   Serial.println(DeviceId);
  
   Serial.print("WiFiName:\t");
   Serial.println(WiFiName);
   Serial.print("WiFiPass:\t");
   Serial.println(WiFiPass);
  
   Serial.print("AuthName:\t");
   Serial.println(AuthName);
   Serial.print("AuthPass:\t");
   Serial.println(AuthPass);
   

   Serial.print("MqttIP:\t\t");
   Serial.println(MqttIP);
   Serial.print("MqttPort:\t");
   Serial.println(MqttPort);
   Serial.print("MqttSet: \t");
   Serial.println(MqttSet);
   Serial.print("MqttState:\t");
   Serial.println(MqttState);
}