////import axios from 'axios'

const errorHandler = require('../handlers/errorHandler').default

//const http = axios.create()


//var GetRequest = function (throwError, url, data = {}) {
//    return new Promise((resolve, reject) => {
//        http.get(url, {
//            params: data
//        }).then(function (data) {
//          console.log(data)
//            resolve(data.data);
//        }).catch(err => {
//            if (throwError)
//                errorHandler.DisplayError(err);

//            reject(err);
//        })
//    });
//};

//const PostRequest = function (throwError, url, data = {}) {
//    return new Promise((resolve, reject) => {
//        http.post(url, data)
//            .then(function (data) {
//                resolve(data.data);
//            })
//            .catch(err => {
//                if (throwError)
//                    errorHandler.DisplayError(err)

//                reject(err);
//            })
//    })
//}

var baseUrl = "api/"

export default ({ $axios }, inject) => {

  //const http = $axios.create({
  //  debug: true,
  //  withCredentials: true,
  //  progress: false,
  //  proxy: {
  //    '/api/': {
  //      port: 6061
  //    }
  //  }
  //})

  var GetRequest = function (throwError, url, data = {}) {
    return new Promise((resolve, reject) => {
      $axios.get(baseUrl + url, {
        params: data
      }).then(function (data) {
        console.log(data)
        resolve(data.data);
      }).catch(err => {
        if (throwError)
          errorHandler.DisplayError(err);

        reject(err);
      })
    });
  };

  const PostRequest = function (throwError, url, data = {}) {
    return new Promise((resolve, reject) => {
      $axios.post(baseUrl + url, data)
        .then(function (data) {
          resolve(data.data);
        })
        .catch(err => {
          if (throwError)
            errorHandler.DisplayError(err)

          reject(err);
        })
    })
  }

  inject('apiService', {
    Get: (url, data) => GetRequest(true, url, data),
    Post: (url, data) => PostRequest(true, url, data),

    BlindGet: (url, data) => GetRequest(false, url, data),
    BlindPost: (url, data) => PostRequest(false, url, data),
  })
}
