
export default ({ app, store }) => {
  // Gards
  app.router.beforeEach((to, from, next) => {
    var isLoggedIn = store.getters['auth/isAuthorized'];

    console.log('isLoggedIn', isLoggedIn);

    if (to.meta.PreventVerifyed && isLoggedIn) {
      return next({ path: "/" });
    }
    if (to.meta.AllowAnonymous || isLoggedIn) {
      next();
    }
    //else {
    //  return next({ path: "/Login" });
    //}
  });
}
