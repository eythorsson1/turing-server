import Vue from 'vue'

Vue.directive('click-outside', {
    beforeMount: function (el, binding, vnode) {
        if (typeof binding.value !== 'function') {
            const compName = vnode.context.name
            let warn = `[Vue-click-outside:] provided expression '${binding.expression}' is not a function, but has to be`
            if (compName) { warn += `Found in component '${compName}'` }

            console.warn(warn)
        }

        // Define Handler and cache it on the element
        const bubble = binding.modifiers.bubble
        const handler = (e) => {
            if (bubble || (!el.contains(e.target) && el !== e.target)) {
                binding.value(e)
            }
        }

        el.__vueClickOutside__ = handler

        // add Event Listeners
        document.addEventListener('mouseup', handler)
    },

    unmounted: function (el) {
        // Remove Event Listeners
        document.removeEventListener('mouseup', el.__vueClickOutside__)
        el.__vueClickOutside__ = null

    }
})
