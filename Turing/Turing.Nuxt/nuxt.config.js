export default {
  ssr: false,

  head: {
    title: 'Turing.Nuxt',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  css: [
    '~/assets/style/site.scss'
  ],

  plugins: [
    '~/plugins/directives/ClickOutside.js',
    '~/plugins/directives/OnSubmit.js',
    '~/plugins/services/apiService.js',
    '~/plugins/services/dataService.js',
    //'~/plugins/router.js',
  ],

  components: true,

  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    '@nuxtjs/tailwindcss'
  ],

  modules: [
    '@nuxtjs/axios',
    //'nuxt-route-meta',
    '@nuxtjs/proxy'
  ],

  //axios: {
  //  withCredentials: true,
  //  progress: false,
  //},

  //proxy: {
  //    port: 6061,
  //"/api/": {
  //target: "http://localhost:6061/api/",
  ////pathRewrite: { "/api/": "/api/" },
  //changeOrigin: false,
  //followRedirects: false,
  //logLevel: "debug",
  //secure: false,
  //},
  //"/api/": "http://localhost:6061/api/",
  //},

  axios: {
    proxy: true,
    host: 'localhost',
    prefix: '/api/',
    //headers: { //optional
    //  Accept: 'application/json',
    //  'Content-Type': 'application/json',
    //}
  },

  proxy: {
    "^/api/": {
      target: "https://localhost:6061",
      pathRewrite: { "^/api/": "/api/" },
      changeOrigin: false,
      followRedirects: false,
      logLevel: "debug",
      secure: false,
      debug: true,
    }
  },

  //loadingIndicator: {
  //  name: 'folding-cube',
  //  color: '#3B8070',
  //  background: 'white'
  //},

  //axios: {
  //  proxy: true,
  //  credentials: true,
  //  prefix: true
  //},

  //axios: {
  //  roxy: true,
  //  prefix: '/api/'
  //},
  //...process.env.NODE_ENV === 'development' && {
  //  proxy: {
  //    '/api/': 'http://localhost:6061',
  //  }
  //},


  //proxy: {
  //  // Simple proxy
  //  //'/api': 'http://localhost:6061',

  //  //// With options
  //  '/api': {
  //    target: 'http://localhost:6061',
  //    ws: false
  //  },
  //}
  //},

  build: {
  }
}
