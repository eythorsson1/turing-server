using Hangfire;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using MQTTnet.AspNetCore;
using MQTTnet.AspNetCore.Extensions;
using System;
using System.Threading.Tasks;
using Turing.API.Configuration.AutoMapper;
using Turing.Core;
using Turing.Core.Service;

namespace Turing.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            // INITIALIZE THE SERVICES
            var serviceContext = new ServiceContext(Configuration.GetConnectionString("TURING"));
            TuringAppContext.Current = new TuringAppContext(serviceContext);


            // HANGFIRE
            services.AddHangfire(configuration => configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseSqlServerStorage(Configuration.GetConnectionString("HANGFIRE"), new SqlServerStorageOptions {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    DisableGlobalLocks = true
                }));

            services.AddHangfireServer();


            // SETUP COOKIE AUTHENTICATION
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options => {
                    options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                    options.Cookie.Name = "Turing.Identity";
                    options.Cookie.HttpOnly = true;

                    options.LoginPath = string.Empty;
                    options.LogoutPath = string.Empty;
                    options.AccessDeniedPath = string.Empty;

                    options.Events = new CookieAuthenticationEvents {
                        OnRedirectToLogin = ctx => {
                            ctx.Response.StatusCode = StatusCodes.Status401Unauthorized;
                            return Task.FromResult(0);
                        }
                    };
                });


            services.SetupAutoMapper();


            //services.SetupEmailService(Configuration, localhost: true);


            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });
        }
    }
}
