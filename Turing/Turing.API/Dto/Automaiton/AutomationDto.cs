﻿using System;
using System.Collections.Generic;

namespace Turing.API.Dto.Automaiton
{
    public class AutomationDto
    {
        public long AutomationId { get; set; }
        public string Name { get; set; }
        public DateTime CratedDateUTC { get; set; }
        public long CreatedByUserId { get; set; }
        public string CreatedByFirstName { get; set; }
        public string CreatedByLastName { get; set; }
        
        public long LastModifiedByUserId { get; set; }
        public string LastModifiedByFirstName { get; set; }
        public string LastModifiedByLastName { get; set; }

        public string CreatedByFullName => $"{CreatedByFirstName} {CreatedByLastName}".Trim();
        public string LastModifiedByFullName => $"{LastModifiedByFirstName} {LastModifiedByLastName}".Trim();

        public List<AutomationItemDto> Items { get; set; } 

    }
}
