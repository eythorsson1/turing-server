﻿using Turing.Domain.Domain.Enums.Device;

namespace Turing.API.Dto.Device
{
    public class DeviceTypeFeatureDto
    {
        public long DeviceTypeFeatureId { get; set; }
        public long DeivceTypeId { get; set; }
        public DeviceTypeFeatureEnum Type { get; set; }
        public string Name { get; set; }
    }
}