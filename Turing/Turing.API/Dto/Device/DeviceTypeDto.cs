﻿using System;
using System.Collections.Generic;

namespace Turing.API.Dto.Device
{
    public class DeviceTypeDto
    {
        public long DeviceTypeId { get; set; }
        public string Name { get; set; }
        public object DataPresetStr { get; set; }

        public DateTime CreatedDateUTC { get; set; }
        public long CreatedByUserId { get; set; }
        public string CreatedByFirstName { get; set; }
        public string CreatedByLastName { get; set; }
        public string CreatedByFullName => $"{CreatedByFirstName} {CreatedByLastName}";
        public string CreatedByEmail { get; set; }

        public List<DeviceTypeFeatureDto> Features { get; set; }
    }
}
