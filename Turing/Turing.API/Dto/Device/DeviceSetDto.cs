﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Turing.API.Dto.Device
{
    public class DeviceSetDto
    {
        public Dictionary<string, object> Data { get; set; }
    }
}
