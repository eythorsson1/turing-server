﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Turing.API.Dto.Device
{
    public class DeviceDto
    {
        public long DeviceId { get; set; }
        public string Name { get; set; }
        public string DataPresetTempalte { get; set; }

        public long DeviceTypeId { get; set; }
        public string DeviceTypeName { get; set; }
        public List<DeviceTypeFeatureDto> Features { get; set; }
    }
}
