﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Turing.API.Configuration.AutoMapper.MapperProfiles;
using Turing.API.Dto;
using Turing.Domain.Infrastructure;
using Turing.Domain.Infrastructure.Extensions;
using Turing.Domain.Models;

namespace Turing.API.Configuration.AutoMapper
{
    public static class AutoMapperConfig
    {
        private static IMapper Mapper { get; set; }

        public static void SetupAutoMapper(this IServiceCollection services)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.AddProfile(new DefaultMapper());
            });

            Mapper = config.CreateMapper();
        }

        public static T MapTo<T>(this object data)
        {
            return Mapper.Map<T>(data);
        }

        public static List<T> MapTo<T>(this List<object> data)
        {
            return Mapper.Map<List<T>>(data);
        }

        public static PagedListDto<T> MapTo<T>(this PagedList<IModel> data)
        {
            return new PagedListDto<T>(
                data.Items.SelectList(x => (object)x).MapTo<T>(),
                data.TotalNumberOfItems,
                data.CurrentPage,
                data.ItemsPerPage
            );
        }
    }
}
