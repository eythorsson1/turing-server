﻿using AutoMapper;
using Turing.API.Dto.User;
using Turing.Domain.Models.User;

namespace Turing.API.Configuration.AutoMapper.MapperProfiles
{
    public class DefaultMapper : Profile
    {
        public DefaultMapper()
        {
            // USER 
            CreateMap<UserModel, UserDto>();
        }
    }
}
