﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security;
using Turing.Core;
using Turing.Core.Request.User;
using Turing.Core.Service;
using Turing.Domain.Models.User;

namespace Turing.API.Controller
{
    [Authorize]
    [ApiController]
    public class BaseController : ControllerBase
    {
        protected ServiceContext Services => TuringAppContext.Current.Services;

        protected UserModel CurrentUser {
            get {
                if (!User.Identity.IsAuthenticated)
                    throw new SecurityException("Invalid principal");

                return Services.UserService.FirstOrDefault(
                    new UserRequest { Username = User.Identity.Name }
                );

            }
        }
    }
}
