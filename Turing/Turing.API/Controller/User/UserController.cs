﻿using Microsoft.AspNetCore.Mvc;
using Turing.API.Configuration.AutoMapper;
using Turing.API.Dto.User;
using Turing.Core.Request.User;
using Turing.Core.Services.User;

namespace Turing.API.Controller.User.Security
{
    [Route("api/user")]
    public class UserController : BaseController
    {
        private UserService UserService => Services.UserService;

        [HttpGet("lookup")]
        public IActionResult GetLookup([FromQuery] UserRequest request)
        {
            if (request == null) return BadRequest();
            request.ClientId = CurrentUser.CurrentClientId;

            return Ok(UserService.GetLookup(request));
        }


        [HttpGet("current")]
        public IActionResult GetCurrentUser()
        {
            return Ok(CurrentUser.MapTo<UserDto>());
        }

    }
}
