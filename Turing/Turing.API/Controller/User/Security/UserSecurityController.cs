﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

using Turing.API.Dto.User.Security;
using Turing.Core.Request.User;
using Turing.Core.Services.Client;
using Turing.Core.Services.User;
using Turing.Domain.Models.User;

namespace Turing.API.Controller.User.Security
{
    [Route("api/user/security")]
    public class UserSecurityController : BaseController
    {
        private UserService UserService => Services.UserService;
        private ClientService ClientService => Services.ClientService;

        [AllowAnonymous]
        [HttpPost("signin")]
        public async Task<IActionResult> SignIn([FromBody] LoginRequestDto dto)
        {
            var user = UserService.FirstOrDefault(new UserRequest { Username = dto.Username });

            if (user == null) return BadRequest();
            if (user.VerifyPassword(dto.Password)) {
                await SignInUser(user, dto.IsPersistant);
            }
            return Ok();
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterRequestDto dto)
        {
            var user = UserService.FirstOrDefault(new UserRequest { Username = dto.Username });
            if (user != null) return BadRequest("Username is already in use");

            user = new UserModel(dto.Email, dto.Username, dto.FirstName, dto.LastName, dto.Password);
            ClientService.AddUser(dto.ClientUniqueIdentifier, user);

            await SignInUser(user, false);

            return Ok();
        }

        [HttpPost("signout")]
        public async Task<IActionResult> Signout()
        {
            await HttpContext.SignOutAsync();
            return Ok();
        }

        // HELPERS
        private async Task SignInUser(UserModel user, bool isPersistent)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.Username),
                new Claim(ClaimTypes.Email, user.Email)
            };

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties {
                IssuedUtc = DateTime.UtcNow,
                ExpiresUtc = DateTime.UtcNow.AddYears(1),
                IsPersistent = isPersistent
            };

            await HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity),
                authProperties
            );
        }
    }
}
