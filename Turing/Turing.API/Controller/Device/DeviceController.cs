﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Turing.API.Configuration.AutoMapper;
using Turing.API.Dto.Device;
using Turing.Core.Request.Device;
using Turing.Core.Request.Device.Type.Feature;
using Turing.Core.Services.Device;
using Turing.Core.Services.Device.Data;
using Turing.Core.Services.Device.Feature;
using Turing.Domain.Domain.Enums.Device;
using Turing.Domain.Domain.Models.Device.Data;
using Turing.Domain.Domain.Models.Device.Type.Feature;
using Turing.Domain.Domain.Models.Device.Type.Field;
using Turing.Domain.Exceptions;
using Turing.Domain.Infrastructure.Extensions;
using Turing.Domain.Infrastructure.Helper;
using Turing.Domain.Models.Device;

namespace Turing.API.Controller.Device
{
    [Route("api/device")]
    public class DeviceController : BaseController
    {
        private DeviceService DeviceService => Services.DeviceService;
        private DeviceDataService DeviceDataService => Services.DeviceDataService;
        private DeviceTypeFeatureService DeviceTypeFeatureService => Services.DeviceTypeFeatureService;

        [HttpGet("lookup")]
        public IActionResult GetLookup([FromQuery] DeviceRequest request)
        {
            if (request == null) return BadRequest();
            request.ClientId = CurrentUser.CurrentClientId;
            return Ok(DeviceService.GetLookup(request));
        }

        [HttpGet("")]
        public IActionResult GetPagedList([FromQuery] DeviceRequest request)
        {
            if (request == null) return BadRequest();
            request.ClientId = CurrentUser.CurrentClientId;
            return Ok(DeviceService.GetPagedList(request).MapTo<DeviceDto>());
        }
        [HttpGet("all")]
        public IActionResult GetList([FromQuery] DeviceRequest request)
        {
            if (request == null) return BadRequest();
            request.ClientId = CurrentUser.CurrentClientId;
            return Ok(DeviceService.GetList(request).MapTo<DeviceDto>());
        }

        //[HttpPost("{deviceId}")]
        //public async Task<IActionResult> Update([FromBody] DeviceDto dto)
        //{
        //    if (dto == null) return BadRequest();

        //    var device = DeviceService.GetById(dto.DeviceId, includeDeviceData: false);
        //    if (device?.ClientId != CurrentUser.CurrentClientId) return BadRequest();

        //    device.Name = dto.Name;
        //    await DeviceService.Update(device);

        //    return Ok();
        //}

        [HttpPost("")]
        public IActionResult Insert([FromBody] DeviceDto dto)
        {
            dto.Name = dto.Name.Trim();
            string deviceAuthName = dto.Name.Replace(" ", "_");
            string deviceAuthPass = StringHelper.GenerateRandomString(12);

            var model = new DeviceModel(dto.Name, dto.DeviceTypeId, deviceAuthName, deviceAuthPass, CurrentUser.CurrentClientId, CurrentUser.CurrentClientId);

            return Ok(new { DeviceAuthName = deviceAuthName, DeivceAuthPassword = deviceAuthPass });
        }


        //[HttpPost("{deviceId}/set")]
        //public async Task<IActionResult> SetDevice([FromRoute] long deviceId, [FromBody] DeviceSetDto dto)
        //{
        //    if (dto == null) return BadRequest();
        //    if (deviceId < 1) return BadRequest();

        //    var device = DeviceService.GetById(deviceId);
        //    if (device?.ClientId != CurrentUser.CurrentClientId) return BadRequest();

        //    DeviceService.SetData(device.DeviceId, dto.Data);


        //    //var deviceData = DeviceService.ParseModelData(device.DeviceId, device.DeviceTypeId, dto.Data, allowMissingFields: true);
        //    //await DeviceService.Update(device, deviceData);
        //    //DeviceService.ParseRequest(device, dto.Data)

        //    // dto.Data = { ColorPicker: {Red: 100, Green: 150, Blue: 300 }}




        //    return Ok();
        //}



        [HttpGet("parseTest")]
        public IActionResult ParseTest()
        {
            // 1. Create the fields list from the presetData
            // 2. Parse the request
            // 3. Create a response from the parsed request

            // 4. Validate the parsed request


            var presetData = JsonConvert.DeserializeObject("{ \"RightColor\": { \"R\": \"{{ColorPicker_Right.Red}}\", \"G\": \"{{ColorPicker_Right.Green}}\", \"B\": \"{{ColorPicker_Right.Blue}}\", }, \"ColorBrightness\": \"{{Brightness1}}\", \"Effect\": \"{{Select_Effect}}\", \"LeftMonitor\": { \"Color\": { \"Hue\": \"{{ColorPicker_LeftMonitor.Hue}}\", \"Saturation\": \"{{ColorPicker_LeftMonitor.Saturation}}\", \"Value\": \"{{ColorPicker_LeftMonitor.Value}}\", \"ColorPreset\": \"{{Select_LeftColorPreset}}\" }, \"State\": { \"Power\": \"{{PowerState_LeftMonitor}}\", \"Effect\": \"{{Select_LeftEffect}}\", \"Speed\": \"{{Slider_Speed}}\" } }}");


            //object request = JsonConvert.DeserializeObject("{ \"RightColor\": { \"R\": 100, \"G\": 150, \"B\": 229 } , \"ColorBrightness\": 100, \"Effect\": 2, \"LeftMonitor\": { \"Color\": { \"Hue\": 123, \"Saturation\": 60, \"Value\": 80, \"ColorPreset\": 1 }, \"State\": { \"Power\": true, \"Effect\": 4 } } }");

            object request = JsonConvert.DeserializeObject("{\"ColorPicker_Right\":{\"Red\":100,\"Green\":150,\"Blue\":229},\"Brightness1\":100,\"Select_Effect\":2,\"ColorPicker_LeftMonitor\":{\"Hue\":123,\"Saturation\":60,\"Value\":80},\"Select_LeftColorPreset\":1,\"PowerState_LeftMonitor\":true,\"Select_LeftEffect\":4}");
            

            long deviceTypeId = 1;


            var features = new List<DeviceTypeFeatureModel>() {
                new DeviceTypeFeatureModel(deviceTypeId, DeviceTypeFeatureEnum.ColorPicker, "ColorPicker_Right"),
                new DeviceTypeFeatureModel(deviceTypeId, DeviceTypeFeatureEnum.Brightness, "Brightness1"),
                new DeviceTypeFeatureModel(deviceTypeId, DeviceTypeFeatureEnum.Select, "Select_Effect"),
                new DeviceTypeFeatureModel(deviceTypeId, DeviceTypeFeatureEnum.ColorPicker, "ColorPicker_LeftMonitor"),
                new DeviceTypeFeatureModel(deviceTypeId, DeviceTypeFeatureEnum.ColorShema, "Select_LeftColorPreset"),
                new DeviceTypeFeatureModel(deviceTypeId, DeviceTypeFeatureEnum.PowerState, "PowerState_LeftMonitor"),
                new DeviceTypeFeatureModel(deviceTypeId, DeviceTypeFeatureEnum.Select, "Select_LeftEffect"),
                new DeviceTypeFeatureModel(deviceTypeId, DeviceTypeFeatureEnum.Slider, "Slider_Speed")
            };

            var featureDict = features.ToDictionary(x => x.Name);

            var presetPlaceholderRegex = new Regex(@"\{\{(.*?)\}\}");

            // Validate the preset



            //var mqttRequest = new Regex(@"\{\{(.*?)\}\}").Replace(presetData.ToString(), delegate (Match m) {
            //    var itemString = m.Groups[1].ToString();
            //    var parts = itemString.Split(".");
            //});


            return Ok();
        }
    }
}
