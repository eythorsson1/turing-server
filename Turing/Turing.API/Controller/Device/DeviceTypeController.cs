﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Turing.API.Dto.Device;
using Turing.Core.Services.Device.Type;
using Turing.Domain.Domain.Models.Device.Type;
using Turing.Domain.Domain.Models.Device.Type.Feature;
using Turing.Domain.Infrastructure.Extensions;

namespace Turing.API.Controller.Device.Type
{
    [Route("api/devicetype")]
    public class DeviceTypeController : BaseController
    {
        private DeviceTypeService DeviceTypeService => Services.DeviceTypeService;


        [HttpPost("")]
        public IActionResult Create([FromBody] DeviceTypeDto dto)
        {
            var dt_model = new DeviceTypeModel(dto.Name, dto.DataPresetStr.ToString(), CurrentUser.UserId, CurrentUser.CurrentClientId);
            var features = dto.Features.SelectList(x=> (x.Name, x.Type));

            DeviceTypeService.Insert(dt_model, features);

            return Ok();
        }
    }
}
